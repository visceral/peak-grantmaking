<?php
/**
 * View: Top Bar
 *
 * Override this template in your own theme by creating a file at:
 * [your-theme]/tribe/events/v2/list/top-bar.php
 *
 * See more documentation about our views templating system.
 *
 * @link http://evnt.is/1aiy
 *
 * @version 5.0.1
 *
 */

$current_url = tribe_events_get_current_filter_url();
$events_url  = get_permalink( get_page_by_path('events') );
$classes     = array( 'tribe-clearfix', 'row' );
// Instead of all event categories, we only want categories of upcoming events
// 1. get query of upcoming events
$upcomingEvents = tribe_get_events([
   'posts_per_page' => -1,
   'start_date'     => 'now',
]);
// 2. loop through these for their post terms
$cats = array();
foreach( $upcomingEvents as $event ) {
    $eventTerms = wp_get_post_terms($event->ID, 'tribe_events_cat', array('fields' =>'ids'));
    $cats[] = $eventTerms;
}
// 3. this is going to return a multidimensional array. flatten that & grab only the unique values
$uniqueCats = array_flatten($cats);
// 4. now we'll grab only the needed terms
$cats = get_terms('tribe_events_cat', array( 'include' => $uniqueCats )); 

?>
<div class="tribe-events-c-top-bar tribe-events-header__top-bar">
    <form id="tribe-bar-form" class="<?php echo esc_attr( implode( ' ', $classes ) ); ?>" name="tribe-bar-form" method="post" action="<?php echo esc_attr( $current_url ); ?>">
		<div class="column sm-50">
			<label class="screen-reader-text" for="tribe_events_category">{{ __('Event Type', 'visceral') }}</label>
			
			<select name="tribe_events_category" id="tribe_events_category" class="input--alt" onchange="window.location.href=this.value">
				<option value="<?php echo $events_url; ?>"><?php echo __('All Upcoming Events', 'visceral'); ?></option>
				<?php if( $cats ) { ?>
					<?php foreach($cats as $cat ) {
						$selected = (get_queried_object() == $cat) ? 'selected' : '';
					?>
						<option value="<?php echo $current_url . '&tribe_events_cat=' . $cat->slug; ?>" <?php echo $selected; ?>><?php echo $cat->name ?></option>
					<?php } ?>
				<?php } ?>
			</select>
		</div>
		<div class="column js-show">
			<input type="submit" value="Submit">
		</div>
	</form>
	<?php // $this->template( 'list/top-bar/nav' ); ?>

	<?php // $this->template( 'components/top-bar/today' ); ?>

	<?php // $this->template( 'list/top-bar/datepicker' ); ?>

	<?php // $this->template( 'components/top-bar/actions' ); ?>

</div>
