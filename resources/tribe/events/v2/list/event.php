<?php
/**
 * View: List Event
 *
 * Override this template in your own theme by creating a file at:
 * [your-theme]/tribe/events/v2/list/event.php
 *
 * See more documentation about our views templating system.
 *
 * @link http://evnt.is/1aiy
 *
 * @version 5.0.0
 *
 * @var WP_Post $event The event post object with properties added by the `tribe_get_event` function.
 *
 * @see tribe_get_event() For the format of the event object.
 */

$container_classes = [ 'tribe-common-g-row', 'tribe-events-calendar-list__event-row' ];
$container_classes['tribe-events-calendar-list__event-row--featured'] = $event->featured;
$event_classes = tribe_get_post_class( [ 'tribe-events-calendar-list__event', 'tribe-common-g-row', 'tribe-common-g-row--gutters' ], $event->ID );

$categories = tribe_get_event_categories(
	get_the_id(), array(
		'before'       => '',
		'sep'          => ', ',
		'after'        => '',
		'label'        => '', // An appropriate plural/singular label will be provided
		'label_before' => '',
		'label_after'  => '',
		'wrap_before'  => '',
		'wrap_after'   => '',
	)
);
$categories = str_replace( ': ', '', $categories);

?>
<div class="list-item-event type-tribe_events">
    <div <?php tribe_classes( 'row md-reverse' ); ?>>
    	<div class="tribe-events-calendar-list__event-wrapper column md-75">
    		<article <?php tribe_classes( $event_classes ) ?>>

    			<div class="tribe-events-calendar-list__event-details tribe-common-g-col">
    				<header class="tribe-events-calendar-list__event-header">
    					<?php $this->template( 'list/event/title', [ 'event' => $event ] ); ?>
    					<?php $this->template( 'list/event/venue', [ 'event' => $event ] ); ?>
    				</header>

    				<?php $this->template( 'list/event/description', [ 'event' => $event ] ); ?>
    				<?php $this->template( 'list/event/cost', [ 'event' => $event ] ); ?>

    			</div>
    		</article>
    	</div>
        <div class="column md-25">
            <?php if( $categories ) {
				echo '<p class="list-item-event__categories"><strong>' . $categories . '</strong></p>'; 
			} ?>
            <?php $this->template( 'list/event/date', [ 'event' => $event ] ); ?>
        </div>
    </div>
</div>