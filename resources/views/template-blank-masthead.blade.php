{{--
  Template Name: Masthead (no overlay)
--}}
@extends('layouts.app')

@section('content')
  @while(have_posts()) @php(the_post())
  @include('partials.content-page')
  @endwhile
@endsection
