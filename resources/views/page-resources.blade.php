@extends('layouts.app')

@section('content')
  @while(have_posts()) @php(the_post())

    {{-- h1 is not displayed in design, but there should still be one on the page --}}
    <h1 class="screen-reader-text">{!! App::title() !!}</h1>

    @include('partials.content-page')

    {{-- slider --}}
    @if($featured_resources)
        <div class="container reveal">
            <div class="featured-resources">
                @foreach($featured_resources as $resource)
                    <div class="featured-resource">
                        <div class="row">
                            <div class="column md-60">
                                <div class="featured-resource__content">
                                    <p class="featured-resource__meta">Featured Resource</p>
                                    <h2 class="featured-resource__title h1"><a href="{{ get_permalink($resource->ID) }}">{{ get_the_title($resource->ID) }}</a></h2>
                                </div>
                            </div>
                            <div class="column md-40">
                                @if(has_post_thumbnail($resource->ID))
                                    <a href="{{ get_permalink($resource->ID) }}" class="featured-resource__image img-zoom">
                                        {!! get_the_post_thumbnail( $resource->ID, 'large', array('class' => 'portrait') ) !!}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
    <section class="list-wrap list-wrap--resources">
        <div class="container">
            <form action="{{ get_permalink() }}" class="filters filters--resources">
                <div class="row">
                    <div class="column md-75">
                        <label class="screen-reader-text" for="resource_type">{{ __('Type', 'visceral') }}</label>
                        <select name="resource_type" id="resource_type" class="input--alt" onchange="this.form.submit()">
                            <option value="">{{ __('All Types', 'visceral') }}</option>
                            @if($get_resource_types)
                                @foreach($get_resource_types as $resource_type)
                                @php( $resource_type_selected = ($form_filters->resource_type === $resource_type->slug ) ? 'selected' : '')
                                <option value="{{ $resource_type->slug }}" {{ $resource_type_selected }}>{{ $resource_type->name }}</option>
                                @endforeach
                            @endif
                        </select>

                        <label class="screen-reader-text" for="resource_principle">{{ __('Principle', 'visceral') }}</label>
                        <select name="resource_principle" id="resource_principle" class="input--alt" onchange="this.form.submit()">
                            <option value="">{{ __('All Principles', 'visceral') }}</option>
                            @if($get_resource_principles)
                                @foreach($get_resource_principles as $resource_principle)  
                                @php( $resource_principle_selected = ($form_filters->resource_principle === $resource_principle->slug ) ? 'selected' : '') 
                                <option value="{{ $resource_principle->slug }}" {{ $resource_principle_selected }}>{{ $resource_principle->name }}</option>
                                @endforeach
                            @endif
                        </select>

                        <label class="screen-reader-text" for="resource_topic">{{ __('Topic', 'visceral') }}</label>
                        <select name="resource_topic" id="resource_topic" class="input--alt" onchange="this.form.submit()">
                            <option value="">{{ __('All Topics', 'visceral') }}</option>
                            @if($get_resource_topics)
                                @foreach($get_resource_topics as $resource_topic)  
                                @php( $resource_topic_selected = ($form_filters->resource_topic === $resource_topic->slug ) ? 'selected' : '') 
                                <option value="{{ $resource_topic->slug }}" {{ $resource_topic_selected }}>{{ $resource_topic->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="column md-25">
                        <h5 class="filters__count">{{$begin . '-' . $end . ' ' . __('of', 'visceral') . ' ' . $total}} Resources</h5>
                    </div>
                    <div class="column js-show">
                        <input type="submit" value="Submit">
                    </div>
                </div>
            </form>
                                             
            @if($get_resources->posts)
                <div class="row">
                    @foreach($get_resources->posts as $post)
                        @php(setup_postdata($GLOBALS['post'] = $post))
                            @include('partials.list-item-resource')
                        @php(wp_reset_postdata())
                    @endforeach
                </div>

                <div class="pagination">
                    {!! $pagination !!}
                </div>
            @else
                <div class="row">
                    <div class="column sm-100">
                        <h4>Sorry, no resources were found.</h4>
                        <p><a href="{{ get_permalink( get_page_by_path('resources') ) }}"><span class="icon icon-arrow-left"></span> View all Resources</a></p>
                    </div>
                </div>
            @endif
        </div>
    </section>


  @endwhile
@endsection