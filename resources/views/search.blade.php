@extends('layouts.app')
@php
// Set up results numbers
global $wp_query;
$total = $wp_query->found_posts;
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$post_per_page = $wp_query->query_vars['posts_per_page'];
$offset = ( $paged - 1 ) * $post_per_page;
$begin = $offset + 1;
$end = ( $paged*$post_per_page < $total ) ? $paged * $post_per_page : $total;

// Save the original query
if ( isset( $_GET['s'] ) ) {
	$original_search = sanitize_text_field( $_GET['s'] );
}

// Create content for filter dropdown
$args = array(
	'public' => true,
);
$post_types = get_post_types( $args );

$filter_items = '';
if ( $post_types ) {
	$filter_items = '<option value="">' . __('Filter', 'visceral') . '</option>';
	foreach ( $post_types as $slug ) {
        $exclude = array( 'guest-author', 'wpdmpro', 'journals' );
        if( TRUE === in_array( $slug, $exclude ) )
            continue;

		if ( $slug != 'attachment' && $slug != 'give_forms' && $slug != 'popupbuilder') {
			$label           = get_post_type_object( $slug )->labels->name;
			$filter_selected = (isset($_GET['filter']) &&  sanitize_text_field( $_GET['filter'] ) == $slug ) ? 'selected' : '';
			$filter_items   .= '<option name="content_filter" value="' . $slug . '"' . $filter_selected . '>' . $label . '</option>';
		}
	}
} else {
	$filter_items .= '<option>' . __('No filters', 'visceral') . '</option>';
}
@endphp
@section('content')
  <div class="search-results-top">
    <form action="<?php echo site_url(); ?>" class="filters filters--search">
      <div class="row row-eq-height">
        <label class="column md-33"><span class="screen-reader-text">{{ __('Search', 'visceral')}}</span>
          <input type="text" name="s" value="{{ $original_search }}" class="">
        </label>
        <div class="column md-20">
          <input type="submit" value="{{ __('Submit', 'visceral')}}">
        </div>
      </div>
      <div class="row md-reverse row-eq-height">
        <div class="column md-33">
            <label><span class="screen-reader-text">{{ __('Show me only:', 'visceral')}}</span>
              <select name="filter" onchange="this.form.submit()">{{!! $filter_items !!}}</select>
            </label>
          </div>
          <div class="column md-67">
            <h5>{{$begin . '-' . $end . ' ' . __('of', 'visceral') . ' ' . $total}} results found for {{ $original_search }}</h5>
          </div>
      </div>
    </form>
  </div>
  @if (!have_posts())
    <div class="alert alert-warning">
      {{  __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @while(have_posts()) @php(the_post())
    @include('partials.content-search')
  @endwhile

  <div class="pagination">
    {!! $pagination !!}
  </div>
@endsection
