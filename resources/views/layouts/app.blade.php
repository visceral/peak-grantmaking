<!doctype html>
<html @php(language_attributes()) class="no-js">
  @include('partials.head')
  <body @php(body_class())>
    <a id="skip-to-content" href="#main-content" class="screen-reader-text no-print" aria-label="<?php _e('Skip to content', 'visceral'); ?>" title="<?php _e('Skip to content', 'visceral'); ?>"><?php _e('Skip to content', 'visceral'); ?></a>
    
    @php(do_action('get_header'))
    @include('partials.header')
    @if(is_page('insights'))
        @include('partials.masthead-insights')
    @elseif(is_singular('journals'))
        @include('partials.masthead-journals')
    @else
        @include('partials.masthead')
    @endif
    @include('partials.masthead-home')
    <div class="wrap container" role="document" id="main-content">

        <main class="main">
          @yield('content')
        </main>
        @if (App\display_sidebar())
          <aside class="sidebar">
            @include('partials.sidebar')
          </aside>
        @endif

    </div>
    @php(do_action('get_footer'))
    @include('partials.footer')
    @php(wp_footer())
  </body>
</html>
