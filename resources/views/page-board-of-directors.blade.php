@extends('layouts.app')

@section('content')
  @while(have_posts()) @php(the_post())
    {{-- h1 is not displayed in design, but there should still be one on the page --}}
    <h1 class="screen-reader-text">{!! App::title() !!}</h1>

    @include('partials.content-page')

  @endwhile
@endsection