@extends('layouts.app')
@section('content')
  @while(have_posts()) @php(the_post())
    {{-- h1 is not displayed in design, but there should still be one on the page --}}
    <h1 class="screen-reader-text">{!! App::title() !!}</h1>

    <form action="{{ get_permalink() }}" class="reveal filters filters--news">
        <div class="row">
            <div class="column lg-50">
                <label class="screen-reader-text" for="news_type">{{ __('Type', 'visceral') }}</label>
                <select name="news_type" id="news_type" class="input--alt" onchange="this.form.submit()">
                    <option value="">{{ __('All Types', 'visceral') }}</option>
                    @if($get_news_types)
                        @foreach($get_news_types as $news_type) 
                        @php( $news_type_selected = ($form_filters->news_type === $news_type->slug ) ? 'selected' : '') 
                        <option value="{{ $news_type->slug }}" {{ $news_type_selected }}>{{ $news_type->name }}</option>
                        @endforeach
                    @endif
                </select>

                <label class="screen-reader-text" for="news_topic">{{ __('Topic', 'visceral') }}</label>
                <select name="news_topic" id="news_topic" class="input--alt" onchange="this.form.submit()">
                    <option value="">{{ __('All Topics', 'visceral') }}</option>
                    @if($get_news_topics)
                        @foreach($get_news_topics as $news_topic)
                        @php( $news_topic_selected = ($form_filters->news_topic === $news_topic->slug ) ? 'selected' : '') 
                        <option value="{{ $news_topic->slug }}" {{ $news_topic_selected }}>{{ $news_topic->name }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="column js-show">
                <input type="submit" value="Submit">
            </div>
        </div>  
    </form>

    @if($featured_posts && empty($form_filters->news_type) && empty($form_filters->news_topic) )
        <article class="featured-news reveal">
            <div class="row">
                @foreach($featured_posts as $post)
                    @php(setup_postdata($GLOBALS['post'] = $post))
                        @include('partials.list-item-post')
                    @php(wp_reset_postdata())
                @endforeach
            </div>
        </article>
    @endif


    <section class="">
        <div class="row">
            <div class="column md-67">
                @if($get_posts->posts)
                    <div class="row list-wrap list-wrap--posts">
                        @php
                            global $post;
                            $page_slug = $post->post_name;
                        @endphp

                        @foreach($get_posts->posts as $post)
                            @php(setup_postdata($GLOBALS['post'] = $post))
                            @include('partials.list-item-post')
                            @php(wp_reset_postdata())
                        @endforeach
                    </div>

                    <div class="pagination">
                        {!! $pagination !!}
                    </div>
                @else 
                    <div class="row">
                        <div class="column sm-100">
                            <h4>Sorry, no posts were found.</h4>
                            <p><a href="{{ get_permalink( get_page_by_path('news') ) }}"><span class="icon icon-arrow-left"></span> View all News</a></p>
                        </div>
                    </div>
                @endif
            </div>
            <div class="column md-33">
                @include('partials.content-page')
            </div>
        </div>    
    </section>

  @endwhile
@endsection
