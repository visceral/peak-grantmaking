@extends('layouts.app')

@section('content')
  @while(have_posts()) @php(the_post())
    {{-- h1 is not displayed in design, but there should still be one on the page --}}
    <h1 class="screen-reader-text">{!! App::title() !!}</h1>

    @include('partials.content-page')

    @foreach( $get_staff as $staff )
        @if(Staff::getStaffUsers($staff->slug))
            <div class="row">
                <div class="column reveal">
                    <h2>{{ $staff->name }}</h2>
                    <hr />
                </div>
            </div>
            <div class="row list-wrap list-wrap--users">
                @foreach(Staff::getStaffUsers($staff->slug) as $user)
                    @include('partials.list-item-user')
                @endforeach
            </div>
        @endif
    @endforeach

  @endwhile
@endsection