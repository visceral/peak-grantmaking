@php
$journal_id        = (function_exists('get_field') && get_field('journal_download_id')) ? get_field('journal_download_id') : '';
$journal_download  = (function_exists('get_field') && get_field('journal_download')) ? get_field('journal_download') : '';
$about_issue       = (function_exists('get_field') && get_field('about_this_issue')) ? get_field('about_this_issue') : '';
$login_url         = esc_url( site_url('/login/?redirect='.get_permalink()) );
$access            = '';

// Get journal download access value
if( $journal_id ) {
    $access = get_package_data($journal_id, 'access');
}

// gated content
$no_js_cookie = '';
if( isset($_COOKIE['peak_download_gate']) ) {
	$no_js_cookie = 'no-resource-gate';
}
@endphp

<article @php(post_class(''))>
	<div class="row" style="display: none;">
		<div class="column xs-100">
			{!! App::breadcrumbs() !!}
		</div>
	</div>
	<div class="about-this-issue">
        <div class="container">
            <div class="row justify-center">
        		<div class="column md-33 text-center">
                    @if( $about_this_issue_image )
                        {!! $about_this_issue_image !!}
                    @endif
        		</div>
        		<div class="column xs-100 md-67 lg-50 introduction">
                    <h2 class="h3">About this issue</h2>
        			@if($about_issue)
        				{!! $about_issue !!}
        			@endif

                    {{-- Check if download access is set to all or gated, display template respectively --}}
                    @if($journal_id)
            			@if($access == '' || (is_array($access) && in_array('guest', $access)) )
            				{!! do_shortcode('[wpdm_package id="'.$journal_id.'" template="peak_download_journal_ungated"]') !!}
            			@else
            				@if(is_user_logged_in())
            					{!! do_shortcode('[wpdm_package id="'.$journal_id.'" template="5e8f08ad2a83b"]') !!}
            				@else
            					<a href="{!! $login_url !!}" class="btn">Download This Issue</a> <span style="margin-left:10px;"><i class="fas fa-lock"></i> Members Only</span>
            				@endif
            			@endif
                      @endif
        		</div>
                <div class="column xs-100 text-center m-t-64">
                    <h3>What's Inside</h3>
                </div>
            </div>
        </div>
	</div>

    @php(the_content())

</article>

{{-- More journals  --}}
@if($get_more_journals)
    <div class="more-journals">
        <section class="more-journals-content">
            <div class="container">
                <p class="h2 text-center reveal more-journals-content__heading">Past Journal Issues</p>
                <div class="row">
                    @foreach ($get_more_journals->posts as $post)
                        @php(setup_postdata($GLOBALS['post'] = $post))
                        @include('partials.more-journals-content')
                    @endforeach
                    @php(wp_reset_postdata())
                </div>
                <p class="text-center reveal">
                    <a href="<?php echo get_post_type_archive_link( 'journals' ); ?>" class="more-journals-content__btn btn">See All Issues</a>
                </p>
            </div>
        </section>
    </div>
@endif

<?php
/**
 * Adding JSON-LD for structured data and SEO
 *
 * Official website: 	http://json-ld.org/
 * Examples: 			http://jsonld.com/
 * Generator: 			https://www.schemaapp.com/tools/jsonld-schema-generator/
 * Testing: 			https://search.google.com/structured-data/testing-tool/u/0/
 * Google Docs: 		https://developers.google.com/search/docs/data-types/data-type-selector
 **/

$logo_url = get_site_icon_url(); // Feel free to customize. Otherwise will use WordPress site icon

if ( $logo_url && has_post_thumbnail() ) :
	// Images are required. We only continue if we have them.
	$featured_img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
	
	// JSON-LD markup for Articles
	$json_ld = array(

		// Required
		"@context" => "http://schema.org",
		"@type" => "Article",
		"publisher" => array(	 
			"@type" => "Organization",
			"url" => get_site_url(),
			"name" => get_bloginfo('name'),
			"logo" => array(
						"@type" => "ImageObject",
						"name" => get_bloginfo('name') . " Logo",
						"width" => "512",
						"height" => "512",
						"url" => $logo_url
			),
		),
		"headline" => get_the_title(),
		"author" => array(
			"@type" => "Person",
			"name" => get_the_author()
		),
		"datePublished" => get_the_date(),
		"image" => array(
			"@type" => "ImageObject",
			"url" => $featured_img[0],
			"width" => $featured_img[1],
			"height" => $featured_img[2]
		),

		// Recommended
		"dateModified" =>  get_the_modified_date(),
		"mainEntityOfPage" => get_permalink(),

		// Optional (Not sure if they affect SEO)
		// "url" => get_permalink(),
		// "description" => get_the_excerpt(),
		// "articleBody" => get_the_content(),
	);
	?>
	<script type='application/ld+json'>
		<?php echo json_encode( $json_ld ); ?>
	</script>
<?php endif; ?>