@php
$post_id          = get_the_id();
$featured_image   = App\get_aspect_ratio_image(4, 3, 'large');
$card_image_class = has_post_thumbnail() ? 'list-item--featured-image': '';
$type             = get_the_terms( $post_id, 'news_type' );
$meta_output      = $type ? '<p class="list-item__type"><strong>' . $type[0]->name . '</strong></p>' : '';

@endphp
<article class="column xs-100 reveal">
    <div class="list-item list-item--insights {{$card_image_class}}">
        <a href="{{ get_permalink() }}" class="list-item__link">
            @if(has_post_thumbnail())
                <div class="list-item__image image-zoom">
                    <div class="img-cover">
                        {!! $featured_image !!}
                    </div>
                </div>
            @endif
            <div class="list-item__content">
                <h3 class="list-item__title">{{ get_the_title() }}</h3>                
            </div>
        </a>
    </div>    
</article>


