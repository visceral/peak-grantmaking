<h6>Related Articles</h6>
    @foreach ($related_content as $post)
        @php(setup_postdata($GLOBALS['post'] = $post))
        @php ($related_content_link = get_the_permalink($post->ID))
        	<p><strong>{{$post->post_title}}<br>
        		<a href="{{$related_content_link}}">READ</a></strong></p>
        @php(wp_reset_postdata())
    @endforeach


