@if($principles_links)
	<h6>Principles for Peak Grantmaking</h6>
	<p>Read more insights related to this Principle:<br>
        {!! $principles_links !!}
    </p>
@endif