<section class="promo promo--newsletter no-print">
    <div class="container">
        <div class="row justify-center">
            <div class="column md-75">
                <div class="promo__icon"><span class="icon icon-envelope"></span></div>
                <p class="h2 text-center reveal promo__heading">Receive the latest insights & announcements via our communications</p>
                <a href="{{ get_permalink( get_page_by_path('stay-connected' ) ) }}" class="promo__btn">Stay Connected <span class="icon icon-arrow-right"></span></a>
            </div>
        </div>
    </div>
</section>