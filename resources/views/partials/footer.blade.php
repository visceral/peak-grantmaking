<div class="footer-promo">
  <div class="container">
    <div class="row">
      <div class="column md-60 lg-67">
        <h3 class="h2 footer-promo__title">Ready to join?</h3>
        <p class="footer-promo__copy">A community of 7,000 philanthropy professionals awaits. Start with a complimentary Individual Membership and gain access to insights, tools, and resources, plus our online community forum.</p>
      </div>
      <div class="column md-40 lg-33">
        <a href="{{ get_permalink( get_page_by_path('/join-us') ) }}#individual-memberships" class="footer-promo__btn">Join Now <span class="icon icon-arrow-right"></span></a>
      </div>
    </div>
  </div>
</div>

<footer class="footer no-print">
  <div class="container-fluid">
    <div class="row lg-reverse">
      <div class="column xs-100 md-50">
          <div class="text-right">
              <a href="https://www.greatplacetowork.com/certified-company/7039753" target="_blank" class="footer__social-proof"><img src="@asset('images/great-place.png')" width="52" height="88" /></a>
              <a href="https://www.guidestar.org/profile/74-3158155" target="_blank" class="footer__social-proof"><img src="@asset('images/candid-seal-platinum-2024.png')" width="100" height="100" /></a>
              <a href="https://www.charitynavigator.org/ein/743158155" target="_blank" class="footer__social-proof"><img src="@asset('images/charity-navigator.png')" width="100" height="100" /></a>
          </div>
      </div>
      <div class="column md-50 lg-25">
        @if (has_nav_menu('footer_2'))
          {!! wp_nav_menu(['theme_location' => 'footer_2', 'menu_class' => 'nav']) !!}
        @endif
      </div>
      <div class="column md-50 lg-25">
          @if (has_nav_menu('footer_1'))
            {!! wp_nav_menu(['theme_location' => 'footer_1', 'menu_class' => 'nav']) !!}
          @endif
      </div>
    </div>
    <div class="row">
      <div class="column xs-100">
        <hr>
      </div>
    </div>
    <div class="row lg-reverse footer__copyright">
      <div class="column xs-100 lg-50">
          <ul class="social-links list-inline">
              @if( get_theme_mod( 'twitter_url' ) )
                <li><a href="{{ get_theme_mod( 'twitter_url' ) }}" class="social-links__link social-link--twitter"><i class="icon-twitter icon">{{ __('Twitter', 'visceral') }}</i></a></li>
              @endif
              @if( get_theme_mod( 'linkedin_url' ) )
                <li><a href="{{ get_theme_mod( 'linkedin_url' ) }}" class="social-links__link social-link--linkedin"><i class="icon-linkedin icon" >{{ __('LinkedIn', 'visceral') }}</i></a></li>
              @endif
              <li><a href="mailto:info@peakgrantmaking.org" class="social-links__link social-link--mail"><i class="icon-mail icon" >{{ __('mail', 'visceral') }}</i></a></li>
          </ul>
      </div>
      <div class="column md-100 lg-50">
          <p class="">&copy; {{date('Y')}} | {{ get_theme_mod( 'copyright_textbox', get_bloginfo('name') )}}</p>
          @if (has_nav_menu('footer_legal'))
            {!! wp_nav_menu(['theme_location' => 'footer_legal', 'container_class' => 'list-inline footer__legal']) !!}
          @endif
      </div>
    </div>
  </div>
</footer>