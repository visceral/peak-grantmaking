@php
/**
 * Masthead - Singular Journal
 *
 */

$image_id 			   = App::mastheadImage(); // check if there is a masthead image set
$full_size_image_url   = '';
$placeholder_image_url = '';
$weekly_reads_link	   = get_term_link('weekly-reads','news_type');
$banner_image          = get_field('banner_image');

if($banner_image) {
	$full_size_image       = wp_get_attachment_image_src( $banner_image ,'full', true);
	$full_size_image_url   = $full_size_image[0];
	$placeholder_image     = wp_get_attachment_image_src( $banner_image,'medium', true);
	$placeholder_image_url = $placeholder_image[0];
} else {
	$full_size_image       = wp_get_attachment_image_src( 18718 ,'full', true);
	$full_size_image_url   = $full_size_image[0];
	$placeholder_image     = wp_get_attachment_image_src( 18718,'medium', true);
	$placeholder_image_url = $placeholder_image[0];
}
@endphp

<div class="masthead masthead--journals masthead--background img-bg">
	@if( $full_size_image_url )
		<div class="masthead__image img-bg" data-image-src="{{ $full_size_image_url }}">
			@if( $placeholder_image_url )
				<span class="masthead__overlay img-bg" style="background-image: url({{$placeholder_image_url}});"></span>
			@endif
		</div>
	@endif

	<div class="masthead__content container">
		<div class="row">
			<div class="column md-80">
				{!! App::breadcrumbs() !!}
				<h1 class="masthead__headline">{!! App::title() !!}</h1>
				{!! App::subtitle() !!}
			</div>
		</div>
	</div>
	<div class="masthead__navigation container">
		<div class="row">
			<div class="column md-66">
				<ul>
					<li><a class='all-insights-link' href="{!! $get_insight_permalink !!}">Insights</a></li>
					<li><a class='current-journal-link' href="{!! $get_latest_journal_permalink !!}">Journal</a></li>
					<li><a class='weekly-reads-link' href="{!! $get_insight_permalink !!}?news_type=weekly-reads">Weekly Reads</a></li>
					{{-- <li style="display:none;"><a class='weekly-reads-link' href="{!! $weekly_reads_link !!}">Weekly Reads</a></li> --}}
					<li><a class='news-link' href="{!! $get_insight_permalink !!}?news_type=news">News</a></li>
                    @if( get_page_by_path('/about-us/write-for-peak/') )
                        <li><a class='news-link' href="{!! get_permalink( get_page_by_path('/about-us/write-for-peak/') ) !!}">Write for PEAK</a></li>
                      @endif
				</ul>
			</div>
			<div class="column journal-select md-33">
    			<form action="{{ get_bloginfo('url') }}">
    				<label class="screen-reader-text" for="page_id">{{ __('Select Another Issue', 'visceral') }}</label>
                    <div class="select_wrapper">
                    	<select name="page_id" id="page_id" class="input--alt" onchange="this.form.submit()">
    	                    <option value="">{{ __('Select Another Issue', 'visceral') }}</option>
    	                    @foreach ($get_all_journals->posts as $post)
    	                		@php(setup_postdata($GLOBALS['post'] = $post))
    	                        
    	                        <option value="{{ get_the_id() }}" >{{ get_the_title() }}</option>
    	                     @endforeach
                         @php(wp_reset_postdata())
                    	</select>
                	</div>
                    <div class="js-show">
                        <input type="submit" value="Submit">
                    </div>
    			</form>
			</div>
		</div>
	</div>
</div>