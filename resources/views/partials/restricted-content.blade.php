@php
    $content = get_the_content();
    $restrictedcontent = strip_shortcodes( $content );
    $restrictedcontent = wp_trim_words( $content , '150', '' );
@endphp

<div class='post__restricted_content'>
    {!! $restrictedcontent !!}
</div>
<div class='post__restricted_login'>
<div class='post__restricted_icon'><i class="fas fa-lock"></i></div>
<h4>This content is exclusive to members.</h4>
<div class='row'>
    <div class='column xs-100 md-40 member_login'>
        <h5>Login for access</h5>
        @php echo do_shortcode('[members_login_form /]'); @endphp
    </div>
    <div class='column xs-100 md-60 member_join'>
        <h5>Join the PEAK Community</h5>
        <p>Start with Individual Membership, complimentary to all grantmaking professionals. You'll unlock access to insights like this one, along with member-exclusive tools and resources, our CONNECT community, and more.</p>
         <p><a href="@php(bloginfo('url'))"/join-us/#individual-memberships' class='btn'>Apply Now</a></p>
    </div>
</div>
</div>
<script>
    (function ($) {
        $(document).ready(function () {
            $('#user_login').attr('placeholder', 'Email Address');
            $('#user_pass').attr('placeholder', 'Password');
        });
    })(jQuery);
</script>