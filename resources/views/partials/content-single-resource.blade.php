@php
// gated content
$no_js_cookie = '';
if( isset($_COOKIE['peak_download_gate']) ) {
	$no_js_cookie = 'no-resource-gate';
}
@endphp

<article @php(post_class('container'))>
	<div class="row">
		<div class="column xs-100">
			{!! App::breadcrumbs() !!}
		</div>
	</div>
	<div class="row">
		<div class="column md-25 lg-33">
			@if ($thumbnail_image)
				<div class="post__image text-center reveal"><img src="{{ $thumbnail_image['url'] }}" alt="{{ $thumbnail_image['alt'] }}" width="{{ $thumbnail_image['width'] }}" height="{{ $thumbnail_image['height'] }}"></div>
            @endif
            
            {{-- CTA buttons --}}
            @if( function_exists('get_field') && get_field('call_to_actions') )
                {!! get_field('call_to_actions') !!}
            @endif
		</div>
		<div class="column md-75 lg-67">
			<header class="post__header">
                <p class="post__label">{!! $types !!}</p>
                <h1 class="entry-title">{{ get_the_title() }}</h1>
                {!! $members !!}
			</header>

			{{-- gated content --}}
			<div class="modal-bg">
				<div class="modal download-modal">
					<i class="icon-cancel" aria-label="Close">Close</i>
					{!! do_shortcode('[gravityform id="3" title="false" description="false" ajax="true"]'); !!}
				</div>
			</div>

			<div class="post__content">
				@php(the_content())
			</div>

			@include('partials.entry-meta')	
		</div>
	</div>
</article>

@include('partials.promo-news')

@includeWhen($related_content, 'partials.related-content')

<?php
/**
 * Adding JSON-LD for structured data and SEO
 *
 * Official website: 	http://json-ld.org/
 * Examples: 			http://jsonld.com/
 * Generator: 			https://www.schemaapp.com/tools/jsonld-schema-generator/
 * Testing: 			https://search.google.com/structured-data/testing-tool/u/0/
 * Google Docs: 		https://developers.google.com/search/docs/data-types/data-type-selector
 **/

$logo_url = get_site_icon_url(); // Feel free to customize. Otherwise will use WordPress site icon

if ( $logo_url && has_post_thumbnail() ) :
	// Images are required. We only continue if we have them.
	$featured_img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
	
	// JSON-LD markup for Articles
	$json_ld = array(

		// Required
		"@context" => "http://schema.org",
		"@type" => "Article",
		"publisher" => array(	 
			"@type" => "Organization",
			"url" => get_site_url(),
			"name" => get_bloginfo('name'),
			"logo" => array(
						"@type" => "ImageObject",
						"name" => get_bloginfo('name') . " Logo",
						"width" => "512",
						"height" => "512",
						"url" => $logo_url
			),
		),
		"headline" => get_the_title(),
		"author" => array(
			"@type" => "Person",
			"name" => get_the_author()
		),
		"datePublished" => get_the_date(),
		"image" => array(
			"@type" => "ImageObject",
			"url" => $featured_img[0],
			"width" => $featured_img[1],
			"height" => $featured_img[2]
		),

		// Recommended
		"dateModified" =>  get_the_modified_date(),
		"mainEntityOfPage" => get_permalink(),

		// Optional (Not sure if they affect SEO)
		// "url" => get_permalink(),
		// "description" => get_the_excerpt(),
		// "articleBody" => get_the_content(),
	);
	?>
	<script type='application/ld+json'>
		<?php echo json_encode( $json_ld ); ?>
	</script>
<?php endif; ?>