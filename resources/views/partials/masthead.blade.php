@php
// posts that don't need a masthead
if( is_front_page() || is_page( array('resources') ) || is_post_type_archive(array('journals','tribe_events')) || is_author() || is_singular( array('resource', 'post', 'tribe_events') ) ) return;

/**
 * Masthead
 *
 * First we select the right hero image to load
 * Then we load in the small version as a placeholder, and the large version lazy loaded in.
 *
 */

// Set up variables
$image_id 			   = App::mastheadImage(); // check if there is a masthead image set
$bg                    = '';
$parent_id             = '';
$full_size_image_url   = '';
$placeholder_image_url = '';
$id                    = get_the_ID();
$parent_id 			   = get_post_ancestors( $id );
$parent 			   = $parent_id ? end( ( $parent_id ) )    : '';
$top_level 			   = ( !is_page('job-board') && is_page() && empty($parent) ) ? true: false;
$mast_top_level        = $top_level ? ' masthead--top-level' : '';
$color 			       = 'teal';
$content 			   = $top_level ? 'md-75 lg-50' : 'md-80';
$weekly_reads_link	   = get_term_link('weekly-reads','news_type');

// set color for page and children
if( $top_level ) {
	$color = get_field('color');
} elseif( !$top_level ) {
	$color = get_field('color', $parent);
} else {
	$color = 'teal';
}
// otherwise, color is color set on parent page

// By now, we should have an image ID to use. Let's use it to get some URLs.
if ( $image_id != '' ) {
	$full_size_image       = wp_get_attachment_image_src( $image_id,'full', true);
	$full_size_image_url   = $full_size_image[0];
	$placeholder_image     = wp_get_attachment_image_src( $image_id,'medium', true);
	$placeholder_image_url = $placeholder_image[0];
	$bg 				   = ' masthead--background img-bg';
}

// There are templates need custom fallback images
if ( is_404() ) {
	// For static headers, we just load the same image for main and placeholder.
	$full_size_image_url = $placeholder_image_url = get_stylesheet_directory_uri() . '/dist/images/header-fallback.jpg';
}
@endphp



{{-- This is the default masthead for child pages/no image set --}}
<div class="masthead{{ $bg }}{{ $mast_top_level }}">
	@if( is_page('job-board') || ($full_size_image_url && !$mast_top_level) )
		<div class="masthead__image img-bg" data-image-src="{{ $full_size_image_url }}">
			@if( $placeholder_image_url )
				<span class="masthead__overlay img-bg" style="background-image: url({{$placeholder_image_url}});"></span>
			@endif
		</div>
        @if( !is_page_template('views/template-blank-masthead.blade.php') )
    		<div class="masthead__decor bg-{{ $color }}">
    			<svg width="439px" height="764px" viewBox="0 0 439 764" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    				<g id="Designs" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
    					<g id="1.1-Implement-Better-Practices" transform="translate(-390.000000, -136.000000)" fill="#50BFC4" fill-rule="nonzero">
    						<polygon id="Triangle" points="829 136 390.085937 900 390.085937 136"></polygon>
    					</g>
    				</g>
    			</svg>
    		</div>
        @endif
	@endif

	<div class="masthead__content container">
		<div class="row">
			<div class="column {{ $content }}">
				@if( is_page('job-board') )
					{!! App::breadcrumbs() !!}
					<h1 class="masthead__headline text-{{ $color }}">{!! App::title() !!}</h1>
					{!! App::subtitle() !!}
				@else
					<h1 class="masthead__headline text-{{ $color }}">{!! App::title() !!}</h1>
					{!! App::breadcrumbs() !!}
					{!! App::subtitle() !!}
				@endif
			</div>
		</div>
	</div>
</div>