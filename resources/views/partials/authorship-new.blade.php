<h6>Meet the Author<?php echo ($author['count'] != 1 ? 's' : '') ?></h6>

@if( !empty($custom_authors) )
    {!! $custom_authors !!}
@else
    @if( $author['image'] )
        <div class="authorship__image text-center">
            <div class="img-circle">{!! $author['image'] !!}</div>
        </div>
    @endif
    <div class="authorship__content">
        <p class="authorship__name">{!! $author['author'] !!}</p>
        @if( $author['bio'] )
            {!! wp_trim_words( $author['bio'], 55 ) !!}
        @endif
    </div>
@endif