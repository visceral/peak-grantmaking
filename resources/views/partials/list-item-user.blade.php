@php
// User data
$image   = '';
$name    = ($user->first_name && $user->last_name) ? $user->first_name . ' ' . $user->last_name : '';
$title   = get_field('job_title', 'user_' . $user->ID);
$org     = get_field('organization', 'user_' . $user->ID);
$email   = $user->user_email;
$phone   = get_field('phone_number', 'user_' . $user->ID);
$twitter = get_field('twitter', 'user_' . $user->ID);
$bio     = get_field('short_bio', 'user_' . $user->ID);

if( get_field('image', 'user_' . $user->ID) ) {
    // ACF field should be set to return image array
    $image = get_field('image', 'user_' . $user->ID);
    $image = wp_get_attachment_image_src( $image, 'medium_large' );
    $class = ( ($image[2] / $image[1]) > 1 ) ? 'portrait' : '';
    $image = '<img src="' . $image[0] . '" width="' . $image[1] . '" height="' . $image[2] . '" alt="' . $name . '" class="' . $class . '">';
}

$column  = $image ? 'sm-67 md-100 lg-60' : 'sm-67 md-100';

@endphp

<article class="column xs-100 md-50 reveal">
    <div class="row list-item list-item--user">
        @if($image)
            <div class="sm-33 md-100 lg-40 column">
                <a href="{{ get_author_posts_url( $user->ID ) }}" class="list-item__link list-item__image img-circle">
                    {!! $image !!}
                </a>
            </div>
        @endif
        <div class="{{ $column }} column">
            <div class="list-item__content">
                <h3 class="list-item__name"><a href="{{ get_author_posts_url( $user->ID ) }}" class="list-item__link">{{ $name }}</a></h3>
                <div class="list-item__meta">
                    @if($title || $org )
                        @if($title)
                            <p class="list-item__title"><strong>{{ $title }}</strong></p>
                        @endif
                        @if($org)
                            <p class="list-item__title"><strong>{{ $org }}</strong></p>
                        @endif
                    @endif
                    {{-- don't display placeholder emails --}}
                    @if( strpos( $email, 'info+' ) === false )
                        <p class="list-item__email"><a href="mailto:{{ $email }}">Email {{ $name }}</a></p>
                    @endif
                    @if($phone)
                        <p class="list-item__phone">{{ $phone }}</p>
                    @endif
                    @if($twitter)
                        <p class="list-item__twitter"><a href="https://twitter.com/{{ $twitter }}" target="_blank" rel="nofollow">{{ $twitter }}</a></p>
                    @endif
                </div>
                {!! $bio !!}
            </div>
        </div>
    </div>
</article>