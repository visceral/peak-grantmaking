<section class="in-this-issue-content">
    <div class="row">
        <div class="column text-right md-40">
            <p class="h2 reveal related-content__heading">In This Issue</p>
            <p class='in-this-issue-content__description'>What's new and notable from PEAK Grantmaking – from exclusive member benefits, to member news, events highlights, and what's ahead.</p>
        </div>
        <div class="column md-60">
            <article class="reveal">
                @php
                    if( have_rows('in_this_issue_content') ) :
                        while ( have_rows('in_this_issue_content') ) : the_row();

                        $post_id   = get_sub_field('post');
                        $permalink = get_permalink($post_id);
                        $title     = get_the_title( $post_id );
                        $excerpt   = get_the_excerpt( $post_id );
                @endphp
                    <div class="row list-item--inthisissue">
                        <div class="column xs-25">
                            <a href="{!! $permalink !!}">
                                @if(has_post_thumbnail())
                                    {!! get_the_post_thumbnail($post_id,'thumbnail') !!}
                                @endif
                            </a>
                        </div>
                        <div class="column xs-75">
                            <p class="heading--inthisissue">{!! the_sub_field('heading') !!}</p>
                            
                            <a href="{!! $permalink !!}">
                                <h6 class="title--inthisissue">{!! $title !!}</h6>
                            </a>
                            <p>{!! $excerpt !!}</p>
                        </div>
                    </div>
                @php
                        endwhile;
                    endif;
                @endphp
            </article>
        </div>
    </div>
</section>