@php
// User data
$user = get_userdata($ID);
$image   = '';
$name    = ($user->first_name && $user->last_name) ? $user->first_name . ' ' . $user->last_name : $user->nickname;
$title   = get_field('job_title', 'user_' . $ID);
$position = get_field('position', 'user_' . $ID);
$org     = get_field('organization', 'user_' . $ID);
$phone   = get_field('phone_number', 'user_' . $ID);
$twitter = get_field('twitter', 'user_' . $ID);
$bio     = get_field('short_bio', 'user_' . $ID);

if( get_field('image', 'user_' . $ID) ) {
    // ACF field should be set to return image array
    $image = get_field('image', 'user_' . $ID);
    $image = wp_get_attachment_image_src( $image, 'large' );
    $class = ( ($image[2] / $image[1]) > 1 ) ? 'portrait' : '';
    $image = '<img src="' . $image[0] . '" width="' . $image[1] . '" height="' . $image[2] . '" alt="' . $name . '" class="' . $class . '">';
}

$column = $image ? 'sm-67 md-100 lg-67' : 'sm-67 md-100';
@endphp

<article class="column xs-100 md-50 reveal">
    <div class="row list-item list-item--user list-item--board">
        @if($image)
            <div class="sm-33 md-100 lg-33 column">
                <a href="{{ get_author_posts_url( $ID ) }}" class="list-item__link list-item__image img-circle">
                    {!! $image !!}
                </a>
            </div>
        @endif
        <div class="{{ $column }} column">
            <div class="list-item__content">
                <h4 class="list-item__name"><a href="{{ get_author_posts_url( $ID ) }}" class="list-item__link">{{ $name }}</a></h3>
                <div class="list-item__meta">
                    @if( $title || $org || $position )
                        @if($title)
                            <p class="list-item__title">{{ $title }}</p>
                        @endif
                        @if($org)
                            <p class="list-item__title">{{ $org }}</p>
                        @endif
                        @if($position)
                            <p class="list-item__title"><strong>{{ $position }}</strong></p>
                        @endif
                    @endif
                    @if($phone)
                        <p class="list-item__phone">{{ $phone }}</p>
                    @endif
                    @if($twitter)
                        <p class="list-item__twitter"><a href="https://twitter.com/{{ $twitter }}" target="_blank" rel="nofollow">{{ $twitter }}</a></p>
                    @endif
                </div>
                {!! $bio !!}
            </div>
        </div>
    </div>
</article>