<header class="header">
    <div class="header__inner container-fluid">
        <a class="header__brand" href="{{ home_url('/') }}">
            <span class="screen-reader-text">PEAK Grantmaking</span>
            @include('graphics.logo')
        </a>

        <nav class="header__navigation">
            @if (has_nav_menu('quicklinks'))
                {!! wp_nav_menu(['theme_location' => 'quicklinks', 'container' => '', 'menu_class' => 'header__quicklinks']) !!}
            @endif
            @if (has_nav_menu('primary_navigation'))
                {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'container' => '', 'menu_class' => 'header__menu']) !!}
            @endif
            <a class="header__search-button icon-search" href="{{ site_url() }}/?s=" title="@php( _e('Search', 'visceral') )" aria-label="Search"><?php _e('Search', 'visceral'); ?></a>
        </nav>

        <label class="mobile-nav__icon no-print" id="mobile-nav-icon" for="nav-toggle" tabindex="0">
            <span class="screen-reader-text"><?php _e('Navigation Toggle', 'visceral'); ?></span>
            <div>
                <div class="mobile-nav__menu-line"></div>
                <div class="mobile-nav__menu-line"></div>
                <div class="mobile-nav__menu-line"></div>
                <div class="mobile-nav__menu-line"></div>
            </div>	
        </label>
    </div>

    <div class="header__search">
        <div class="container-fluid">
            <form role="search" class="header__search-form searchform" method="get" action="{{ site_url() }}">
                <label><span class="screen-reader-text"><?php _e('Search', 'visceral'); ?></span>
                    <input type="text" placeholder="<?php _e('Type here and hit enter', 'visceral'); ?>..." name="s" autocomplete="off" spellcheck="false" autofocus>
                </label>
                <input type="submit" class="screen-reader-text" value="<?php _e('Submit', 'visceral'); ?>">
            </form>
            <i class="header__search-close icon-cancel" aria-label="Close"><?php _e('Close', 'visceral'); ?></i>
        </div>
    </div>
</header>

<input type="checkbox" id="nav-toggle">

@include('partials.mobile-nav')