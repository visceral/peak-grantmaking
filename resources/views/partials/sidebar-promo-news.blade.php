<section class="promo promo--newsletter no-print">
	<div class="promo__icon"><div class="promo__star"></div></div>
	<p class="h2 text-center reveal promo__heading">Join our Community</p>
	<p>Start with Individual Membership, complimentary to all grantmaking professionals. Unlock access to insights, resources, our CONNECT community, and more.</p>
	<a href="{{ get_permalink( get_page_by_path('join-us' ) ) }}/#individual-memberships" class="btn">Apply Today </a>
</section>