<footer class="post__footer row">
      <div class="column md-100">
        <div class="post__meta">
          @if( get_post_type() == 'resource' )
            <p class="post__date">Published <time class="" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time></p>
            @if( $types )
                <p class="post__categories"><strong>Types: </strong>{!! $types !!}</p>
            @endif

            @if( $topics )
                <p class="post__categories"><strong>Topics: </strong>{!! $topics !!}</p>
            @endif

            @if( $principles )
                <p class="post__categories"><strong>Principle: </strong>{!! $principles !!}</p>
            @endif
          @else 
            <p class="post__date"><time class="" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time></p>
          @endif
          
        </div>
        <div class="post__share">
          <p><strong>Share</strong></p>
          <ul class="social-links list-inline">
              {{-- <li><a href="https://twitter.com/share?text={{ get_the_title() }}&url={{ get_permalink() }}" class="social-links__link social-link--twitter"><i class="fab fa-sm fa-twitter"></i></a></li> --}}
              {{-- <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ get_permalink() }}" class="social-links__link social-link--facebook"><i class="fab fa-sm fa-facebook-f"></i></a></li> --}}
              <li><a href="https://www.linkedin.com/cws/share?url={{ get_permalink() }}" class="social-links__link social-link--linkedin"><i class="fab fa-sm fa-linkedin-in"></i></a></li>
              <li><a href="mailto:?body={{ get_permalink() }}&subject=Shared%20from%20PEAKgrantmaking.org" class="social-links__link"><i class="far fa-sm fa-envelope"></i></a></li>
          </ul>
        </div>
        
      </div>
      <div class="post__navigation">
        @php(the_post_navigation(array(
        'prev_text' => '<i class="fas fa-caret-left"></i> %title',
        'next_text' => '%title <i class="fas fa-caret-right"></i>'
        )))
      </div>
</footer>