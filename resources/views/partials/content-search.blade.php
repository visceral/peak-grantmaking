@php
    $col = has_post_thumbnail() ? 'md-67' : 'md-100';
    if( get_post_type() == 'tribe_events' ) {
        $type = 'event';
    } elseif( get_post_type() == 'post' ) {
        $type = 'insight';
    } else {
        $type = get_post_type();
    }
@endphp

<div @php(post_class())>
  <div class="row list-item--search list-item--{{ get_post_type() }}">
    <div class="column {{ $col }}">
      <header>
        <p class="list-item__label">{{ $type }}</p>
        <h3 class=""><a href="{{ get_permalink() }}" class="">{!! get_the_title() !!}</a></h3>
      </header>
      <div class="list-item__content">
        @if( get_post_type() =='post' )
          <div class="list-item__meta">
            <p class="post__date"><time class="" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time></p>
          </div>
        @endif
        @php(the_excerpt())
      </div>
    </div>
    @if(has_post_thumbnail())
    <div class="column md-33">
      {{ the_post_thumbnail() }}
    </div>
    @endif
  </div>
</div>
