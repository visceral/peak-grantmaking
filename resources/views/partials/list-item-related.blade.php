@php
$post_id          = get_the_id();
$post_type        = ( get_post_type() == 'tribe_events' ) ? 'event' : get_post_type();
$featured_image   = App\get_aspect_ratio_image(4, 3, array('500', '300'));
$meta_output      = '';
$tax_term         = '';
$card_image_class = has_post_thumbnail() ? 'list-item--featured-image' : '';

if ($post_type === 'post') {
    $meta_output = '<span class="meta-post-type">' . get_the_date() . '</span>';
} elseif ($post_type === 'resource') {
    $type = get_the_terms( $post_id, 'resource_type' );
    $meta_output = '<span class="meta-post-type">' . $type[0]->name . '</span>';
} elseif ($post_type === 'event') {
    $meta_output = '<span class="meta-post-type">' . tribe_get_start_time ( $post_id, 'F j, Y' ) . ' @ ' . tribe_get_start_time() . '</span>';
}
@endphp

<article class="column xs-100 md-50 lg-33 reveal">
    <div class="list-item list-item--related list-item--{{$post_type}} {{$card_image_class}}">
        <a href="{{ get_permalink() }}" class="list-item__link">
            
            
            @if(has_post_thumbnail())
                <div class="image-zoom list-item__image">
                    <div class="img-cover">
                        {!! $featured_image !!}
                    </div>
                </div>
            @endif

            <div class="list-item__content">
                <h3 class="list-item__title">{!! get_the_title() !!}</h3>

                @if($meta_output)
                    <div class="list-item__meta">
                        {!! $meta_output !!}
                    </div>
                @endif
            </div>
        </a>
    </div>
</article>