@php
$post_id          = get_the_id();
$featured_image   = App\get_aspect_ratio_image(4, 3, 'large');
if(!isset($get_insight_filters->news_topic)) {
    $type             = App\get_primary_taxonomy_term( $post_id, 'news_topic' );
    $meta_output      = $type ? '<p class="list-item__type"><strong>' . $type['title'] . '</strong></p>' : '';
}
$card_image_class = has_post_thumbnail() ? 'list-item--featured-image' : '';
$restricted       = get_field('restrict_content');
$restricted_class = $restricted ? 'list-item--restricted' : '';
$restricted_out   = $restricted ? '<p class="list-item__members"><i class="fas fa-lock"></i> Members Only</p>' : '';
@endphp

<article class="column xs-100 sm-50 lg-33 reveal">
    <div class="list-item list-item--journals {{$card_image_class}} {{$restricted_class}} ">
        <a href="{{ get_permalink() }}" class="list-item__link">
            @if(has_post_thumbnail())
                <div class="list-item__image image-zoom">
                    <div class="img-cover">
                        {!! $featured_image !!}
                    </div>
                </div>
            @endif
            <div class="list-item__content @if(!has_post_thumbnail()) no-image @endif">
                {!! $meta_output !!}
                <h3 class="list-item__title">{!! get_the_title() !!}</h3>
                {!! $restricted_out !!}
            </div>
        </a>
    </div>    
</article>