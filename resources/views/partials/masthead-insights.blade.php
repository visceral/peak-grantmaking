@php
/**
 * Masthead - Insights
 *
 */

$image_id = App::mastheadImage(); // check if there is a masthead image set
if($image_id) {
    $full_size_image       = wp_get_attachment_image_src( $image_id,'full', true);
    $full_size_image_url   = $full_size_image[0];
    $placeholder_image     = wp_get_attachment_image_src( $image_id,'medium', true);
    $placeholder_image_url = $placeholder_image[0];
    $bg 				   = ' masthead--background img-bg';
}
$form_filters = App::formFilters();
$weekly_reads_link = get_term_link('weekly-reads','news_type');
@endphp

{{-- This is the default masthead for child pages/no image set --}}
<div class="masthead masthead--insights {{ $bg }}">
	@if($image_id)
		<div class="masthead__image img-bg" data-image-src="{{ $full_size_image_url }}">
			@if( $placeholder_image_url )
				<span class="masthead__overlay img-bg" style="background-image: url({{$placeholder_image_url}});"></span>
			@endif
		</div>
		<div class="masthead__decor bg-teal">
			<svg width="439px" height="764px" viewBox="0 0 439 764" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
				<g id="Designs" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					<g id="1.1-Implement-Better-Practices" transform="translate(-390.000000, -136.000000)" fill="#50BFC4" fill-rule="nonzero">
						<polygon id="Triangle" points="829 136 390.085937 900 390.085937 136"></polygon>
					</g>
				</g>
			</svg>
		</div>
	@endif

	<div class="masthead__content container">
		<div class="row">
			<div class="column md-80">
				{!! App::breadcrumbs() !!}
				<h1 class="masthead__headline">{!! App::title() !!}</h1>
				{!! App::subtitle() !!}
			</div>
		</div>
	</div>

	<div class="masthead__navigation container">
		<div class="row">
			<div class="column md-66">
				<ul>
					<li><a class='all-insights-link' href="{{ get_permalink() }}">Insights</a></li>
					<li><a class='current-journal-link' href="{{ $get_latest_journal_permalink }}">Journal</a></li>
					<li><a class='weekly-reads-link' href="{{ get_permalink() }}?news_type=weekly-reads">Weekly Reads</a></li>
					<li><a class='news-link' href="{{ get_permalink() }}?news_type=news">News</a></li>
                    @if( get_page_by_path('/about-us/write-for-peak/') )
                        <li><a class='news-link' href="{{ get_permalink( get_page_by_path('/about-us/write-for-peak/') ) }}">Write for PEAK</a></li>
                      @endif
				</ul>
			</div>
		</div>
        <div class="row"> {{-- .journal-select --}}
            <div class="column">
                <form action="{{ get_permalink() }}"  class="filters filters--insights">
                    <div class="select-wrapper">
        				<label class="screen-reader-text" for="news_topic">{{ __('Select a Topic', 'visceral') }}</label>
                    	<select name="news_topic" id="news_topic" class="input--alt" onchange="this.form.submit()">
    	                    <option value="">{{ __('Select a Topic', 'visceral') }}</option>
    	                    @if($topics)
    	                        @foreach($topics as $topic)
    	                        	@php( $topic_selected = ($form_filters->news_topic === $topic->slug ) ? 'selected' : '') 
    	                        	<option value="{{ $topic->slug }}" {{ $topic_selected }}>{!! $topic->name !!}</option>
    	                        @endforeach
    	                    @endif
                    	</select>
                	</div>
                    <div class="select-wrapper">
                        <label class="screen-reader-text" for="news_principle">{{ __('Principle', 'visceral') }}</label>
                        <select name="news_principle" id="news_principle" class="input--alt" onchange="this.form.submit()">
                            <option value="">{{ __('Select a Principle', 'visceral') }}</option>
                            @if($principles)
                                @foreach($principles as $principle)  
                                    @php( $principle_selected = ($form_filters->news_principle == $principle->ID ) ? 'selected' : '') 
                                    <option value="{{ $principle->ID }}" {{ $principle_selected }}>{{ $principle->post_title }}</option>
                                @endforeach
                            @endif
                        </select>
                        <div class="js-show">
                            <input type="submit" value="Submit">
                        </div>
                    </div>
				</form>
            </div>
        </div>
	</div>
</div>