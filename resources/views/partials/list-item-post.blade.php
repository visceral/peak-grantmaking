@php
$post_id          = get_the_id();
$featured_image   = App\get_aspect_ratio_image(4, 3, 'large');
$card_image_class = has_post_thumbnail() ? 'list-item--featured-image': '';
$type             = get_the_terms( $post_id, 'news_type' );
$meta_output      = $type ? '<p class="list-item__type"><strong>' . $type[0]->name . '</strong></p>' : '';

// Authorship
$author = '';
if( get_field('custom_author') ) {
    $author = get_field('custom_author');
} else {
    $author_id    = get_the_author_meta( 'ID' );
    $author_image = '';

    if( get_field('image', 'user_' . $author_id) ) {
        // ACF field should be set to return image array
        $author_image = get_field('image', 'user_' . $author_id);
        $author_image = wp_get_attachment_image_src( $author_image, array('36', '36') );
        $author_image = '<img src="' . $author_image[0] . '" width="' . $author_image[1] . '" height="' . $author_image[2] . '" alt="' . $author_image[3] . '" class="img-circle">';
    }

    $author = $author_image . get_the_author();
}

$thumbnail = $image['sizes'];
@endphp

<article class="column xs-100 sm-50 reveal">
    <div class="list-item list-item--post {{$card_image_class}}">
        <a href="{{ get_permalink() }}" class="list-item__link">
            <div class="list-item__image">
                @if(has_post_thumbnail())
                    <div class="img-cover">
                        {!! $featured_image !!}
                    </div>
                @endif
                {!! $meta_output !!}
            </div>

            <div class="list-item__content">
                <h3 class="list-item__title">{!! get_the_title() !!}</h3>
                <div class="list-item__meta">
                    <p class="list-item__date">{{ get_the_date() }}</p>
                    <p class="list-item__author">{!! $author !!}</p>
                </div>
            </div>
        </a>
    </div>
</article>