<div class="authorship reveal">
    @if( $image )
        <div class="authorship__image text-center">
            <a href="{{ get_author_posts_url($id) }}" class="img-circle">{!! $image !!}</a>
        </div>
    @endif
    <div class="authorship__content">
        {!! wp_trim_words( $bio, 55 ) !!}
    </div>
</div>