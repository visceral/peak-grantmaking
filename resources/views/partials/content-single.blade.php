<article {{ post_class('container') }}>
	<div class="row justify-center">
		<div class="column xs-100">
			{!! App::breadcrumbs() !!}
		</div>
	</div>
	<div class="row">
		<div class="column xs-100">
			<header class="post__header">
				<div class="header_content">
					<div class='top_header'>
						{!! $post_meta !!}
						<h1 class="post__title">{{ get_the_title() }}</h1>
						<p class="post__author">By {!! $author['author'] !!}</p>
						@include('partials.entry-meta-header')
					</div>
					<div class="top_excerpt">
						@if(get_field('use_excerpt'))
							{!! the_excerpt() !!}
						@endif
					</div>
				</div>
				<div class="post__image">
					@if( $featured_image && $show_image )
						{!! $featured_image !!}
					@endif
				</div>				
			</header>
			<div class="row">
				<div class="column md-67">
					<div class="post__content">
						@if($has_access)
							@php(the_content())
						@else
							@include('partials.restricted-content')
						@endif
					</div>
					@if(!$restrictedcontent)
						@include('partials.entry-meta')
					@endif
				</div>
				<div class="column md-33 post__sidebar">
					<div class="post__sidebar__content">

						@include('partials.authorship-new')

						@if($principles_links)
                        	<h6>Principles for Peak Grantmaking</h6>
                        	<p>Read more insights related to this Principle:<br>
                                {!! $principles_links !!}
                            </p>
                        @endif

                        @includeWhen($topics,'partials.sidebar-related-topics')

						@if($has_access)
							@includeWhen($related_content, 'partials.sidebar-related-articles')
							@include('partials.sidebar-promo-news')
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</article>

@includeWhen($related_content, 'partials.related-content')

<?php
	/**
	 * Adding JSON-LD for structured data and SEO
	 *
	 * Official website: 	http://json-ld.org/
	 * Examples: 			http://jsonld.com/
	 * Generator: 			https://www.schemaapp.com/tools/jsonld-schema-generator/
	 * Testing: 			https://search.google.com/structured-data/testing-tool/u/0/
	 * Google Docs: 		https://developers.google.com/search/docs/data-types/data-type-selector
	 **/

	$logo_url = get_site_icon_url(); // Feel free to customize. Otherwise will use WordPress site icon

	if ( $logo_url && has_post_thumbnail() ) :
		// Images are required. We only continue if we have them.
		$featured_img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
		
		// JSON-LD markup for Articles
		$json_ld = array(
			// Required
			"@context"  => "http: //schema.org",
			"@type"     => "Article",
			"publisher" => array(	 
				"@type" => "Organization",
				"url"   => get_site_url(),
				"name"  => get_bloginfo('name'),
				"logo"  => array(
					"@type" => "ImageObject",
						"name"   => get_bloginfo('name') . " Logo",
						"width"  => "512",
						"height" => "512",
						"url"    => $logo_url
				),
			),
			"headline" => get_the_title(),
			"author"   => array(
				"@type" => "Person",
				"name"  => $author,
                // "image" => $author_image
			),
			"datePublished" => get_the_date(),
			"image"         => array(
				"@type"  => "ImageObject",
				"url"    => $featured_img[0],
				"width"  => $featured_img[1],
				"height" => $featured_img[2]
			),

			// Recommended
			"dateModified"     => get_the_modified_date(),
			"mainEntityOfPage" => get_permalink(),

			// Optional (Not sure if they affect SEO)
			// "url"         => get_permalink(),
			// "description" => get_the_excerpt(),
			// "articleBody" => get_the_content(),
		);
		

	    echo '<script type="application/ld+json">';
			json_encode( $json_ld );
		echo '</script>';
	endif; ?>
