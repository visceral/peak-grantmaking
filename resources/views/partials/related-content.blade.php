<section class="related-content">
    <div class="container">
        <p class="h2 text-center reveal related-content__heading">You might also like</p>
        <div class="row">
            @foreach ($related_content as $post)
                @php(setup_postdata($GLOBALS['post'] = $post))
                @include('partials.list-item-related')
                @php(wp_reset_postdata())
            @endforeach
        </div>
    </div>
</section>