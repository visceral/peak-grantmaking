{{-- graphic --}}
<div id="graphic-container" class="phases phases--parents">
    <div id="outer-circle" class="phases phases--children">
        @foreach( $phase_labels as $label )
            <a href="#" class="phase wheel" data-reveal-id="{{ preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $label['label'])) }}"></a>
        @endforeach
    </div>
    <div id="background-circle">
        <img src="@asset('images/cross-cutting.svg')" />
    </div>
    <div id="inner-circle" class="competencies competencies--labels">
        @foreach($competency_labels as $label)
            <a href="#" data-reveal-id="{{ preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $label['label'])) }}">{{$label['label']}}</a><br />
        •••<br />
        @endforeach
    </div>
</div>

{{-- Modals --}}
@foreach( $phases as $phase )
    @include('partials.modal-model', $phase)
@endforeach

@foreach( $competencies as $competency )
    @include('partials.modal-model', $competency)
@endforeach