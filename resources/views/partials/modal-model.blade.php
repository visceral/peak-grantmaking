<div id="{{ preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $label)) }}" class="reveal-modal xlarge">
    <div class="row">
        @if($image)
            <div class="column sm-20 text-center">
                <div class="icon-bar">
                    <img src="{{ $image[0] }}" width="{{ $image[1] }}" height="{{ $image[2] }}"/>
                </div>
            </div>
        @endif
        <div class="column sm-80">
            <div class="row">
                <div class="column sm-100">
                    {!! $heading !!}
                </div>
                <div class="column sm-60 border-right">
                    {!! $main !!}
                </div>
                <div class="column sm-40">
                    <div class="side-bar">
                        {!! $sidebar !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="close-reveal-modal">&#215;</a>
</div>