   <div class="row">
    <div class="column sm-100 md-60">
        <div class="post__meta">
            @if( get_post_type() == 'resource' )
                <p class="post__date">Published <time class="" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time></p>
            @else 
                <p class="post__date"><time class="" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time></p>
            @endif
        </div>
    </div>
    <div class="column">
        <div class="post__share">
          <ul class="social-links list-inline">
              {{-- <li><a href="https://twitter.com/share?text={{ get_the_title() }}&url={{ get_permalink() }}" class="social-links__link social-link--twitter"><i class="fab fa-sm fa-twitter"></i></a></li> --}}
              {{-- <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ get_permalink() }}" class="social-links__link social-link--facebook"><i class="fab fa-sm fa-facebook-f"></i></a></li> --}}
              <li><a href="https://www.linkedin.com/cws/share?url={{ get_permalink() }}" class="social-links__link social-link--linkedin"><i class="fab fa-sm fa-linkedin-in"></i></a></li>
              <li><a href="mailto:?body={{ get_permalink() }}&subject=Shared%20from%20PEAKgrantmaking.org" class="social-links__link"><i class="far fa-sm fa-envelope"></i></a></li>
          </ul>
        </div>
      </div>
    </div>