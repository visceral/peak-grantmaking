@php
$post_id              = get_the_id();
$journal_image        = get_field('about_this_issue_image',$post_id );
$size                 = 'more-journals';
$journal_number       = get_field('journal_issue_number');
@endphp
<article class="column xs-100 sm-50 lg-25 reveal">
    <div class="list-item list-item--journals {{$card_image_class}} {{$restricted_class}} ">
        <a href="{{ get_permalink() }}" class="list-item__link">
            
                <div class="list-item__image image-zoom">
                    <div class="img-cover">
                        @if( $journal_image )
                           @php echo wp_get_attachment_image( $journal_image, $size ); @endphp
                        @endif
                    </div>
                </div>
           
            <div class="list-item__content">
                {!! $meta_output !!}
                <h3 class="list-item__title">Issue {!! $journal_number !!}<br>{{ get_the_title() }}</h3>
            </div>
        </a>
    </div>    
</article>