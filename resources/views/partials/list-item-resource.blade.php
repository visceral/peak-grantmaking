@php
    $post_id          = get_the_id();
    $featured_image   = App\get_aspect_ratio_image(4, 3, 'large');
    $type             = get_the_terms( $post_id, 'resource_type' );
    $meta_output      = $type ? '<p class="list-item__type">' . $type[0]->name . '</p>' : '';
    $members          = '';
    $member_access    = '';
    $card_image_class = has_post_thumbnail() ? 'list-item--featured-image' : '';

    if( function_exists('get_field') && ($member_access = get_field('member_options')) ) {
        if( $member_access['value'] == 'organizations') {
            $members = '<p class="list-item__access"><small><strong><i class="fas fa-lock"></i></strong></small> Organization Members Only</p>';
        } elseif( $member_access['value'] == 'members') {
            $members = '<p class="list-item__access"><small><strong><i class="fas fa-lock"></i></strong></small> Members Only</p>';
        }
    }

    $locked = ( $member_access['value'] == 'organizations' || $member_access['value'] == 'members' ) ? 'list-item--locked' : '';
@endphp

<article class="column xs-100 sm-50 lg-33 reveal">
    <div class="list-item list-item--resource {{$card_image_class}} {{$locked}}">
        <a href="{{ get_permalink() }}" class="list-item__link">
            @if(has_post_thumbnail())
                <div class="list-item__image img-cover">
                    {!! $featured_image !!}
                </div>
            @endif
            <div class="list-item__content">
                {!! $meta_output !!}
                <h3 class="list-item__title">{!! get_the_title() !!}</h3>
            </div>
            {!! $members !!}
        </a>
    </div>   
</article>