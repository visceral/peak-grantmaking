<div id="mobile-nav">
	<div class="mobile-nav no-print">
        @if (has_nav_menu('quicklinks'))
            {!! wp_nav_menu(['theme_location' => 'quicklinks', 'container' => '', 'after' => '<span class="icon icon-arrow-down sub-menu--open"></span>', 'menu_class' => 'mobile-nav__menu']) !!}
        @endif
        @if (has_nav_menu('primary_navigation'))
            {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'container' => '', 'after' => '<span class="icon icon-arrow-down sub-menu--open"></span>', 'menu_class' => 'mobile-nav__menu']) !!}
        @endif
        
        <form class="mobile-search" role="search" method="get" action="{!! site_url() !!}">
            <label class="mobile-search__input"><span class="screen-reader-text"><?php _e('Search', 'sage'); ?></span>
                <input type="text" placeholder="<?php _e('Search', 'sage'); ?>..." name="s" autocomplete="off" spellcheck="false" autofocus>
            </label>
            <button type="submit" class="mobile-search__submit">
                <span class="icon icon-search">Search</span>
            </button>
        </form>
	</div>
</div>