@php
    $post_id        = get_the_id();
    $location       = ($locations = wp_get_post_terms( $post_id, 'job_location', array( 'fields' => 'names') ) ) ? implode( ' | ', $locations ) : '' ;
    $job_url        = ( function_exists('get_field') && get_field('job_post_url') ) ? get_field('job_post_url') : '';
    $salary_range   = ( function_exists('get_field') && get_field('salary_range') ) ? get_field('salary_range') : '';
    $posted         = get_the_time('U');
    $posted_since   = human_time_diff($posted, current_time( 'U' ));
@endphp

<article class="list-item list-item--job reveal row">
    <div class="column md-75">
        <h3 class="list-item__title h4">{!! get_the_title() !!}</h3>
        <h5 class="list-item__location">{!! $location !!}</h5>
        @if( $salary_range )
            <p>{{ $salary_range }}</p>
        @endif
        <p><strong>Posted {{ $posted_since }} ago</strong></p>
    </div>
    <div class="column md-25">
        @if( $job_url )
            <a href="{{ $job_url }}" class="btn" target="_blank">Learn More</a>
        @endif
    </div>
</article>