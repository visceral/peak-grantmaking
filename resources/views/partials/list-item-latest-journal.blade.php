@php
    $journal_issue     = get_field('journal_issue_number');
    $subheadline       = get_field('subheadline');
    $about_this_issue  = get_field('about_this_issue');
    $image             = get_field('about_this_issue_image');
    if($image) {
        $featured_image = wp_get_attachment_image( $image, 'about-this-issue' );
    } else {
        $featured_image = '';
    }
@endphp

<div class="row about-this-issue">
    <div class="column md-25">
        @if( $featured_image )
            {!! $featured_image !!}
        @endif
    </div>
    <div class="column xs-100 md-75 introduction">
        <p class="journal-issue"><a href="{{ get_the_permalink() }}">Journal | Issue {{ $journal_issue }}</a></p>
        <h2>{!! get_the_title() !!}</h2>
        @if( $subheadline )
            <p class="subheadline">{{ $subheadline }}</p>
        @endif
        @if( $about_this_issue )
            {!! $about_this_issue !!}
        @endif
        <a class="btn" href="{{ get_the_permalink() }}">Go to the Journal</a>
    </div>
</div>