@php
/**
 * Single WPDM post
 * These posts should not be directly seen and redirected to the homepage;
 * see redirect_downloads_single() in filters.php
 * However, posts categorized as 'Video' are displayed as single posts
 *
 */
	$args = array(
	    'echo'           => false,
	    'redirect'       => get_the_permalink(), // login and redirect back to this post to view
	    'form_id'        => 'loginform',
	    'label_username' => __( 'Username' ),
	    'label_password' => __( 'Password' ),
	    'label_remember' => __( 'Remember Me' ),
	    'label_log_in'   => __( 'Log In' ),
	    'id_username'    => 'user_login',
	    'id_password'    => 'user_pass',
	    'id_remember'    => 'rememberme',
	    'id_submit'      => 'wp-submit',
	    'remember'       => true,
	    'value_username' => NULL,
		'value_remember' =>  false
	);

// Store a cookie for Download Packages visited. This will be used for the Member Views count
$wpdmpro_visited = [];
if ( isset( $_COOKIE['wpdmpro_visited'] ) ) {
    $wpdmpro_visited = json_decode($_COOKIE['wpdmpro_visited']);
}
if ( !in_array(get_the_ID(), $wpdmpro_visited) ) {
    $wpdmpro_visited[] = get_the_ID();
    setcookie( 'wpdmpro_visited', json_encode($wpdmpro_visited), time() + 3600, "/" );
}


$post_id = get_the_ID();
$wpdmpro_visited = ( isset($_COOKIE['wpdmpro_visited']) ) ? json_decode($_COOKIE['wpdmpro_visited']) : [];

if ( !current_user_can( 'administrator' ) && !current_user_can( 'editor' ) && !in_array( $post_id, $wpdmpro_visited ) ) { // If it's not an Admin or Editor, and the post has not already been viewed

    if( is_user_logged_in() ) {
        $user  = wp_get_current_user();
        $roles = ( array ) $user->roles;
        
        // If the user is an Organization Member
        if( in_array('organization_member', $roles) ) {
            $old_views = (int)get_post_meta( $post_id, 'organization_member_views', true ); // get current view value

            if ( $old_views === 0 || $old_views ) { // '0' returns falsey but is a valid value
        		update_post_meta($post_id, 'organization_member_views', $old_views + 1, $old_views);
        	} else {
        		add_post_meta($post_id, 'organization_member_views', $old_views + 1);
        	}
        }

        // If the user is an Individual Member
        if( in_array('individual_member', $roles) ) {
            $old_views = (int)get_post_meta( $post_id, 'individual_member_views', true ); // get current view value

            if ( $old_views === 0 || $old_views ) { // '0' returns falsey but is a valid value
        		update_post_meta($post_id, 'individual_member_views', $old_views + 1, $old_views);
        	} else {
        		add_post_meta($post_id, 'individual_member_views', $old_views + 1);
        	}
        }

        // If the user is a Consultant
         if( in_array('consultant_member', $roles) ) {
            $old_views = (int)get_post_meta( $post_id, 'consultant_views', true ); // get current view value

            if ( $old_views === 0 || $old_views ) { // '0' returns falsey but is a valid value
        		update_post_meta($post_id, 'consultant_views', $old_views + 1, $old_views);
        	} else {
        		add_post_meta($post_id, 'consultant_views', $old_views + 1);
        	}
        }
    } else {
        // Otherwise, if the user is a Non-Member (not logged in)
        $old_views = (int)get_post_meta( $post_id, 'non_member_views', true ); // get current view value

    	if ( $old_views === 0 || $old_views ) { // '0' returns falsey but is a valid value
    		update_post_meta($post_id, 'non_member_views', $old_views + 1, $old_views);
    	} else {
    		add_post_meta($post_id, 'non_member_views', $old_views + 1);
    	}
    }
}
@endphp
<article @php(post_class('container fill'))>
	<div class="row">
		<div class="column xs-100 sm-50">
			<div class="row">
				<div class="column">
					<div class="post__content">
						@if( current_user_can( 'manage_options' ) || wpdm_user_has_access( get_the_id() ) )
                            {{-- If user has access (admin or WPDM), display contents. --}}
							@php(the_content())
						@else
                            {{-- If user does not have access, display login form. --}}
                            <p>You must login to view this content.</p>
							{!! do_shortcode('[mo_oauth_login]') !!}
						@endif
					</div>					
				</div>
			</div>
			
		</div>
	</div>
</article>

<?php
	/**
	 * Adding JSON-LD for structured data and SEO
	 *
	 * Official website: 	http://json-ld.org/
	 * Examples: 			http://jsonld.com/
	 * Generator: 			https://www.schemaapp.com/tools/jsonld-schema-generator/
	 * Testing: 			https://search.google.com/structured-data/testing-tool/u/0/
	 * Google Docs: 		https://developers.google.com/search/docs/data-types/data-type-selector
	 **/

	$logo_url = get_site_icon_url(); // Feel free to customize. Otherwise will use WordPress site icon

	if ( $logo_url && has_post_thumbnail() ) :
		// Images are required. We only continue if we have them.
		$featured_img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
		
		// JSON-LD markup for Articles
		$json_ld = array(

			// Required
			"@context"  => "http: //schema.org",
			"@type"     => "Article",
			"publisher" => array(	 
				"@type" => "Organization",
				"url"   => get_site_url(),
				"name"  => get_bloginfo('name'),
				"logo"  => array(
					"@type" => "ImageObject",
						"name"   => get_bloginfo('name') . " Logo",
						"width"  => "512",
						"height" => "512",
						"url"    => $logo_url
				),
			),
			"headline" => get_the_title(),
			"author"   => array(
				"@type" => "Person",
				"name"  => get_the_author()
			),
			"datePublished" => get_the_date(),
			"image"         => array(
				"@type"  => "ImageObject",
				"url"    => $featured_img[0],
				"width"  => $featured_img[1],
				"height" => $featured_img[2]
			),

			// Recommended
			"dateModified"     => get_the_modified_date(),
			"mainEntityOfPage" => get_permalink(),

			// Optional (Not sure if they affect SEO)
			// "url"         => get_permalink(),
			// "description" => get_the_excerpt(),
			// "articleBody" => get_the_content(),
		);
		?>

		<script type='application/ld+json'>
			{{ json_encode( $json_ld ) }}
		</script>
	<?php endif; ?>
