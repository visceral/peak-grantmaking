@php
    $post_id       = get_the_id();
    $journal_issue = get_field('journal_issue_number');
    $journal_image = get_field('about_this_issue_image',$post_id );
    $size          = 'more-journals';
@endphp

<article class="column xs-100 sm-50 lg-25 reveal">
    <a href="<?php the_permalink(); ?>" class="list-item__link">
        @if( $journal_image )
            {!! wp_get_attachment_image( $journal_image, $size ) !!}
        @endif

        <div class="list-item__content_under">
            <p class="journal-issue-number">Issue {{ $journal_issue }}</p>
            <h3 class="list-item__title"> {{ get_the_title() }}</h3>
        </div>
    </a>
</article>