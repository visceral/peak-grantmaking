@extends('layouts.app')

@section('content')
  @while(have_posts()) @php(the_post())

    @include('partials.content-page')

    <form action="{{ get_permalink() }}" class="filters filters--jobs">
        <div class="row">
            <div class="column md-33 xl-20">
                @if($get_job_locations)
                    <label class="screen-reader-text" for="job_location">{{ __('Location', 'visceral') }}</label>
                    <select name="job_location" id="job_location" class="input--alt" onchange="this.form.submit()">
                        <option value="">{{ __('All Locations', 'visceral') }}</option>
                        @php( $job_location_selected = ($form_filters->job_location === 'remote' ) ? 'selected' : '')
                        <option value="remote" {{ $job_location_selected }}>{{ __('Remote', 'visceral') }}</option>
                        @foreach($get_job_locations as $job_location)
                            @php( $job_location_selected = ($form_filters->job_location === $job_location->slug ) ? 'selected' : '')
                            <option value="{{ $job_location->slug }}" {{ $job_location_selected }}>{!! $job_location->name !!}</option>
                        @endforeach
                    </select>
                @endif
            </div>

            <div class="column md-33 xl-25">
                <div class="filters__checkbox">
                    <input type="checkbox" id="salary_filter" name="salary_filter" value="salary_filter" {{ (is_object($form_filters) && $form_filters->salary_filter) ? 'checked' : '' }}  onchange="this.form.submit()">
                    <label for="salary_filter">{{ __('Show jobs with salaries first', 'visceral') }}</label>
                </div>
            </div>

            <div class="column text-right">
                <select name="per_page" id="per_page" class="per-page" onchange="this.form.submit()">
                    @foreach($per_page as $label => $value )
                        @php( $per_page_selected = ($form_filters->per_page == $value ) ? 'selected' : '')
                        <option value="{{ $value }}" {{ $per_page_selected }}>{{ $label }}</option>
                    @endforeach
                </select>
                <span>results per page</span>
            </div>
        </div>
        <div class="row">
            <div class="column md-100 text-right">
                @if($get_jobs->posts)
                    <h5 class="filters__count">{{$end . ' ' . __('of', 'visceral') . ' ' . $total}} postings</h5>
                @endif
            </div>

            <div class="column js-show">
                <input type="submit" value="Submit">
            </div>
        </div>
    </form>
                                        
    @if($get_jobs->posts)
        <div class="list-wrap list-wrap--jobs">
            @foreach($get_jobs->posts as $post)
                @php(setup_postdata($GLOBALS['post'] = $post))
                    @include('partials.list-item-job')
                @php(wp_reset_postdata())
            @endforeach
        </div>

        <div class="pagination">
            {!! $pagination !!}
        </div>
    @else
        <div class="row">
            <div class="column sm-100">
                <p>Sorry, no jobs were found.</p>
            </div>
        </div>
    @endif

  @endwhile
@endsection