@extends('layouts.app')

@section('content')

<div class="row">
    <div class="column xs-100">
        @if( is_array($department) && in_array( 'board', $department) )
            <p class="breadcrumbs reveal"><span class="icon icon-arrow-left"></span> <a href="{{ get_the_permalink( get_page_by_path('about-us/board-of-directors') ) }}" class="breadcrumbs__link">Board</a></p>
        @else
            {!! App::breadcrumbs() !!}
        @endif
    </div>
</div>

<div class="author-info row justify-center">
    <div class="column lg-75">
        <div class="row">
            @if( $headshot[0] )
                <div class="column sm-40 xl-25 text-center">
                    <div class="img-circle author-info__image">
                        <img src="{{ $headshot[0] }}" width="{{ $headshot[1] }}" height="{{ $headshot[2] }}" alt="{{ $headshot[3] }}" class="{{ $headshot[4] }}">
                    </div>
                </div>
            @endif
            <div class="column sm-50 lg-60">
                <h1 class="h3 author-info__name">{!! App::title() !!}</h1>
                @if($title || $org )
                    <p class="list-item__title">
                        @if($title)
                            <span class=""><strong>{{ $title }}</strong></span>
                        @endif
                        @if($title && $org )
                        &nbsp;|&nbsp;
                        @endif
                        @if($org)
                            <span class=""><strong>{{ $org }}</strong></span>
                        @endif
                    </p>
                @endif
                @if( strpos( $email, 'info+' ) === false )
                    <p class="author-info__email"><a href="mailto:{{ $email }}">Email {{ $fname }}</a></p>
                @endif
                @if($phone)
                    <p class="author-info__phone">{{ $phone }}</p>
                @endif
                @if($twitter)
                    <p class="author-info__twitter"><a href="https://twitter.com/{{ $twitter }}" target="_blank" rel="nofollow">{{ $twitter }}</a></p>
                @endif
            </div>
        </div>

        <div class="author-info__bio">
            @if($short_bio)
                <div class="row bold">
                    <div class="column xs-100">
                        <hr />
                        {!! $short_bio !!}
                    </div>
                </div>
            @endif 
            
            @if($full_bio)
                <div class="row">
                    <div class="column xs-100">
                        <hr />
                        {!! $full_bio !!}
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection
