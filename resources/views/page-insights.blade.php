@extends('layouts.app')
@section('content')

@while(have_posts()) @php(the_post())
    {{-- h1 is not displayed in design, but there should still be one on the page --}}
    <h1 class="screen-reader-text">{!! App::title() !!}</h1>    
    <section class="">
        @if(!isset($_GET['news_type']) && !isset($_GET['news_topic']))
            <div class="row">
                <div class="column md-67">
                    @if($featured_post)
                        <div class="featured-news reveal">
                            <div class="row">
                                @foreach($featured_post as $post)
                                    @php(setup_postdata($GLOBALS['post'] = $post))
                                    @include('partials.list-item-insightmainfeatured')
                                    @php(wp_reset_postdata())
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>

                <div class="column md-33">
                    <div class="weekly-read reveal">
                        @if($get_weekly_read->posts)
                            <div class="row">
                                @foreach($get_weekly_read->posts as $post)
                                    @php(setup_postdata($GLOBALS['post'] = $post))
                                        @include('partials.list-item-insightfeatured')
                                    @php(wp_reset_postdata())
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="top-news-article reveal">
                        @if($featured_new_article)
                            <div class="row">
                                @foreach($featured_new_article as $post)
                                    @php(setup_postdata($GLOBALS['post'] = $post))
                                        @include('partials.list-item-insightfeatured')
                                    @php(wp_reset_postdata())
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="column md-100">
                @if($get_insights->posts)
                    <div class="row list-wrap list-wrap--posts">
                        @foreach($get_insights->posts as $post)
                            @php(setup_postdata($GLOBALS['post'] = $post))
                                @include('partials.list-item-insight')
                            @php(wp_reset_postdata())
                        @endforeach
                    </div>

                    <div class="pagination">
                        {!! $insight_pagination !!}
                    </div>
                @else 
                    <div class="row">
                        <div class="column sm-100">
                            <h4>Sorry, no posts were found.</h4>
                            <p><a href="{{ get_permalink( get_page_by_path('news') ) }}"><span class="icon icon-arrow-left"></span> View all News</a></p>
                        </div>
                    </div>
                @endif
            </div>
        </div>

        @if($get_latest_journal->posts)
            <div class="insight-journal">
                @foreach($get_latest_journal->posts as $post)
                    @php(setup_postdata($GLOBALS['post'] = $post))
                    @include('partials.list-item-latest-journal')
                @endforeach
                @php(wp_reset_postdata())
            </div>
        @endif

        {{-- @includeWhen($get_more_journals, 'partials.more-journals-content') --}}
    </section>
{{-- More journals  --}}
@if($get_more_journals)
    <div class="more-journals">
        <section class="more-journals-content">
            <div class="container">
                <p class="h2 text-center reveal more-journals-content__heading">Past Journal Issues</p>
                <div class="row">
                    @foreach ($get_more_journals->posts as $post)
                        @php(setup_postdata($GLOBALS['post'] = $post))
                        @include('partials.more-journals-content')
                    @endforeach
                    @php(wp_reset_postdata())
                </div>
                <p class="text-center reveal">
                    <a href="<?php echo get_post_type_archive_link( 'journals' ); ?>" class="more-journals-content__btn">See All Issues</a>
                </p>
            </div>
        </section>
    </div>
@endif

@endwhile
@endsection
