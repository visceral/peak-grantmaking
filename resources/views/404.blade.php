@extends('layouts.app')

@section('content')
  <div class="row justify-center">
    <div class="column md-75">
      <h2>We're sorry.</h2>
      <p>The page you were trying to view does not exist.</p>
      {!! get_search_form(false) !!}
    </div>
  </div>
@endsection
