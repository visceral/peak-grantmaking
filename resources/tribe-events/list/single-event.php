<?php
/**
 * List View Single Event 
 * This file contains one event in the list view
 * This is an override of Event Calendar's list view
 *
 * @version 4.6.19
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Setup an array of venue details for use later in the template
$venue_details = tribe_get_venue_details();

// The address string via tribe_get_venue_details will often be populated even when there's
// no address, so let's get the address string on its own for a couple of checks below.
$venue_address = tribe_get_address();

// Venue
$has_venue_address = ( ! empty( $venue_details['address'] ) ) ? ' location' : '';

// Organizer
$organizer = tribe_get_organizer();

// Categories
$categories = tribe_get_event_categories(
	get_the_id(), array(
		'before'       => '',
		'sep'          => ', ',
		'after'        => '',
		'label'        => '', // An appropriate plural/singular label will be provided
		'label_before' => '',
		'label_after'  => '',
		'wrap_before'  => '',
		'wrap_after'   => '',
	)
);
$categories = str_replace( ': ', '', $categories);

$column = tribe_event_featured_image() ? 'md-75' : 'md-100';
$excerpt = tribe_events_get_the_excerpt( null, wp_kses_allowed_html( 'post' ) );
$excerpt = wp_trim_words($excerpt, 30 );

?>

<!-- Event Title -->
<div class="row md-reverse reveal">
	<div class="column md-75">
		<div class="list-item-event__content row">
			<!-- Event Image -->
			<?php if( tribe_event_featured_image() ) {?>
				<div class="column md-25">
					<?php echo tribe_event_featured_image( null, 'medium' ); ?>
				</div>
			<?php } ?>

			<div class="column <?php echo $column; ?>">
				<?php do_action( 'tribe_events_before_the_event_title' ) ?>
				<h4 class="list-item-event__title">
					<a class="tribe-event-url" href="<?php echo esc_url( tribe_get_event_link() ); ?>" title="<?php the_title_attribute() ?>" rel="bookmark">
						<?php the_title() ?>
					</a>
				</h4>
				<?php do_action( 'tribe_events_after_the_event_title' ) ?>

				<!-- Event Content -->
				<?php do_action( 'tribe_events_before_the_content' ); ?>
				<div class="list-item-event__description">
					<?php echo $excerpt; ?>
				</div><!-- .tribe-events-list-event-description -->
				<?php do_action( 'tribe_events_after_the_content' ); ?>
				</div>
			</div>
	</div>

	<div class="column md-25">
		<!-- Event Meta -->
		<?php do_action( 'tribe_events_before_the_meta' ) ?>
		<div class="list-item-event__meta <?php echo esc_attr( $has_venue_address ); ?>">
			<?php if( $categories ) {
				echo '<p class="list-item-event__categories"><strong>' . $categories . '</strong></p>'; 
			} ?>

			<!-- Schedule & Recurrence Details -->
			<div class="list-item-event__schedule">
				<?php echo str_replace( '@', '<br>', tribe_events_event_schedule_details()); ?>
			</div>

			<?php if ( $venue_details ) : ?>
				<!-- Venue Display Info -->
				<div class="list-item-event__venue">
				<?php
					$address_delimiter = empty( $venue_address ) ? ' ' : ', ';

					// These details are already escaped in various ways earlier in the process.
					echo implode( $address_delimiter, $venue_details );

					if ( tribe_show_google_map_link() ) {
						echo tribe_get_map_link_html();
					}
				?>
				</div> <!-- .tribe-events-venue-details -->
			<?php endif; ?>

			<?php if( get_field('members_only') ) { ?>
				<p class="list-item-event__members"><strong>MEMBERS ONLY</strong></p>
			<?php } ?>
		</div><!-- .tribe-events-event-meta -->

		<!-- Event Cost -->
		<?php if ( tribe_get_cost() ) : ?>
			<div class="tribe-events-event-cost">
				<span class="ticket-cost"><?php echo tribe_get_cost( null, true ); ?></span>
				<?php
				/**
				 * Runs after cost is displayed in list style views
				 *
				 * @since 4.5
				 */
				do_action( 'tribe_events_inside_cost' )
				?>
			</div>
		<?php endif; ?>

		<?php do_action( 'tribe_events_after_the_meta' ) ?>
	</div>
</div>