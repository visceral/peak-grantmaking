<?php
/**
 * List View Title Template
 * The title template for the list view of events.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/title-bar.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 * @since   4.6.19
 *
 */

// Main events page
$page           = get_page_by_path('events');
$id             = $page->ID;
$title          = get_the_title($id);
$featured_event = get_field('featured_event', 55);
$featured_event = $featured_event ? $featured_event[0]->ID : '';
$past   	    = isset($_GET['tribe_event_display']);
$past			= $past && ( $_GET['tribe_event_display'] == 'past' ) ? true : false;
$list 		    = tribe_is_past() || tribe_is_upcoming() && !is_tax();

// Format featured event time. 
if( $featured_event ) {
	$schedule = str_replace(array('am', 'pm', '@'), array('AM', 'PM', '<br>'), tribe_events_event_schedule_details($featured_event) );
}
?>

<div class="tribe-events-title-bar">

	<!-- Featured Event - only display on list view -->
	<?php if( !$past && $featured_event && $list ) : ?>
		<div class="featured-event featured reveal row">
			<div class="column md-60 lg-67 xl-50">
				<div class="featured-event__content">
					<p class="featured-event__meta">Featured Event</p>
					<h2 class="featured-event__title h1"><a href="<?php echo get_permalink($featured_event); ?>"><?php echo get_the_title($featured_event); ?></a></h2>
					<h5><?php echo $schedule; ?></h5>
				</div>
			</div>
			<div class="column md-40 lg-33 xl-50">
				<?php if( has_post_thumbnail($featured_event) ) : ?>
					<a href="<?php echo get_permalink($featured_event); ?>" class="featured-event__image img-zoom">
						<?php echo get_the_post_thumbnail( $featured_event, 'large', array('class' => 'portrait') ); ?>
					</a>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>

	<!-- List Title -->
	<?php do_action( 'tribe_events_before_the_title' ); ?>
	<h1 class="tribe-events-page-title screen-reader-text"><?php echo tribe_get_events_title() ?></h1>
	<?php do_action( 'tribe_events_after_the_title' ); ?>

</div>
