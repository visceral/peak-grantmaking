<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural   = tribe_get_event_label_plural();

$event_id = get_the_ID();

$categories = tribe_get_event_categories(
	get_the_id(), array(
		'before'       => '',
		'sep'          => ', ',
		'after'        => '',
		'label'        => '', // An appropriate plural/singular label will be provided
		'label_before' => '',
		'label_after'  => '',
		'wrap_before'  => '<p class="post__label"><strong>',
		'wrap_after'   => '</strong></p>',
	)
);
$categories = str_replace( ': ', '', $categories);

$related_content = App\visceral_related_posts(3, array('post', 'tribe_events', 'resource'), 'related_posts');
?>

<div id="" class="tribe-events-single">
	<?php while ( have_posts() ) :  the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class('container'); ?>>
		
			<header class="post__header">
				<p class="breadcrumbs">
				<span class="icon icon-arrow-left"></span> <a class="breadcrumbs__link" href="<?php echo esc_url( tribe_get_events_link() ); ?>"><?php printf( esc_html_x( '%s', '%s Events plural label', 'the-events-calendar' ), $events_label_plural ); ?></a>
				</p>

				<!-- Notices -->
				<?php tribe_the_notices() ?>

				<?php if( $categories ) {
					echo $categories;
				} ?>

				<?php the_title( '<h1 class="h2">', '</h1>' ); ?>

				<!-- <div class="tribe-events-schedule tribe-clearfix"> -->
					<?php //echo tribe_events_event_schedule_details( $event_id, '<h2>', '</h2>' ); ?>
					<?php //if ( tribe_get_cost() ) : ?>
						<!-- <span class="tribe-events-cost"><?php echo tribe_get_cost( null, true ) ?></span> -->
					<?php //endif; ?>
				<!-- </div> -->
				<!-- Event featured image, but exclude link -->
				<?php // echo tribe_event_featured_image( $event_id, 'full', false ); ?>
			</header>

			<div class="row md-reverse">
				<div class="column md-67">
					<!-- Event content -->
					<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
					<div class="tribe-events-single-event-description tribe-events-content post__content">
						<?php the_content(); ?>
					</div>
					<!-- .tribe-events-single-event-description -->
				</div>

				<div class="column md-33 entry-meta entry-meta--event">
					<!-- Event meta -->
					<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
					<?php tribe_get_template_part( 'modules/meta' ); ?>
					<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>

					<?php // do_action( 'tribe_events_single_event_after_the_content' ) ?>
				</div>
			</div>

			<div class="row">
				<div class="column xs-100">
					<footer class="post__footer">
						<!-- Event footer -->
						<div id="" class="post__share">
							<p><strong>Share</strong></p>
							<ul class="social-links list-inline">
								<!-- <li><a href="https://twitter.com/share?text=<?php echo get_the_title(); ?>&url=<?php echo get_permalink(); ?>" class="social-links__link social-link--twitter"><i class="icon-twitter icon"><?php echo __('Twitter', 'visceral'); ?></i></a></li> -->
								<!-- <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" class="social-links__link social-link--facebook"><i class="icon-facebook icon" ><?php echo __('Facebook', 'visceral'); ?></i></a></li> -->
								<li><a href="https://www.linkedin.com/cws/share?url=<?php echo get_permalink(); ?>" class="social-links__link social-link--linkedin"><i class="icon-linkedin icon" ><?php echo __('LinkedIn', 'visceral'); ?></i></a></li>
							</ul>
						</div>
						<!-- #tribe-events-footer -->
					</footer>
				</div>
			</div>

		</article> <!-- #post-x -->
		
		<section class="promo promo--newsletter">
			<div class="container">
				<div class="row justify-center">
					<div class="column md-75">
						<div class="promo__icon"><span class="icon icon-envelope"></span></div>
						<p class="h2 text-center reveal promo__heading">Receive the latest insights & announcements via our communications</p>
						<a href="<?php echo get_permalink( get_page_by_path('stay-connected' ) ); ?>" class="promo__btn">Stay Connected <span class="icon icon-arrow-right"></span></a>
					</div>
				</div>
			</div>
		</section>

		<section class="related-content">
			<div class="container">
				<p class="h6 text-center reveal related-content__heading">Around Peak Grantmaking</p>
				<div class="row">
					<?php foreach($related_content as $post) {
						setup_postdata($GLOBALS['post'] = $post);
						$post_id          = get_the_id();
						$post_type        = ( get_post_type() == 'tribe_events' ) ? 'event' : get_post_type();
						$featured_image   = App\get_aspect_ratio_image(4, 3, 'medium');
						$meta_output      = '';
						$tax_term         = '';
						$card_image_class = has_post_thumbnail() ? 'list-item--featured-image' : '';
						
						if ($post_type === 'post') {
							$meta_output = '<span class="meta-post-type">' . get_the_date() . '</span>';
						} elseif ($post_type === 'resource') {
							$type = get_the_terms( $post_id, 'resource_type' );
							$meta_output = '<span class="meta-post-type">' . $type[0]->name . '</span>';
						} elseif ($post_type === 'event') {
							$meta_output = '<span class="meta-post-type">' . tribe_get_start_time ( $post_id, 'F j, Y' ) . ' @ ' . tribe_get_start_time() . '</span>';
						}
						?>
						
						<article class="column xs-100 md-50 lg-33 reveal">
							<div class="list-item list-item--related list-item--<?php echo $post_type; ?> <?php echo $card_image_class; ?>">
								<a href="<?php echo get_permalink(); ?>" class="list-item__link">
									<p class="list-item__label"><?php echo $post_type; ?></p>
									<?php if(has_post_thumbnail()) { ?>
										<div class="image-zoom list-item__image">
											<div class="img-cover">
												<?php echo $featured_image; ?>
											</div>
										</div>
									<?php } ?>
									<div class="list-item__content">
										<h3 class="list-item__title"><?php echo get_the_title(); ?></h3>
										<?php if($meta_output) { ?>
											<div class="list-item__meta">
												<?php echo $meta_output; ?>
											</div>
										<?php } ?>
									</div>
								</a>
							</div>    
						</article>
						<?php wp_reset_postdata();
					} ?>
				</div>
			</div>
		</section>
	
	<?php endwhile; ?>
	

</div><!-- #tribe-events-content -->
