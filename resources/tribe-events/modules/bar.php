<?php
/**
 * Events Navigation Bar Module Template
 * Renders our events navigation bar used across our views
 *
 * $filters and $views variables are loaded in and coming from
 * the show funcion in: lib/Bar.php
 *
 * Override this template in your own theme by creating a file at:
 *
 *     [your-theme]/tribe-events/modules/bar.php
 *
 * @package  TribeEventsCalendar
 * @version 4.6.26
 */
?>

<?php

$filters      = tribe_events_get_filters();
$views       = tribe_events_get_views();
$current_url = tribe_events_get_current_filter_url();
$events_url  = get_permalink( get_page_by_path('events') );
$classes     = array( 'tribe-clearfix', 'row' );
// Instead of all event categories, we only want categories of upcoming events
// 1. get query of upcoming events
$upcomingEvents = tribe_get_events([
   'posts_per_page' => -1,
   'start_date'     => 'now',
]);
// 2. loop through these for their post terms
$cats = array();
foreach( $upcomingEvents as $event ) {
    $eventTerms = wp_get_post_terms($event->ID, 'tribe_events_cat', array('fields' =>'ids'));
    $cats[] = $eventTerms;
}
// 3. this is going to return a multidimensional array. flatten that & grab only the unique values
$uniqueCats = array_flatten($cats);
// 4. now we'll grab only the needed terms
$cats = get_terms('tribe_events_cat', array( 'include' => $uniqueCats )); 
?>

<?php do_action( 'tribe_events_bar_before_template' ) ?>
<div id="tribe-events-bar">

	<h2 class="tribe-events-visuallyhidden"><?php printf( esc_html__( '%s Search and Views Navigation', 'the-events-calendar' ), tribe_get_event_label_plural() ); ?></h2>

	<form id="tribe-bar-form" class="<?php echo esc_attr( implode( ' ', $classes ) ); ?>" name="tribe-bar-form" method="post" action="<?php echo esc_attr( $current_url ); ?>">
		<div class="column sm-50">
			<label class="screen-reader-text" for="tribe_events_category">{{ __('Event Type', 'visceral') }}</label>
			
			<select name="tribe_events_category" id="tribe_events_category" class="input--alt" onchange="window.location.href=this.value">
				<option value="<?php echo $events_url; ?>"><?php echo __('All Upcoming Events', 'visceral'); ?></option>
				<?php if( $cats ) { ?>
					<?php foreach($cats as $cat ) {
						$selected = (get_queried_object() == $cat) ? 'selected' : '';
					?>
						<option value="<?php echo $current_url . '&tribe_events_cat=' . $cat->slug; ?>" <?php echo $selected; ?>><?php echo $cat->name ?></option>
					<?php } ?>
				<?php } ?>
			</select>
		</div>
		<div class="column sm-50">
		<?php if ( count( $views ) > 1 ) : ?>
			<div id="tribe-bar-views" class="tribe-bar-views">
				<div class="tribe-bar-views-inner tribe-clearfix">
					<h3 class="tribe-events-visuallyhidden"><?php printf( esc_html__( '%s Views Navigation', 'the-events-calendar' ), tribe_get_event_label_singular() ); ?></h3>
					<label id="tribe-bar-views-label" aria-label="<?php printf( esc_html__( 'View %s As', 'the-events-calendar' ), tribe_get_event_label_plural() ); ?>">
						<?php esc_html_e( 'View as:', 'the-events-calendar' ); ?>
					</label>
					<select
						class="tribe-bar-views-select tribe-no-param"
						name="tribe-bar-view"
						aria-label="<?php printf( esc_attr__( 'View %s As', 'the-events-calendar' ), tribe_get_event_label_plural() ); ?>"
					>
						<?php
						foreach ( $views as $view ) {
							printf(
								'<option value="%1$s" data-view="%2$s"%3$s>%4$s</option>',
								esc_attr( $view['url'] ),
								esc_attr( $view['displaying'] ),
								tribe_is_view( $view['displaying'] ) ? ' selected' : '',
								esc_html( $view['anchor'] )
							);
						}
						?>
					</select>
				</div>
			</div>
		<?php endif; ?>
		</div>

		<div class="column js-show">
			<input type="submit" value="Submit">
		</div>
	</form>

</div>
<?php
do_action( 'tribe_events_bar_after_template' );
