<?php
/**
 * Single Event Meta (Details) Template
 *
 * Override this template in your own theme by creating a file at:
 * [your-theme]/tribe-events/modules/meta/details.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 */

$time_format          = get_option( 'time_format', Tribe__Date_Utils::TIMEFORMAT );
$time_range_separator = tribe_get_option( 'timeRangeSeparator', ' - ' );

$start_datetime       = tribe_get_start_date();
$start_date           = tribe_get_start_date( null, false, 'F j' );
$start_time           = tribe_get_start_date( null, false, $time_format );
$start_ts             = tribe_get_start_date( null, false, Tribe__Date_Utils::DBDATEFORMAT );

$end_datetime         = tribe_get_end_date();
$end_date             = tribe_get_display_end_date( null, false, 'j, Y' );
$end_time             = tribe_get_end_date( null, false, $time_format );
$end_ts               = tribe_get_end_date( null, false, Tribe__Date_Utils  ::DBDATEFORMAT );

$time_formatted = null;
if ( $start_time == $end_time ) {
	$time_formatted = esc_html( $start_time );
} else {
	$time_formatted = esc_html( $start_time . $time_range_separator . $end_time );
}

$event_id = Tribe__Main::post_id_helper();

// timezone
$abbr = get_post_meta($event_id, '_EventTimezoneAbbr', true);
if (empty($abbr)) {
	$timezone_string = self::get_event_timezone_string($event_id);
	$date            = tribe_get_start_date($event_id, true, Tribe__Date_Utils::DBDATETIMEFORMAT);
	$abbr            = self::abbr($date, $timezone_string);
}

/**
 * Returns a formatted time for a single event
 *
 * @var string Formatted time string
 * @var int Event post id
 */
$time_formatted = apply_filters( 'tribe_events_single_event_time_formatted', $time_formatted, $event_id );

/**
 * Returns the title of the "Time" section of event details
 *
 * @var string Time title
 * @var int Event post id
 */
$time_title = apply_filters( 'tribe_events_single_event_time_title', __( 'Time:', 'the-events-calendar' ), $event_id );

$cost    = tribe_get_formatted_cost();
$website = tribe_get_event_website_url();
?>
	
<?php
// Event Website
if ( ! empty( $website ) ) : ?>
	<a href="<?php echo $website; ?>" class="entry-meta__button btn--fill btn--large">Register <span class="icon icon-arrow-right"></span></a>
<?php endif ?>

<?php
do_action( 'tribe_events_single_meta_details_section_start' );

// All day (multiday) events
if ( tribe_event_is_all_day() && tribe_event_is_multiday() ) : ?>

	<dl class="screen-reader-text">
		<dt class="tribe-events-start-date-label"> <?php esc_html_e( 'Start:', 'the-events-calendar' ) ?> </dt>
		<dd>
			<abbr class="tribe-events-abbr tribe-events-start-date published dtstart" title="<?php esc_attr_e( $start_ts ) ?>"> <?php esc_html_e( $start_date ) ?> </abbr>
		</dd>

		<dt class="tribe-events-end-date-label"> <?php esc_html_e( 'End:', 'the-events-calendar' ) ?> </dt>
		<dd>
			<abbr class="tribe-events-abbr tribe-events-end-date dtend" title="<?php esc_attr_e( $end_ts ) ?>"> <?php esc_html_e( $end_date ) ?> </abbr>
		</dd>
	</dl>

	<p class="entry-meta__time h4" title="<?php esc_attr_e( $end_ts ) ?>"><?php esc_html_e( $start_date . ' - ' . $end_date ) ?></p>

<?php
// All day (single day) events
elseif ( tribe_event_is_all_day() ):
	?>

	<dt class="tribe-events-start-date-label"> <?php esc_html_e( 'Date:', 'the-events-calendar' ) ?> </dt>
	<dd>
		<abbr class="tribe-events-abbr tribe-events-start-date published dtstart" title="<?php esc_attr_e( $start_ts ) ?>"> <?php esc_html_e( $start_date ) ?> </abbr>
	</dd>

<?php
// Multiday events
elseif ( tribe_event_is_multiday() ) :
	?>

	<dt class="tribe-events-start-datetime-label"> <?php esc_html_e( 'Start:', 'the-events-calendar' ) ?> </dt>
	<dd>
		<abbr class="entry-meta__time h4 tribe-events-abbr tribe-events-start-datetime updated published dtstart" title="<?php esc_attr_e( $start_ts ) ?>"> <?php esc_html_e( $start_datetime . ' ' . $abbr ) ?></abbr>
	</dd>

	<dt class="tribe-events-end-datetime-label"> <?php esc_html_e( 'End:', 'the-events-calendar' ) ?> </dt>
	<dd>
		<abbr class="entry-meta__time h4 tribe-events-abbr tribe-events-end-datetime dtend" title="<?php esc_attr_e( $end_ts ) ?>"> <?php esc_html_e( $end_datetime . ' ' . $abbr ) ?> </abbr>
	</dd>

<?php
// Single day events
else : ?>

	<p class="entry-meta__date h4">
		<?php esc_html_e( $start_date ) ?>
	</p>
	
	<p class="entry-meta__time h4" title="<?php esc_attr_e( $end_ts ) ?>"><?php echo $time_formatted . ' ' . $abbr; ?></p>

<?php endif ?>

<?php
// Event Cost
if ( ! empty( $cost ) ) : ?>

	<dt class="tribe-events-event-cost-label"> <?php esc_html_e( 'Cost:', 'the-events-calendar' ) ?> </dt>
	<dd class="tribe-events-event-cost"> <?php esc_html_e( $cost ); ?> </dd>
<?php endif ?>

<?php echo tribe_meta_event_tags( sprintf( esc_html__( '%s Tags:', 'the-events-calendar' ), tribe_get_event_label_singular() ), ', ', false ) ?>

<?php do_action( 'tribe_events_single_meta_details_section_end' ) ?>