// import external dependencies
import 'jquery';
import 'magnific-popup';
import 'slick-carousel/slick/slick.min';
import 'waypoints/lib/jquery.waypoints.min.js';

// Import everything from autoload

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import grantsManagementProfessionalCompetencyModel from './routes/grants-management-professional-competency-model';
import aboutUs from './routes/about';
import resources from './routes/resources';
import singleResource from './routes/single_resource';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
  resources,
  grantsManagementProfessionalCompetencyModel,
  singleResource,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
