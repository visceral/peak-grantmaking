export default {
  init() {
    // JavaScript to be fired on a single resource

    /** COOKIE FUNCTIONS */
    function createCookie(name,value,days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days*24*60*60*1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }
  
    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }
    /** END COOKIE FUNCTIONS */
  
    // Gated content
    var $modalBG = $('.modal-bg'),
        $gateLink = $('a.link--gated'),
        downloadGate = readCookie('peak_download_gate'),
        $resourceURL;
  
    $gateLink.on('click', function(e) {
    // Check if should not have download gate 

    var a = new RegExp('/' + window.location.host + '/');

    // Check if it's an internal link 
    if (a.test(this.href)) {
        if ( downloadGate === null ) {
        e.preventDefault();
        $resourceURL = $(this).attr('href');
        $modalBG.show();
        }
    }
    });

    // Close modal
    $('.download-modal .icon-cancel').on('click', function() {
    $modalBG.hide();
    });

    // Once form is submitted, set cookie and open link
    $(document).bind('gform_confirmation_loaded', function(){
        createCookie('peak_download_gate', '', 365);
        // downloadGate = false;
        window.open($resourceURL, '_blank');
    });

    // Disable menu on right click
    $gateLink.bind('contextmenu', function() {
    return false;
    });
    // End gated content

  },
};
