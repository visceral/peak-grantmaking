/* eslint-disable no-unused-vars */

export default {
    init() {
        // JavaScript to be fired on all pages

        /** GLOBAL VARIABLES */
        var body = document.querySelector("body");
        /** END GLOBAL VARIABLES */

        /** SKIP LINK NAVIGATION */
        $("#skip-to-content").click(function () {
            // strip the leading hash and declare
            // the content we're skipping to
            var skipTo = "#" + this.href.split("#")[1];
            // Setting 'tabindex' to -1 takes an element out of normal
            // tab flow but allows it to be focused via javascript
            $(skipTo)
                .attr("tabindex", -1)
                .on("blur focusout", function () {
                    // when focus leaves this element,
                    // remove the tabindex attribute
                    $(this).removeAttr("tabindex");
                })
                .focus(); // focus on the content container
        });
        /** END SKIP LINK NAVIGATION */

        /** RESPONSIVE NAV OPEN/CLOSE **/
        var mobileNav = document.querySelector("#mobile-nav");
        var mobileNavButton = document.querySelector("#mobile-nav-icon");

        // Toggle menu icon
        if (mobileNavButton) {
            mobileNavButton.addEventListener("click", function () {
                body.classList.toggle("mobile-nav-open");
            });
        }

        // Close menu if page is clicked (not menu item)
        if (mobileNav) {
            mobileNav.addEventListener("click", function (e) {
                // Don't hide if click is on a child element (nav link)
                if (e.target !== this) {
                    return;
                }
                mobileNavButton.classList.toggle("opened");
                $("#nav-toggle").prop("checked", false);
            });

            // Disable click on mobile
            $("#mobile-nav .menu-item-has-children > .sub-menu--open").on(
                "click",
                function () {
                    $(this)
                        .parent(".menu-item")
                        .toggleClass("mobile-nav--opened");
                }
            );
        }
        /** END RESPONSIVE NAV OPEN/CLOSE **/

        /** SHOW AND HIDE SEARCH BAR **/
        var searchButton = $(".header__search-button"),
            headerSearch = $(".header__search"),
            searchClose = $(".header__search-close");

        searchButton.click(function () {
            headerSearch.slideToggle("slow", function () {
                headerSearch.find('input[type="text"]').focus();
                if (window.ActiveXObject && !("ActiveXObject" in window)) {
                    headerSearch.find('input[type="text"]').focus();
                }
            });
            return false;
        });

        searchClose.click(function () {
            headerSearch.slideToggle("fast");
        });

        // Shake Animation
        function shake(div) {
            var interval = 100;
            var distance = 10;
            var times = 4;

            $(div).css("position", "relative");
            for (var iter = 0; iter < times + 1; iter++) {
                $(div).animate(
                    { left: iter % 2 === 0 ? distance : distance * -1 },
                    interval
                );
            }
            $(div).animate({ left: 0 }, interval);
        }

        // Search Submit
        headerSearch.submit(function (e) {
            var s = $(this).find('input[type="text"]');
            if (!s.val()) {
                e.preventDefault();
                shake($(this).find('input[type="text"]'));
                $(this).find('input[type="text"]').focus();
            }
        });
        /** END SHOW OR HIDE SEARCH BAR **/

        /** MASTHEAD PRGRESSIVE IMAGES **/
        var masthead = document.querySelector(".masthead__image"),
            placeholder = document.querySelector(".masthead__overlay");

        if (masthead && placeholder) {
            // Load full size image. When loaded, fade our placeholder add it as bg to masthead
            var img = new Image();
            img.src = masthead.dataset.imageSrc;
            img.onload = function () {
                placeholder.classList.add("fade-out");
                masthead.style.backgroundImage = "url(" + img.src + ")";
            };
        }
        /** END MASTHEAD PRGRESSIVE IMAGES **/

        /* HEADER SCROLL EFFECT */
        // Reference: http://tympanus.net/codrops/2013/06/06/on-scroll-animated-header/

        var animatedHeader = (function () {
            // eslint-disable-line no-unused-vars
            var didScroll = false,
                scrollOffset = 0;

            function scrollY() {
                return window.pageYOffset || document.documentElement.scrollTop;
            }

            function scrollPage() {
                var sy = scrollY();
                if (sy > scrollOffset) {
                    body.classList.add("scroll-triggered");
                } else {
                    body.classList.remove("scroll-triggered");
                }
                didScroll = false;
            }

            function init() {
                window.addEventListener("load", scrollPage);
                window.addEventListener(
                    "scroll",
                    function (event) {
                        // eslint-disable-line no-unused-vars
                        if (!didScroll) {
                            didScroll = true;
                            setTimeout(scrollPage, 50);
                        }
                    },
                    false
                );
            }
            init();
        })();
        /* END HEADER SCROLL EFFECT */

        /** ANIMATED ANCHOR LINKS **/
        // $('a[href*="#"]:not([href="#"])').click(function (e) {
        //     if (!$(this).closest("li").hasClass("vc_tta-tab")) {
        //         if (!$(this).closest("h4").hasClass("vc_tta-panel-title")) {
        //             if (
        //                 location.pathname.replace(/^\//, "") ===
        //                 this.pathname.replace(/^\//, "") &&
        //                 location.hostname === this.hostname
        //             ) {
        //                 var target = $(this.hash);
        //                 var $this = this;
        //                 var header = $("header.header");
        //                 var wpAdminBar = $("#wpadminbar");
        //                 var fixedHeaderOffset =
        //                     parseInt(header.outerHeight()) -
        //                     parseInt(
        //                         header.css("padding-top").replace("px", "")
        //                     );
        //                 // If we're logged in and WP Admin Bar exists, add it to the offset
        //                 if (wpAdminBar.length) {
        //                     fixedHeaderOffset += parseInt(
        //                         wpAdminBar.outerHeight()
        //                     );
        //                 }
        //                 target = target.length
        //                     ? target
        //                     : $("[name=" + this.hash.slice(1) + "]");
        //                 if (target.length) {
        //                     $("html,body").animate(
        //                         {
        //                             scrollTop:
        //                                 target.offset().top - fixedHeaderOffset,
        //                         },
        //                         500,
        //                         function () {
        //                             if (history.pushState) {
        //                                 history.pushState(
        //                                     null,
        //                                     null,
        //                                     $this.hash
        //                                 );
        //                             } else {
        //                                 location.hash = $this.hash;
        //                             }
        //                         }
        //                     );
        //                     e.preventDefault();
        //                 }
        //             }
        //         }
        //     }
        // });
        /** END ANIMATED ANCHOR LINKS **/

        // Reveal animation using waypoints
        function itemReveal() {
            var waypoints = $(".reveal").waypoint({
                // eslint-disable-line no-unused-vars
                handler: function (direction) {
                    // eslint-disable-line no-unused-vars
                    $(this)[0].element.classList.add("revealed");
                },
                offset: "85%",
            });

            var waypointChildren = $(
                ".reveal--children > .wpb_wrapper > *"
            ).waypoint({
                // eslint-disable-line no-unused-vars
                handler: function (direction) {
                    // eslint-disable-line no-unused-vars
                    $(this)[0].element.classList.add("revealed");
                },
                offset: "85%",
            });
        }

        itemReveal();

        // Homepage background video
        $(".player").YTPlayer();

        // slick slide
        if ($(".visceral-slider").length > 0) {
            var slider = $(
                ".visceral-slider > .wpb_column > .vc_column-inner > .wpb_wrapper"
            );

            slider.slick({
                slide:
                    ".visceral-slider > .wpb_column > .vc_column-inner > .wpb_wrapper > *",
                autoplay: false,
                autoplaySpeed: 4000,
                infinite: true,
                centerMode: true,
                centerPadding: "0px",
                cssEase: "linear",
                slidesToShow: 1,
                slidesToScroll: 1,
                swipeToSlide: true,
                dots: true,
                arrows: false,
                fade: true,
                mobileFirst: true,
                prevArrow:
                    '<button type="button" class="slick-prev"><span class="icon icon-arrow-left"></span><span class="screen-reader-text">Previous</span></button>',
                nextArrow:
                    '<button type="button" class="slick-next"><span class="icon icon-arrow-right"></span><span class="screen-reader-text">Next</span></button>',
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            arrows: true,
                        },
                    },
                ],
            });
        }

        $(".key-numeric").keypress(function (e) {
            var verified =
                e.which == 8 || e.which == undefined || e.which == 0
                    ? null
                    : String.fromCharCode(e.which).match(/[^0-9]/);
            if (verified) {
                e.preventDefault();
            }
        });

        // magnific-popup
        $(document).ready(function () {
            // Video modals
            if ($(".video-modal").length > 0) {
                $(".video-modal").magnificPopup({
                    disableOn: 700,
                    type: "iframe",
                    mainClass: "mfp-fade",
                    removalDelay: 160,
                    preloader: false,
                    fixedContentPos: false,
                });
            }
        });

        // Image modal
        if ($(".image-modal").length > 0) {
            $(".image-modal").each(function () {
                console.log($(this));
                $(this).magnificPopup({
                    delegate: 'a',
                    type: 'inline',
                    closeMarkup: '<button title="%title%" type="button" class="mfp-close">Close <i class="icon-cancel"></i></button>',
                    mainClass: 'mfp-fade',
                    closeOnContentClick: true,
                    removalDelay: 160,
                    preloader: false,
                    fixedContentPos: false,
                });
            });
        }

        /*
         * jQuery Reveal Plugin 1.0
         * www.ZURB.com
         * Copyright 2010, ZURB
         * Free to use under the MIT license.
         * http://www.opensource.org/licenses/mit-license.php
         */
        $("a[data-reveal-id]").on("click", function (event) {
            event.preventDefault();
            var modalLocation = $(this).attr("data-reveal-id");
            $("#" + modalLocation).reveal($(this).data());
        });

        $.fn.reveal = function (optionsa) {
            var defaults = {
                animation: "fade", // fade, fadeAndPop, none
                animationSpeed: 500, // how fast animtions are
                closeOnBackgroundClick: true, // if you click background will modal close?
                dismissModalClass: "close-reveal-modal", // the class of a button or element that will close an open modal
            };
            var options = $.extend({}, defaults, optionsa);

            return this.each(function () {
                var modal = $(this),
                    topMeasure = parseInt(modal.css("top")),
                    topOffset = modal.height() + topMeasure,
                    locked = false,
                    modalBg = $(".reveal-modal-bg");

                if (modalBg.length == 0) {
                    modalBg = $('<div class="reveal-modal-bg" />').insertAfter(
                        modal
                    );
                    modalBg.fadeTo("fast", 0.8);
                }

                function openAnimation() {
                    modalBg.unbind("click.modalEvent");
                    $("." + options.dismissModalClass).unbind(
                        "click.modalEvent"
                    );
                    if (!locked) {
                        lockModal();
                        if (options.animation == "fadeAndPop") {
                            modal.css({
                                top: $(document).scrollTop() - topOffset,
                                opacity: 0,
                                visibility: "visible",
                            });
                            modalBg.fadeIn(options.animationSpeed / 2);
                            modal.delay(options.animationSpeed / 2).animate(
                                {
                                    top:
                                        $(document).scrollTop() +
                                        topMeasure +
                                        "px",
                                    opacity: 1,
                                },
                                options.animationSpeed,
                                unlockModal
                            );
                        }
                        if (options.animation == "fade") {
                            modal.css({
                                opacity: 0,
                                visibility: "visible",
                                top: $(document).scrollTop() - 700,
                            });
                            // console.log("Top measure: " + topMeasure);
                            // console.log("Top offset: " + topOffset / 3);
                            modalBg.fadeIn(options.animationSpeed / 2);
                            modal.delay(options.animationSpeed / 2).animate(
                                {
                                    opacity: 1,
                                },
                                options.animationSpeed,
                                unlockModal
                            );
                        }
                        if (options.animation == "none") {
                            modal.css({
                                visibility: "visible",
                                top: $(document).scrollTop() + topMeasure,
                            });
                            modalBg.css({ display: "block" });
                            unlockModal();
                        }
                    }
                    modal.unbind("reveal:open", openAnimation);
                }
                modal.bind("reveal:open", openAnimation);

                function closeAnimation() {
                    if (!locked) {
                        lockModal();
                        if (options.animation == "fadeAndPop") {
                            modalBg
                                .delay(options.animationSpeed)
                                .fadeOut(options.animationSpeed);
                            modal.animate(
                                {
                                    top:
                                        $(document).scrollTop() -
                                        topOffset +
                                        "px",
                                    opacity: 0,
                                },
                                options.animationSpeed / 2,
                                function () {
                                    modal.css({
                                        top: topMeasure,
                                        opacity: 1,
                                        visibility: "hidden",
                                    });
                                    unlockModal();
                                }
                            );
                        }
                        if (options.animation == "fade") {
                            modalBg
                                .delay(options.animationSpeed)
                                .fadeOut(options.animationSpeed);
                            modal.animate(
                                {
                                    opacity: 0,
                                },
                                options.animationSpeed,
                                function () {
                                    modal.css({
                                        opacity: 1,
                                        visibility: "hidden",
                                        top: topMeasure,
                                    });
                                    unlockModal();
                                }
                            );
                        }
                        if (options.animation == "none") {
                            modal.css({
                                visibility: "hidden",
                                top: topMeasure,
                            });
                            modalBg.css({ display: "none" });
                        }
                    }
                    modal.unbind("reveal:close", closeAnimation);
                }
                modal.bind("reveal:close", closeAnimation);
                modal.trigger("reveal:open");

                var closeButton = $("." + options.dismissModalClass);

                closeButton.bind("click.modalEvent", function () {
                    modal.trigger("reveal:close");
                });

                if (options.closeOnBackgroundClick) {
                    modalBg.css({ cursor: "pointer" });
                    modalBg.bind("click.modalEvent", function () {
                        modal.trigger("reveal:close");
                    });
                }

                $("body").keyup(function (event) {
                    if (event.which === 27) {
                        // 27 is the keycode for the Escape key
                        modal.trigger("reveal:close");
                    }
                });

                function unlockModal() {
                    locked = false;
                }

                function lockModal() {
                    locked = true;
                }
            });
        };

        /* Modify Page Builder Tabs to work with external links */

        // Variables
        const $sectionTabs = $(".vc_tta-tab a");
        const $externalLinkSection = $(".vc_tta-panel.visc-link-tab");

        if ($sectionTabs) {
            $sectionTabs.each(function () {
                let href = $(this).attr("href");

                // If tab is a full link, remove the hash and remove tab data attribute
                if (href.includes("http")) {
                    let newUrl = href.replace("#", ""); // Create new url
                    $(this).attr("href", newUrl); // Set herf value

                    $(this).attr("target", "_blank");
                    $(this).attr("rel", "noopener noreferrer");

                    $(this).removeAttr("data-vc-tabs");
                }
            });
        }
        // Removes the empty external link section
        if ($externalLinkSection) {
            $externalLinkSection.remove();
        }

        /* Add link that will trigger tab click */
        let $tabLink = $('.link--open-tab a');

        if ($tabLink) {
            $tabLink.each(function () {
                let $href = $(this).attr('href');

                let $tab = $('.vc_tta-tab').children('a[href$="' + $href + '"]');
                $(this).click(function (e) {
                    e.preventDefault();
                    if ($tab) {
                        $tab.trigger('click');
                    }
                });
            });
        }
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    },
};
