import ScrollMagic from 'scrollmagic';
import TweenMax from "gsap/TweenMax";

export default {
  init() {
    // JavaScript to be fired on the home page

    var windowHeight = $(window).height();
    var controller   = new ScrollMagic.Controller();

    new ScrollMagic.Scene({
      duration: windowHeight,
      offset: 0,
    }).addTo(controller).on("progress", function(e) {
      TweenMax.to($(".tri-left-1"), 3, {
        y: -200 * e.progress,
        // ease: Expo.easeOut
      })
    });

    new ScrollMagic.Scene({
        duration: windowHeight,
        offset: 0,
    }).addTo(controller).on("progress", function(e) {
        TweenMax.to($(".tri-left-2"), 3, {
            y: -300 * e.progress,
            // ease: Expo.easeOut
        })
    }),
    new ScrollMagic.Scene({
        duration: windowHeight + 200,
        offset: 0,
    }).addTo(controller).on("progress", function(e) {
        TweenMax.to($(".tri-left-3"), 3, {
            y: -400 * e.progress,
            // ease: Expo.easeOut
        })
    }),

    new ScrollMagic.Scene({
        duration: windowHeight,
        offset: 0,
    }).addTo(controller).on("progress", function(e) {
        TweenMax.to($(".tri-right-1"), 3, {
            y: -100 * e.progress,
            // ease: Expo.easeOut
        })
    }),
    new ScrollMagic.Scene({
        duration: windowHeight,
        offset: 0,
    }).addTo(controller).on("progress", function(e) {
        TweenMax.to($(".tri-right-2"), 3, {
            y: -500 * e.progress,
            // ease: Expo.easeOut
        })
    }),
    new ScrollMagic.Scene({
        duration: windowHeight,
        offset: 0,
    }).addTo(controller).on("progress", function(e) {
        TweenMax.to($(".tri-right-3"), 3, {
            y: -200 * e.progress,
            // ease: Expo.easeOut
        })
    });



  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
