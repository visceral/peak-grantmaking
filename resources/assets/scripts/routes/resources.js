export default {
  init() {
    // JavaScript to be fired on the about us page
    // slick slide
    if ($('.featured-resources').length > 0 ) {
        var slider = $('.featured-resources');
  
        slider.slick({
          slide         : '.featured-resource',
          autoplay      : true,
          autoplaySpeed : 3500,
          infinite       : true,
          cssEase       : 'linear',
          slidesToShow  : 1,
          slidesToScroll: 1,
          swipeToSlide  : true,
          dots          : true,
          arrows        : false,
          fade          : true,
          mobileFirst   : true,
        });
      }
  },
};
