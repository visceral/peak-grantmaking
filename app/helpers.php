<?php

namespace App;

use Roots\Sage\Container;

/**
 * Get the sage container.
 *
 * @param string $abstract
 * @param array  $parameters
 * @param Container $container
 * @return Container|mixed
 */
function sage($abstract = null, $parameters = [], Container $container = null)
{
    $container = $container ?: Container::getInstance();
    if (!$abstract) {
        return $container;
    }
    return $container->bound($abstract)
        ? $container->makeWith($abstract, $parameters)
        : $container->makeWith("sage.{$abstract}", $parameters);
}

/**
 * Get / set the specified configuration value.
 *
 * If an array is passed as the key, we will assume you want to set an array of values.
 *
 * @param array|string $key
 * @param mixed $default
 * @return mixed|\Roots\Sage\Config
 * @copyright Taylor Otwell
 * @link https://github.com/laravel/framework/blob/c0970285/src/Illuminate/Foundation/helpers.php#L254-L265
 */
function config($key = null, $default = null)
{
    if (is_null($key)) {
        return sage('config');
    }
    if (is_array($key)) {
        return sage('config')->set($key);
    }
    return sage('config')->get($key, $default);
}

/**
 * @param string $file
 * @param array $data
 * @return string
 */
function template($file, $data = [])
{
    if (remove_action('wp_head', 'wp_enqueue_scripts', 1)) {
        wp_enqueue_scripts();
    }

    return sage('blade')->render($file, $data);
}

/**
 * Retrieve path to a compiled blade view
 * @param $file
 * @param array $data
 * @return string
 */
function template_path($file, $data = [])
{
    return sage('blade')->compiledPath($file, $data);
}

/**
 * @param $asset
 * @return string
 */
function asset_path($asset)
{
    return sage('assets')->getUri($asset);
}

/**
 * @param string|string[] $templates Possible template files
 * @return array
 */
function filter_templates($templates)
{
    $paths = apply_filters('sage/filter_templates/paths', [
        'views',
        'resources/views'
    ]);
    $paths_pattern = "#^(" . implode('|', $paths) . ")/#";

    return collect($templates)
        ->map(function ($template) use ($paths_pattern) {
            /** Remove .blade.php/.blade/.php from template names */
            $template = preg_replace('#\.(blade\.?)?(php)?$#', '', ltrim($template));

            /** Remove partial $paths from the beginning of template names */
            if (strpos($template, '/')) {
                $template = preg_replace($paths_pattern, '', $template);
            }

            return $template;
        })
        ->flatMap(function ($template) use ($paths) {
            return collect($paths)
                ->flatMap(function ($path) use ($template) {
                    return [
                        "{$path}/{$template}.blade.php",
                        "{$path}/{$template}.php",
                        "{$template}.blade.php",
                        "{$template}.php",
                    ];
                });
        })
        ->filter()
        ->unique()
        ->all();
}

/**
 * @param string|string[] $templates Relative path to possible template files
 * @return string Location of the template
 */
function locate_template($templates)
{
    return \locate_template(filter_templates($templates));
}

/**
 * Determine whether to show the sidebar
 * @return bool
 */
function display_sidebar()
{
    static $display;
    isset($display) || $display = apply_filters('sage/display_sidebar', false);
    return $display;
}





/**
 * Custom pullquote
 *
 * @return markup for displaying pullquote.
 */


function peak_pull_quote($atts){

    $a = shortcode_atts( array(
            'quote' => '',
            'name'  => ''
        ), $atts );

    $markup = '<div class="vc-author-blockquote peak-pullquote">';

        if($a['quote']){
            $markup .= '<blockquote class="vc-author-blockquote__quote">'.$a['quote'].'</blockquote>';
        }

        if($a['name']){
            $markup .= '<div class="vc-author-blockquote__author"><p>- '.$a['name'].'</div>';
        }

    $markup .= '</div>';
    

    return $markup;

}

add_shortcode('peak-pullquote', __NAMESPACE__ . '\\peak_pull_quote');






/**
 * Custom login Form
 *
 * @return  markup for login form and signup link.
 */

function peak_login_form(){
    if(is_user_logged_in()){
        $new_content = '';
        // $new_content .= "<div class='post__restricted_login'>";
        //     $new_content .= "<div class='post__restricted_icon'><i class=\"fas fa-lock\"></i></div>";
        //         $new_content .= "<div class='row'>";
                    

        //              $new_content .= "<div class='column xs-100 member_login'>";
                        $new_content .= "<h5>You are Logged in!</h5>";
                //      $new_content .= "</div>";


                //         $new_content .= "</div>";
                //     $new_content .= "</div>";
                // $new_content .= "</div>";
    }else{
        $new_content = '';
        // $new_content .= "<div class='post__restricted_login'>";
        //     $new_content .= "<div class='post__restricted_icon'><i class=\"fas fa-lock\"></i></div>";
                // $new_content .= "<div class='row'>";
                    

                    //  $new_content .= "<div class='column xs-100 md-40 member_login'>";
                        // $new_content .= "<h5>Login for access</h5>";
                        $args = array(
                                'echo' => false,
                                'redirect' => $_GET['redirect_to'], 
                                'form_id' => 'loginform',
                                'label_username' => __( 'Username' ),
                                'label_password' => __( 'Password' ),
                                'label_remember' => __( 'Remember Me' ),
                                'label_log_in' => __( 'Log In' ),
                                'id_username' => 'user_login',
                                'id_password' => 'user_pass',
                                'id_remember' => 'rememberme',
                                'id_submit' => 'wp-submit',
                                'remember' => true,
                                'value_username' => NULL,
                                'value_remember' => false );

                        $new_content .= wp_login_form( $args );
                        $new_content .= "<p><a target='_blank' href='". wp_lostpassword_url() ."'>Forgot Password?</a></p>";
                    //  $new_content .= "</div>";

        //              $new_content .= "<div class='column xs-100 md-60 member_join'>";
        //                 $new_content .= "<h5>Join the PEAK Community</h5>";
        //                 $new_content .= "<p>Start with Individual Membership, complimentary to all grantmaking professionals. You'll unlock access to member-exclusive insights, tools, and resources, our CONNECT community, and more.</p>";
        //                 $new_content .= "<p><a href='".get_bloginfo('url')."/join-us/#individual-memberships' class='btn'>Apply Now</a></p>";
        //              $new_content .= "</div>";
        //         $new_content .= "</div>";
        //     $new_content .= "</div>";
        // $new_content .= "</div>";
        $new_content .= "<script>
                        (function ($) {
                            $(document).ready(function () {
                            $('#user_login').attr('placeholder', 'Email Address');
                            $('#user_pass').attr('placeholder', 'Password');
                          });
                        })(jQuery);
                        </script>";
    }
    

    return $new_content;
}
add_shortcode('peak-login', __NAMESPACE__ . '\\peak_login_form');

/**
 * Document Download Shortcode, checks if logged in user has access to link
 *
 * @return shortcode
 * @author 
 **/
function peak_download_check($atts)
{
     extract(shortcode_atts(array(
        'template' => '5eea1ff0513cb',
        'id'       => '',
        'message'  => '',
    ), $atts));

    $has_access = wpdm_user_has_access($id);

    if($has_access || !is_user_logged_in()) {
        $comment = '';
    }else{
        $comment = "<p style='padding-top:20px;'><strong>".$message."</strong></p>"; 
    }

    $content = do_shortcode("[wpdm_package id='".$id."' template='".$template."']");
    $content .= $comment; 
    return $content;
}

add_shortcode('peak-download', __NAMESPACE__ . '\\peak_download_check');


/**
 * Visceral Query
 *
 * @return  The markup for a list of posts based on query parameters and template files.
 */
function visceral_query_shortcode($atts)
{
    extract(shortcode_atts(array(
        'container_class' => '',
        'template'        => 'views/partials/list-item',
        'show_pagination' => 0,
        'no_posts'        => '',
        'post_type'       => 'post',
        'posts_per_page'  => 6,
        'offset'          => 0,
        'order'           => 'DESC',
        'orderby'         => 'post_date',
        'post__not_in'    => '',
        'taxonomy'        => '',
        'terms'           => ''
    ), $atts));

    $markup = '';

    // Return either a single post type or an array of post types depending on how many were passed in.
    // Note: We need to do this because the Intuitive Custom Post Type plugin won't work with an array of a single post type.
    $post_type = (strpos($post_type, ',') > 0) ? explode(', ', $post_type) : $post_type;

    $args = array(
        'post_type'      => $post_type,
        'posts_per_page' => $posts_per_page,
        'offset'         => $offset,
        'order'          => $order,
        'orderby'        => $orderby,
        'post__not_in'   => explode(', ', $post__not_in),
    );
    if ($taxonomy && $terms) {
        $args['tax_query'] = array(
            array(
                'taxonomy' => $taxonomy,
                'field'    => 'slug',
                'terms'    => explode(', ', $terms),
            ),
        );
    }
    $query = new \WP_Query($args);

    if ($query->have_posts()) :
        $markup .= '<div class="' . $container_class . '">';
        while ($query->have_posts()) :
            $query->the_post();
            ob_start();
            include \App\template_path(locate_template($template . '-' . $post_type . '.blade.php'));
            $markup .= ob_get_contents();
            ob_end_clean();
        endwhile;
        $markup .= '</div>';
        wp_reset_postdata();

        // Pagination
        $total = $query->max_num_pages;
        // only bother with pagination if we have more than 1 page
        if ($show_pagination && $total > 1) :
            $big = 999999999; // need an unlikely integer
            $current_page = max(1, get_query_var('paged'));
            $pagination = paginate_links(array(
                'base'      => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                'format'    => '?paged=%#%',
                'current'   => $current_page,
                'total'     => $total,
                'type'      => 'plain',
                'prev_next' => true,
                'prev_text' => '<span class="icon-angle-left">' . __('Previous', 'visceral') . '</span>',
                'next_text' => '<span class="icon-angle-right">' . __('Next', 'visceral') . '</span>'
            ));

            $markup .= '<nav class="pagination text-center">';
            $markup .= $pagination;
            $markup .= '</nav>';
        endif;
    else :
        $markup = $no_posts;
    endif;

    return $markup;
}
add_shortcode('visceral_query', __NAMESPACE__ . '\\visceral_query_shortcode');


/* END SHORTCODES */

/* GENERAL PURPOSE FUNCTIONS */

/**
 * RELATED POSTS FUNCTION - Creates an array of related post objects
 *
 * @param int   $total_posts                 Total posts to display
 * @param mixed $post_type                   Either a string or array of post types to display
 * @param string $related_posts_field_name   Name of the manually selected related posts ACF field
 *
 * @return array                             Returns an of related posts
 */
function visceral_related_posts($posts_per_page = 3, $post_type = 'post', $related_posts_field_name = 'related_posts')
{
    $ID = get_the_ID();
    // Set up the an array to hold them
    $related_posts = array();
    // Get post ID's from related field
    // $selected_posts_ids = get_post_meta(null, $related_posts_field_name, true);
    $selected_posts_ids = get_field($related_posts_field_name);

    // Add each post object to our array
    // Subtract each post from total posts
    if ($selected_posts_ids) {
        $args = array(
            'include'   => $selected_posts_ids,
            'post_type' => $post_type
        );
        $selected_posts = get_posts($args);
        foreach ($selected_posts as $post_object) {
            array_push($related_posts, $post_object);
            $posts_per_page--;
        }
    }

    // If we need more posts, let's get some from recent posts
    if ($posts_per_page) {
        // Don't repeat the posts that we already have
        $exclude_posts = $selected_posts_ids ? $selected_posts_ids : []; // Make sure it's an array
        array_push($exclude_posts, get_the_id());

        // Exclude any events that have passed
        $args  = array(
            'post_type' => 'tribe_events',
            'meta_query' => array(
                array(
                    'key'     => '_EventStartDate',
                    'value'   => date('Y-m-d H:i:s'), //today
                    'compare' => '<=',
                )
            )
        );

        $past_events = get_posts($args);
        foreach ($past_events as $past) {
            array_push($exclude_posts, $past->ID);
        }

        if(is_page('insights') || is_singular('post')) {
            $news_topics = get_primary_taxonomy_term($ID, 'news_topic');
            if($news_topics){
                $terms_array = $news_topics;
            }else{
                $terms = get_the_terms($ID,'news_topic');
                $terms_array = array();

                if( $terms ) {
                    foreach ( $terms as $term ) {
                        $terms_array[] = $term->name;
                    }
                }
            }


            $args = array(
                'post_type' => $post_type,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'news_topic',
                        'field'    => 'name',
                        'terms'    => $terms_array,
                    ),
                ),
                'posts_per_page' => $posts_per_page,
                'exclude'        => $exclude_posts,
            );
        }else{
            $args = array(
                'post_type'      => $post_type,
                'posts_per_page' => $posts_per_page,
                'exclude'        => $exclude_posts,
            );
        }
       
        $even_more_posts = get_posts($args);
        foreach ($even_more_posts as $post_object) {
            array_push($related_posts, $post_object);
        }
    }

    return $related_posts;
}
// END RELATED POSTS FUNCTION


/**
 * Takes a string and returns a truncated version. Also strips out shortcodes
 *
 * @param  string  $text   String to truncate
 * @param  integer $length Character count limit for truncation
 * @param  string  $append Appended to the end of the string if character count exceeds limit
 * @return string          Truncated string
 */
function truncate_text($text = '', $length = 150, $append = '...')
{
    $new_text = preg_replace(" ([.*?])", '', $text);
    $new_text = strip_shortcodes($new_text);
    $new_text = strip_tags($new_text);
    $new_text = substr($new_text, 0, $length);
    if (strlen($new_text) == $length) {
        $new_text = substr($new_text, 0, strripos($new_text, " "));
        $new_text = $new_text . $append;
    }

    return $new_text;
}

/**
 * Returns an image with a class for fitting to a certain aspect ratio
 *
 * @param  integer $aspect_ratio_width   Aspect Ratio Width
 * @param  integer $aspect_ratio_height   Aspect Ratio Height
 * @param  string  $image_size   Size of featured image to return
 * @return string  markup of image
 */
function get_aspect_ratio_image($aspect_ratio_width = 1, $aspect_ratio_height = 1, $image_size = 'medium')
{
    if(get_post_thumbnail_id()) {
        $image = wp_get_attachment_image_src(get_post_thumbnail_id(), $image_size);
        $w     = $image[1];
        $h     = $image[2];
        if ($w > 0 && $h > 0) {
            $class = (($h / $w) > ($aspect_ratio_height / $aspect_ratio_width)) ? 'portrait' : '';
        } else {
            $class = '';
        }

        $image = get_the_post_thumbnail(get_the_id(), $image_size, array('class' => $class));
    } else {
        $image = '';
    }
    return $image;
}


/**
 * Related posts output
 *
 * @return  The markup for a list of posts based on query parameters and template files.
 */
function related_posts_shortcode($atts)
{

    // extract(shortcode_atts(array(
    // 	'sector' => ''
    // ), $atts));

    $related_content = visceral_related_posts(3, array('post', 'tribe_events', 'resource'), 'related_posts');

    $markup = '';

    $markup .= '<section class="related-content list-wrap">';
    $markup .= '<div class="row">';
    global $post;
    foreach ($related_content as $post) :
        setup_postdata($post);
        $markup .= \App\template('partials.list-item-related');
        wp_reset_postdata();
    endforeach;
    $markup .= '</div>';
    $markup .= '</section>';

    return $markup;
}
add_shortcode('related_posts', __NAMESPACE__ . '\\related_posts_shortcode');

/**
 * Regional Chapters
 * Carried over from old site
 */
// connect to database
function connectToDatabase()
{
    switch ( wp_get_environment_type() ) {
        case 'local':
            $localhost = 'localhost';
            $user      = 'root';
            $pass      = 'root';
            $db        = 'local';
            break;
        case 'staging':
            $localhost = 'localhost';
            $user      = 'a5f6268c_3100ec';
            $pass      = 'Need34Meant26Ragged54Lifers50';
            $db        = 'a5f6268c_3100ec';
            break;
        default:
            $localhost = 'localhost';
            $user      = 'a89bbc20_972889';
            $pass      = 'Penned16There99Melody52Shuns31';
            $db        = 'a89bbc20_972889';
            break;
    }

    //connection to the database
    $dbhandle = mysqli_connect($localhost, $user, $pass, $db);
    // Oh no! A connect_errno exists so the connection attempt failed!
    if ($dbhandle->connect_errno) {
        echo "Error: Could not connect to database.  Please submit this error <a href='contact'>here</a>.";
        die();
    }

    //select a database to work with
    return $dbhandle;
}

function closeDatabase($args)
{
    $args->close();
    return true;
}

function parseZip($z)
{

    // strval php 7.1.3
    // $a = strval(substr($z, 0, 3));

    // intval php 7.1.2
    $a = intval(substr($z, 0, 3));
    // $a = substr($z, 0, 3);

    if ($a >= "100" && $a <= "149") {
        return "New York"; // new york split
    }

    if ($a >= "005" && $a <= "069") {
        return "New York"; // conneticut split
    }

    if ($a >= "070" && $a <= "085") {
        return "New York"; // New Jersey split
    }

    if ($a >= "150" && $a <= "190") {
        return "New York"; // penns....
    }

    if ($a >= "197" && $a <= "199") {
        return "Delaware Valley"; // deleware....
    }

    if ($a >= "086" && $a <= "089") {
        return "Delaware Valley"; // NJ include princeton....
    }

    if ($a >= "191" && $a <= "196") {
        return "Delaware Valley"; // NJ include princeton....
    }

    if ($a >= "900" && $a <= "934") {
        return "Southern California"; // southern california regional chapter....
    }

    if ($a >= "935" && $a <= "999") {
        return "Northern California"; // northern california regional chapter....
    }

    return "E";
}

function parseRegions($s, $m)
{
    /* reference
    ZIP CODE SPLITS    
    New York: 100–149 (all of NY), 005-069 (CT), 070-085 (northern NJ), 150-190 (northern PA) - includes Pittsburgh)

    Delaware: 197-199 (all of DE), 086-089 (southern NJ) – includes Princeton, 191-196 (southeastern PA) – includes Philadelphia
    
    Midwest: Northeastern Nebraska – grandfathered members in this list
    
    Southern California Regional Chpt: 900-934
    
    Northern California Regional Chpt: 935-999
    
    */

    $s = trim(strtolower($s));
    $r = parseZip($m);

    // first pass  start
    if ($r !== 'E') { // if we have a sub zip
        // LETS RUN THESE FIRST
        // Southern New Jersey, Southeastern Pennsylvania, and Delaware
        if ($s == 'delaware') {
            return parseZip($m);
        }

        if ($s == 'new york' || $s == 'new jersey' || $s == 'conneticut' || $s == 'pennsylvania') {
            return parseZip($m);
        }

        if ($s == 'california' || $s == 'nevada') {
            return parseZip($m);
        } // this could be northern or southern california
    }

    if ($s == 'new york' || $s == 'new jersey' || $s == 'conneticut' || $s == 'pennsylvania') {
        return "New York";
    }

    if ($s == 'california' || $s == 'nevada') {
        return "Northern California";
    }

    if ($s == 'delaware') {
        return "Delaware Valley";
    }

    if ($s == 'district of columbia' || $s == 'maryland' || $s == 'virginia') {
        return "Washington D.C.";
    }

    if ($s == 'washington' || $s == 'oregon' || $s == 'idaho' || $s == 'montana' || $s == 'alaska' || $s == 'hawaii') {
        return "Pacific Northwest";
    }

    if ($s == 'maine' || $s == 'massachusetts' || $s == 'new hampshire' || $s == 'rhode island' || $s == 'vermont') {
        return "New England";
    }

    if ($s == 'alabama' || $s == 'arkansas' || $s == 'georgia' || $s == 'louisiana' || $s == 'mississippi' || $s == 'north carolina' || $s == 'south carolina' || $s == 'tennessee') {
        return "Southeast";
    }

    if ($s == 'arizona' || $s == 'utah' || $s == 'colorado' || $s == 'wyoming' || $s == 'new mexico') {
        return "Rocky Mountain";
    }

    if ($s == 'texas' || $s == 'oklahoma') {
        return "Texas";
    }

    if ($s == 'kentucky' || $s == 'west virginia' || $s == 'ohio') {
        return "Ohio";
    }

    if ($s == 'south dakota' || $s == 'north dakota' || $s == 'minnesota') {
        return "Minnesota";
    }

    if ($s == 'florida') {
        return "Florida";
    }

    if ($s == 'illinois' || $s == 'indiana' || $s == 'iowa' || $s == 'kansas' || $s == 'michigan' || $s == 'missouri' || $s == 'nebraska' || $s == 'wisconsin') {
        return "Midwest";
    }

    return "Error: Region no chapter in selected region.";
}

function regional_maps_shortcode()
{

    $output = '';

    // default zip / declare scope variables we need
    $emp = false;
    if ($_GET['address']) {
        $zip = $_GET['address'];
    } else {
        $emp = true;
        $zip = '10001';
    }
    if (strlen($zip) > 6) {
        $emp = true;
        $zip = '10001';
    }

    $states = array(
        'AL' => 'Alabama',
        'AK' => 'Alaska',
        'AS' => 'American Samoa',
        'AZ' => 'Arizona',
        'AR' => 'Arkansas',
        'CA' => 'California',
        'CO' => 'Colorado',
        'CT' => 'Connecticut',
        'DE' => 'Delaware',
        'DC' => 'District of Columbia',
        'FM' => 'Federated States of Micronesia',
        'FL' => 'Florida',
        'GA' => 'Georgia',
        'GU' => 'Guam',
        'HI' => 'Hawaii',
        'ID' => 'Idaho',
        'IL' => 'Illinois',
        'IN' => 'Indiana',
        'IA' => 'Iowa',
        'KS' => 'Kansas',
        'KY' => 'Kentucky',
        'LA' => 'Louisiana',
        'ME' => 'Maine',
        'MH' => 'Marshall Islands',
        'MD' => 'Maryland',
        'MA' => 'Massachusetts',
        'MI' => 'Michigan',
        'MN' => 'Minnesota',
        'MS' => 'Mississippi',
        'MO' => 'Missouri',
        'MT' => 'Montana',
        'NE' => 'Nebraska',
        'NV' => 'Nevada',
        'NH' => 'New Hampshire',
        'NJ' => 'New Jersey',
        'NM' => 'New Mexico',
        'NY' => 'New York',
        'NC' => 'North Carolina',
        'ND' => 'North Dakota',
        'MP' => 'Northern Mariana Islands',
        'OH' => 'Ohio',
        'OK' => 'Oklahoma',
        'OR' => 'Oregon',
        'PW' => 'Palau',
        'PA' => 'Pennsylvania',
        'PR' => 'Puerto Rico',
        'RI' => 'Rhode Island',
        'SC' => 'South Carolina',
        'SD' => 'South Dakota',
        'TN' => 'Tennessee',
        'TX' => 'Texas',
        'UT' => 'Utah',
        'VT' => 'Vermont',
        'VI' => 'Virgin Islands',
        'VA' => 'Virginia',
        'WA' => 'Washington',
        'WV' => 'West Virginia',
        'WI' => 'Wisconsin',
        'WY' => 'Wyoming',
        'AE' => 'Armed Forces Africa \ Canada \ Europe \ Middle East',
        'AA' => 'Armed Forces America (Except Canada)',
        'AP' => 'Armed Forces Pacific'
    );

    $regionsLink = array(
        'Delaware Valley'     => '/delaware-valley',
        'Florida'             => '/florida',
        'Midwest'             => '/midwest',
        'Minnesota'           => '/minnesota',
        'New England'         => '/new-england',
        'New York'            => '/new-york',
        'Ohio'                => '/ohio',
        'Northern California' => '/northern-california',
        'Southern California' => '/southern-california',
        'Pacific Northwest'   => '/pacific-northwest',
        'Rocky Mountain'      => '/rocky-mountain',
        'Southeast'           => '/southeast',
        'Texas'               => '/texas',
        'Washington D.C.'     => '/washington-d-c'
    );

    ob_start();

    $output .= '<div id="regionText" class="regional-map__results">';

    if ($zip && $emp == false) {
        $db     = connectToDatabase() or die("ERROR");
        $zip    = $db->real_escape_string($zip);
        $query  = "SELECT * from zipcodes_2011 WHERE zipcode = '" . $zip . "'"; // grab the info for that user
        $result = $db->query($query);

        if ($result->num_rows == 0) {
            $output .= '<h4>No match for zip code ' . $zip . '. Sorry!</h4>';
        } else {
            while ($row = $result->fetch_array()) {
                $s      = $row['state'];
                $c      = $row['city'];
                $state  = $states[$s];
                $region = parseRegions($state, $zip);
                $rLink  = $regionsLink[$region];

                $output .= '<h5>Your search for <strong>' . strtolower($c) . ', ' . $state . '</strong> has returned the following matching region: <strong>'  . $region . '</strong></h5>';
                // $output .= '<h4>Matching Region: <a href="' . $rLink . '-regional-chapter">'  . $region . ' Regional Chapter</a></h4>';
            }
        }

        $closed = closeDatabase($db);
    }

    $output .= '</div>';

    $output .= '<form name="address" method="GET" class="regional-map__form">';
    $output .= '<input id="address" name="address" type="text" value="" class="key-numeric" maxlength="5"></input>';
    $output .= '<input type="submit" name="submit" value="Get Region">';
    $output .= '</form>';

    $output .= ob_get_contents();
    ob_end_clean();

    return $output;
}

add_shortcode('regional_maps', __NAMESPACE__ . '\\regional_maps_shortcode');


function competency_model_shortcode()
{

    // Start object caching or output
    ob_start();

    // Set the template we'll use for the shortcode
    $template = 'partials/competency-model';

    // Set up template data
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);

    // Echo the shortcode blade template
    echo Template($template, $data);

    // Return cached object
    return ob_get_clean();
}
add_shortcode('competency_model', __NAMESPACE__ . '\\competency_model_shortcode');

/**
 * Outputs Resources on Principles page
 * Layout emulates a resource list item
 * 
 */
function principles_resources_shortcode()
{
    $output = '';

    // Start off with a check for custom header image (ACF). Return value should be set to Image ID
    if (function_exists('have_rows') && have_rows('principles_resources') != '') {

        if (have_rows('principles_resources')) {
            $output .= '<div class="list-wrap">';
            $output .= '<div class="row">';
            while (have_rows('principles_resources')) {
                the_row();
                $image        = get_sub_field('image') ? wp_get_attachment_image_src(get_sub_field('image'), 'large') : '';
                $title        = get_sub_field('title');
                $type         = get_sub_field('type') ? '<p class="list-item__type"><strong>' . get_sub_field('type') . '</strong></p>' : '';
                $url          = get_sub_field('url');
                $members_only = get_sub_field('members_only') ? '<span class="list-item__locked"><span class="icon-lock"></span> Members only</span>' : '';
                $card_image_class = $image ? 'list-item--featured-image' : '';

                $output .= '<article class="column xs-100 sm-50 lg-33 reveal">';
                $output .= '    <a href="' .  $url . '" class="list-item list-item--principle ' . $card_image_class . '">';
                // $output .= '        <a href="' .  $url . '" class="list-item__link">';
                if ($image) :
                    $output .= '                <div class="list-item__image image-zoom">';
                    $output .= '                    <div class="img-cover"><img src="' . $image[0] . '" width=' . $image[1] . ' height="' . $image[2] . '" alt="' . $image[3] . '" /></div>';
                    $output .= '                </div>';
                endif;
                $output .= '            <div class="list-item__content">';
                $output .= '                <h3 class="list-item__title">' .  $title . '</h3>';
                $output .= '            </div>';
                $output .=                  $type;
                $output .=                  $members_only;
                $output .= '        </a>';
                // $output .= '    </div>';
                $output .= '</article>';
            }
            $output .= '</div>';
            $output .= '</div>';
        }
    }

    // Return cached object
    return $output;
}
add_shortcode('principles_resources', __NAMESPACE__ . '\\principles_resources_shortcode');

/**
 * Return list of users for output on board of directors page
 * 
 */
function board_listing_shortcode()
{
    $args = array(
        'taxonomy' => 'user-type',
        'term'     => 'board-of-directors',
    );

    $user_args = array(
        'meta_key' => 'last_name',
        'orderby'  => 'meta_value',
        'order'    => 'ASC'
    );

    $board = wp_get_users_of_group( $args, $user_args );

    $m = '';

    if($board) {
        $m.= '<div class="row">';
          foreach($board as $user) {
            $m .= \App\template('partials.list-item-board', ['ID' => $user->ID]);
            wp_reset_postdata();
          }
        $m.= '</div>';
    }

    return $m;
}
add_shortcode('board_listing', __NAMESPACE__ . '\\board_listing_shortcode');

/**
 * WPBakery Page Builder overrides
 * Remove unneeded elements/options
 *
 * Hooks into 'init' action
 * 
 */
if (function_exists("vc_remove_element")) {
    // Remove default elements
    vc_remove_element('vc_facebook');
    vc_remove_element('vc_tweetmeme');
    vc_remove_element('vc_googleplus');
    vc_remove_element('vc_pinterest');
    vc_remove_element('vc_widget_sidebar');
    vc_remove_element('vc_flickr');
    vc_remove_element('vc_basic_grid ');
    vc_remove_element('vc_media_grid ');
    vc_remove_element('vc_wp_search');
    vc_remove_element('vc_wp_meta');
    vc_remove_element('vc_wp_recentcomments');
    vc_remove_element('vc_wp_calendar');
    vc_remove_element('vc_wp_pages');
    vc_remove_element('vc_wp_tagcloud');
    vc_remove_element('vc_wp_text');
    vc_remove_element('vc_wp_categories');
    vc_remove_element('vc_wp_archives');
    vc_remove_element('vc_wp_rss');

    // Remove extraneous options from buttons element
    vc_remove_param("vc_btn", "shape");
    // vc_remove_param( "vc_btn", "style" );
    vc_remove_param("vc_btn", "custom_background");
    vc_remove_param("vc_btn", "gradient_color_1");
    vc_remove_param("vc_btn", "gradient_color_2");
    vc_remove_param("vc_btn", "gradient_custom_color_1");
    vc_remove_param("vc_btn", "gradient_custom_color_2");
    vc_remove_param("vc_btn", "gradient_text_color");
    vc_remove_param("vc_btn", "gradient_color_2");
    vc_remove_param("vc_btn", "custom_text");
    vc_remove_param("vc_btn", "outline_custom_color");
    vc_remove_param("vc_btn", "outline_custom_hover_background");
    vc_remove_param("vc_btn", "outline_custom_hover_text");
}

function vc_update_defaults()
{
    if (function_exists('vc_update_shortcode_param')) {
        // Add custom colors to buttons
        $colors_arr = array(
            __('Burnt Red (Primary)', 'js_composer') => 'burnt-red',
            __('Dark Red', 'js_composer')            => 'dark-red',
            __('Teal', 'js_composer')                => 'teal',
            __('Light Blue', 'js_composer')          => 'light-blue',
            __('Steel Gray', 'js_composer')          => 'steel-gray',
        );

        $param = \WPBMap::getParam('vc_btn', 'color');
        $param['value'] = $colors_arr;

        vc_update_shortcode_param('vc_btn', $param);

        // Add custom styles to buttons
        $style_arr = array(
            __('Outline (Default)', 'js_composer') => 'default',
            __('Reverse', 'js_composer')           => 'reverse',
            __('Filled', 'js_composer')            => 'fill',
        );

        $styleParam = \WPBMap::getParam('vc_btn', 'style');
        $styleParam['value'] = $style_arr;

        vc_update_shortcode_param('vc_btn', $styleParam);
    }
}

add_action('vc_after_init', __NAMESPACE__ . '\\vc_update_defaults');


/**
 * Returns the primary term for the chosen taxonomy set by Yoast SEO
 * or the first term selected.
 *
 * @param integer $post The post id.
 * @param string  $taxonomy The taxonomy to query. Defaults to category.
 * @return array The term with keys of 'title', 'slug', and 'url'.
 */
function get_primary_taxonomy_term( $post = 0, $taxonomy = 'category' ) {
    if ( ! $post ) {
        $post = get_the_ID();
    }

    $terms        = get_the_terms( $post, $taxonomy );
    $primary_term = array();

    if ( $terms ) {
        $term_display = '';
        $term_slug    = '';
        $term_link    = '';
        if ( class_exists( 'WPSEO_Primary_Term' ) ) {
            $wpseo_primary_term = new \WPSEO_Primary_Term( $taxonomy, $post );
            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
            $term               = get_term( $wpseo_primary_term );
            if ( is_wp_error( $term ) ) {
                $term_display = $terms[0]->name;
                $term_slug    = $terms[0]->slug;
                $term_link    = get_term_link( $terms[0]->term_id );
            } else {
                $term_display = $term->name;
                $term_slug    = $term->slug;
                $term_link    = get_term_link( $term->term_id );
            }
        } else {
            $term_display = $terms[0]->name;
            $term_slug    = $terms[0]->slug;
            $term_link    = get_term_link( $terms[0]->term_id );
        }
        $primary_term['url']   = $term_link;
        $primary_term['slug']  = $term_slug;
        $primary_term['title'] = $term_display;
    }
    return $primary_term;
}

function peakMemberHasAccess() {

    $inRoles = array();
    $restrictedcontent = get_field('restrict_content');
    $allowedToAccess = get_field('member_access');

    if($restrictedcontent) {
        if ( is_user_logged_in() ) {
            $user = wp_get_current_user();
            if ( !empty( $user->roles ) && is_array( $user->roles ) ) {

                foreach ( $user->roles as $role => $name ) {
                    array_push($inRoles, $name);

                    if(in_array($inRoles[0],$allowedToAccess) || !$allowedToAccess || current_user_can('administrator') || current_user_can('editor') || current_user_can('author') ){
                        return true;
                    }else{
                        return false;
                    }
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }else{
        return true;
    }
}

function array_flatten($array) {
   $return = array();
   foreach ($array as $key => $value) {
       if (is_array($value)){ $return = array_merge($return, array_flatten($value));}
       else {$return[$key] = $value;}
   }
   return $return;
}



/**
 * Organizations Listing
 *
 * @return  The markup for a list of posts based on query parameters
 */
function peak_organization_list($atts)
{
    extract(shortcode_atts(array(
        'type' => ''
    ), $atts));

    $markup = '';

    $args = array(
        'post_type'       => 'organization',
        'posts_per_page'  => -1,
        'order'           => 'ASC',
        'orderby'         => 'post_title',
    );

    if (!empty($type)) {
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'organization-type',
                'field'    => 'slug',
                'terms'    => explode(', ', $type),
            ),
        );
    }

    $today = date('F j, Y');

    $query = new \WP_Query($args);

    if ($query->have_posts()) :
        $markup .= '<ul class="list-columns">';
        while ($query->have_posts()) :
            $query->the_post();
            $date = (function_exists('get_field') && get_field('membership_end_date', get_the_ID())) ? get_field('membership_end_date', get_the_ID()) : false; // stored as a string so can't be used in meta_query
            $title = (function_exists('get_field') && get_field('dba_name')) ? get_field('dba_name') : get_the_title();
            $sustaining = (function_exists('get_field') && get_field('sustaining_member')) ? get_field('sustaining_member') : false;

            // Only display if the membership end date is in the future
            if($date) {
                if((strtotime($date) > strtotime($today))) {
                    // If title string beings with 'The' remove it
                    if (substr($title, 0, 3) === 'The') {
                        $title = substr($title, 4);
                    }

                    if($sustaining) {
                        $markup .= '<li class=""><strong>' . $title . '</strong></li>';
                    } else {
                        $markup .= '<li class="">' . $title . '</li>';
                    }
                }
            }
        endwhile;
        $markup .= '</ul>';
        wp_reset_postdata();

    else :
        $markup = '<p>Sorry, no organizations were found.</p>';
    endif;

    return $markup;
}
add_shortcode('peak_org_listing', __NAMESPACE__ . '\\peak_organization_list');


/**
 * Donations Listing
 *
 * @return  The markup for a list of posts based on query parameters
 */
function peak_donation_list($atts)
{
    extract(shortcode_atts(array(
        'type' => ''
    ), $atts));

    $markup = '';

    $args = array(
        'post_type'       => 'donation',
        'posts_per_page'  => -1,
        'order'           => 'DESC',
        'orderby'         => 'post_date',
    );

    $query = new \WP_Query($args);

    if ($query->have_posts()) :
        $markup .= '<div class="row">';
        while ($query->have_posts()) :
            $query->the_post();
                $ID = get_the_ID();
                $amount = get_post_meta($ID, 'donation_amount', true) ? get_post_meta($ID, 'donation_amount', true) : '';
                $honorary = get_post_meta($ID, 'in_honor_of', true) ? get_post_meta($ID, 'in_honor_of', true) : '';

                $markup .= '<div class="column xs-50 sm-33 m-b-32">';
                $markup .= '<div class="list-item-donation">';
                $markup .= '<h4 class="m-b-8">' . get_the_title() . '</h4>';

                if( $amount ) {
                    $markup .= '<p class="small m-b-8"><strong>Amount: </strong> $' . number_format($amount, 0) . '</p>';
                }
                if( $honorary ) {
                    $markup .= '<p class="small"><strong>In Honor Of: </strong>' . $honorary . '</p>';
                }
                $markup .= '</div>';
                $markup .= '</div>';

        endwhile;
        $markup .= '</div>';
        wp_reset_postdata();

    else :
        $markup = '<p>Sorry, no organizations were found.</p>';
    endif;

    return $markup;
}
add_shortcode('peak_donation_listing', __NAMESPACE__ . '\\peak_donation_list');


/**
 * Committee Listing
 *
 * @return  The markup for a list of committee members posts based on the committee name
 */
function peak_committee_list($atts)
{
    extract(shortcode_atts(array(
        'committee' => ''
    ), $atts));

    $markup = '';

    $args = array(
        'post_type'       => 'committee',
        'posts_per_page'  => -1,
        'order'           => 'ASC',
        'orderby'         => 'post_title',
    );

    if (!empty($committee)) {
        $args['meta_query'] = array(
            array(
                'key'   => 'committee_name',
                'value' => $committee,
            ),
        );
    }

    $query = new \WP_Query($args);

    if ($query->have_posts()) :
        $markup .= '<ul>';
        while ($query->have_posts()) :
            $query->the_post();
                $position = get_post_meta(get_the_ID(), 'committee_position', true);
                $organization = get_post_meta(get_the_ID(), 'committee_member_organization', true);

                $markup .= '<li class="">' . get_the_title();
                if($organization) {
                    $markup .= ', ' . $organization;
                }
                if($position) {
                    $markup .= ', <em>' . $position . '</em>';
                }
                $markup .= '</li>'; // Name
                
        endwhile;
        $markup .= '</ul>';
        wp_reset_postdata();

    else :
        $markup = '<p>Sorry, no committee members were found.</p>';
    endif;

    return $markup;
}
add_shortcode('peak_committee_list', __NAMESPACE__ . '\\peak_committee_list');
