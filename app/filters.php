<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});


/**
 * Remove tags from posts
 */
function remove_tags()
{
    unregister_taxonomy_for_object_type('post_tag', 'post');
}
add_action('init', __NAMESPACE__ . '\\remove_tags');

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__ . '\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory() . '/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Tell WordPress how to find the compiled path of comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );
    return template_path(locate_template(["views/{$comments_template}", $comments_template]) ?: $comments_template);
});


/**
 * Allow SVG Uploads
 */
function theme_allow_svg_uploads($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', __NAMESPACE__ . '\\theme_allow_svg_uploads');
// End Allow SVG Uploads

/**
 * Custom Font Formats
 */
function theme_tiny_mce_formats_button($buttons)
{
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', __NAMESPACE__ . '\\theme_tiny_mce_formats_button');

// Callback function to filter the MCE settings
function theme_tiny_mce_custom_font_formats($init_array)
{
    // Define the style_formats array
    $style_formats = array(
        // Each array child is a format with it's own settings
        array(
            'title'    => 'Split Divider',
            'selector' => '*',
            'classes'  => 'split-divider'
        ),
        array(
            'title'    => 'Button',
            'selector' => 'a',
            'classes'  => 'btn button'
        ),
        array(
            'title'   => 'Number Ticker',
            'inline'  => 'span',
            'classes' => 'ticker'
        ),
        array(
            'title'    => 'Link with arrow',
            'selector' => 'a',
            'classes'  => 'link--arrow'
        ),
        array(
            'title'    => 'Callout Link',
            'selector' => 'a',
            'classes'  => 'link--callout'
        ),
        array(
            'title'    => 'Gated Content Link',
            'selector' => 'a',
            'classes'  => 'link--gated'
        ),
        array(
            'title'    => 'Members Only Label',
            'inline'   => 'span',
            'classes'  => 'label--gated'
        ),
        array(
            'title'    => 'Checklist',
            'selector' => 'ul',
            'classes'  => 'checklist'
        )
        ,
        array(
            'title'    => 'Video popup',
            'selector' => 'a',
            'classes'  => 'video-modal'
        )
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode($style_formats);
    return $init_array;
}
add_filter('tiny_mce_before_init', __NAMESPACE__ . '\\theme_tiny_mce_custom_font_formats'); // Attach callback to 'tiny_mce_before_init'
// End Custom Font Formats

/**
 * Query for all post statuses so attachments are returned
 */
function visceral_modify_link_query_args($query)
{
    // Attachment post types have status of inherit. This allows them to be included.
    $query['post_status'] = array('publish', 'inherit');
    return $query;
}
add_filter('wp_link_query_args', __NAMESPACE__ . '\\visceral_modify_link_query_args');

/**
 * Link to media file URL instead of attachment page
 */
function visceral_modify_link_query_results($results, $query)
{
    foreach ($results as &$result) {
        if ('Media' === $result['info']) {
            $result['permalink'] = wp_get_attachment_url($result['ID']);
        }
    }
    return $results;
}

add_filter('wp_link_query', __NAMESPACE__ . '\\visceral_modify_link_query_results');

/**
 * Filter Search by Post Type
 */
add_filter( 'searchwp\swp_query\args', function( $args ) {
  if ( isset( $_GET['filter'] ) ) {
    $args['post_type'] = array(sanitize_text_field($_GET['filter']));
  }
  return $args;
} );

/**
 * FILTER SEARCH BY POST TYPE
 */

add_action( 'pre_get_posts', function( $query ) {
    if ($query->is_search() && $query->is_main_query() && !empty( $_GET['filter'] )) {
        $query->query_vars['post_type'] = sanitize_text_field($_GET['filter']);
    }
});
  // END FILTER SEARCH BY POST TYPE

// Changes 'Posts' label to Insights
function change_post_label() {
        global $wp_post_types;
        $labels = &$wp_post_types['post']->labels;
        $labels->name = 'Insights';
        $labels->singular_name = 'Insight';
}
add_action( 'init', __NAMESPACE__ . '\\change_post_label' );

/**
 * LOWER YOAST META BOX
 */
function yoast_dont_boast($html)
{
    return 'low';
}
add_filter('wpseo_metabox_prio', __NAMESPACE__ . '\\yoast_dont_boast');
// END LOWER YOAST META BOX


if (!function_exists('visc_remove_personal_options')) {
    function visc_remove_personal_options($subject)
    {
        $subject = preg_replace('#<h2>' . __("Personal Options") . '</h2>#s', '', $subject, 1); // Remove the "Personal Options" title
        $subject = preg_replace('#<tr class="user-rich-editing-wrap(.*?)</tr>#s', '', $subject, 1); // Remove the "Visual Editor" field
        $subject = preg_replace('#<tr class="user-comment-shortcuts-wrap(.*?)</tr>#s', '', $subject, 1); // Remove the "Keyboard Shortcuts" field
        // $subject = preg_replace('#<tr class="show-admin-bar(.*?)</tr>#s', '', $subject, 1); // Remove the "Toolbar" field
        $subject = preg_replace('#<h2>' . __("Name") . '</h2>#s', '<h2>' . __("Basic Information") . '</h2>', $subject, 1); // Remove the "Name" title
        // $subject = preg_replace('#<tr class="user-display-name-wrap(.*?)</tr>#s', '', $subject, 1); // Remove the "Display name publicly as" field
        $subject = preg_replace('#<h2>' . __("Contact Info") . '</h2>#s', '', $subject, 1); // Remove the "Contact Info" title
        // $subject = preg_replace('#<tr class="user-url-wrap(.*?)</tr>#s', '', $subject, 1); // Remove the "Website" field
        $subject = preg_replace('#<h2>' . __("About Yourself") . '</h2>#s', '', $subject, 1); // Remove the "About Yourself" title
        $subject = preg_replace('#<h2>' . __("About the user") . '</h2>#s', '', $subject, 1); // Remove the "About Yourself" title
        $subject = preg_replace('#<tr class="user-description-wrap(.*?)</tr>#s', '', $subject, 1); // Remove the "Biographical Info" field
        // $subject = preg_replace('#<tr class="user-profile-picture(.*?)</tr>#s', '', $subject, 1); // Remove the "Profile Picture" field
        return $subject;
    }

    function visc_profile_subject_start()
    {
        // if ( ! current_user_can('manage_options') ) {
        ob_start(__NAMESPACE__ . '\\visc_remove_personal_options');
        // }
    }

    function visc_profile_subject_end()
    {
        // if ( ! current_user_can('manage_options') ) {
        ob_end_flush();
        // }
    }
}
add_action('admin_head', __NAMESPACE__ . '\\visc_profile_subject_start');
add_action('admin_footer', __NAMESPACE__ . '\\visc_profile_subject_end');

/**
 * Events Calendar
 */
function remove_export()
{
    return false;
}
add_filter('tribe_events_list_show_ical_link',  __NAMESPACE__ . '\\remove_export', 10, 0);

/**
 * Author URL
 */
// The first part //
// add_filter('author_rewrite_rules', __NAMESPACE__ . '\\no_author_base_rewrite_rules');
// function no_author_base_rewrite_rules($author_rewrite) { 
//     global $wpdb;
//     $author_rewrite = array();
//     $authors = $wpdb->get_results("SELECT user_nicename AS nicename from $wpdb->users");    
//     foreach($authors as $author) {
//         $author_rewrite["({$author->nicename})/page/?([0-9]+)/?$"] = 'index.php?author_name=$matches[1]&paged=$matches[2]';
//         $author_rewrite["({$author->nicename})/?$"] = 'index.php?author_name=$matches[1]';
//     }   
//     return $author_rewrite;
// }

// add_filter('author_link',  __NAMESPACE__ . '\\no_author_base', 1000, 3);
// function no_author_base($link, $author_id,) {
//     $link_base = trailingslashit(get_option('home'));
//     $link = preg_replace("|^{$link_base}author/|", '', $link);
//     return $link_base . $link;
// }

function edit_breadcrumb($link_output, $link)
{
    $link_output = '';

    if (isset($link['text']) && (is_string($link['text']) && $link['text'] !== '')) {

        $link['text'] = trim($link['text']);
        if (!isset($link['allow_html']) || $link['allow_html'] !== true) {
            $link['text'] = esc_html($link['text']);
        }

        $title_attr   = isset($link['title']) ? ' title="' . esc_attr($link['title']) . '"' : '';

        $link_output .= '<a class="breadcrumbs__link" href="' . esc_url($link['url']) . '" ' . $title_attr . '><span class="icon-arrow-left"></span>' . $link['text'] . '</a>';
    }

    return $link_output;
}

add_filter('wpseo_breadcrumb_single_link',  __NAMESPACE__ . '\\edit_breadcrumb', 10, 2);

/**
 * Menu ACF
 */
add_filter('wp_nav_menu_objects',  __NAMESPACE__ . '\\visc_wp_nav_menu_objects', 10, 2);
function visc_wp_nav_menu_objects($items)
{
    foreach ($items as &$item) {
        if (get_field('menu_subline', $item)) {
            $item->title .= ' <span class="menu-item-subline">' . get_field('menu_subline', $item) . '</span>';
        }
    }

    return $items;
}


//Additions Redstart


/**
 * Remove Categories taxonomy from view.
 */

add_action( 'init', __NAMESPACE__ . '\\unregister_taxonomy');

function unregister_taxonomy()
{
    unregister_taxonomy_for_object_type('category', 'post');
}



/**
 * Custom ACF field options for dropdowns, member access
 */

add_filter('acf/load_field/name=member_access', __NAMESPACE__ . '\\populateUserGroups');
function populateUserGroups( $field )
{   
    // reset choices
    $field['choices'] = array();
    
    global $wp_roles;
    $roles = $wp_roles->get_names();
    
    foreach ($roles as $role => $key) {
        $field['choices'][ $role ] = $role;
    }

    return $field;
}



/**
 * Custom ACF field options for selecting a grantmaking principle
 */

function related_principle_query( $args, $field, $post )
{
    
    $args['post__in'] = array(27,17,21,23,19);
    return $args;
}
add_filter('acf/fields/post_object/query/name=grantmaking_principle', __NAMESPACE__ . '\\related_principle_query', 10, 3);


/**
 * Disable profile if not admin/editor
 */

add_action( 'load-profile.php', function() {
    if( ! current_user_can( 'manage_options' ) )
        exit( wp_safe_redirect( admin_url() ) );
} );

/**
 * Redirect signle download manager posts to homepage
 * Excluding posts tagged as 'video'
 */

add_action( 'template_redirect', __NAMESPACE__ . '\\redirect_downloads_single',10,3 );
function redirect_downloads_single() {
    $categories = wp_get_post_terms( get_the_id(), 'wpdmcategory', array('fields' => 'names') );
    if ( !is_singular( 'wpdmpro' ) || in_array('Video', $categories) ) return;

    wp_redirect( get_bloginfo('url' ), 301 );
    exit;
}

/**
 * Overwrite number of posts for journals
 */

function peak_change_journals_per_page( $query ) {
    if ( is_admin() || ! $query->is_main_query() ) {
       return;
    }

    if ( is_post_type_archive( 'journals' ) ) {
       $query->set( 'posts_per_page', -1 );
    }
}
add_filter( 'pre_get_posts', __NAMESPACE__ . '\\peak_change_journals_per_page' );


/**
 * Add Login/Logout Links
 */

add_filter( 'wp_nav_menu_items', __NAMESPACE__ . '\\peak_loginout_menu_link', 10, 2 );
function peak_loginout_menu_link( $items, $args ) {
   if ($args->theme_location == 'quicklinks') {
      if (is_user_logged_in()) {
         global $current_user; 
         wp_get_current_user();

         $start_items = '<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-105"><span> Hello, '. $current_user->first_name .'</span>&nbsp;|&nbsp;<a href="'. wp_logout_url(get_permalink()) .'"><i class="fas fa-sign-out-alt"></i> Logout</li>';
      } else {
         $start_items = '<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-105"><a href="'. site_url('/login/?redirect_to='.get_permalink()) .'"><span class="icon icon-lock"></span> '. __("Log In") .'</a></li>';
      }
      $new_items = $start_items . $items;
      return $new_items;
   }

   return $items;  
}


/**
 * Edit wp default login page.
 */

function peak_login_logo() { 

     $logo = wp_get_attachment_image_src( 15392 , 'full' );

    ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo $logo[0]; ?>);
        height:65px;
        width:320px;
        background-size: 320px 65px;
        background-repeat: no-repeat;
            padding-bottom: 30px;
        }
        .login {
            background: #ffffff;
            background: -moz-linear-gradient(top,  #ffffff 0%, #a5a5a5 100%);
            background: -webkit-linear-gradient(top,  #ffffff 0%,#a5a5a5 100%);
            background: linear-gradient(to bottom,  #ffffff 0%,#a5a5a5 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#a5a5a5',GradientType=0 );

        }
        .login.wp-core-ui .button-primary {
            border-radius: 0;
            font-size: 1.1em;
            font-weight: 700;

        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', __NAMESPACE__ . '\\peak_login_logo' );

// /**
//  * Change url for login logo
//  */
// function peak_login_logo_url() {
//     return home_url();
// }
// add_filter( 'login_headerurl', __NAMESPACE__ . '\\peak_login_logo_url' );

// /**
//  * Change url for reset password
//  * @return bool
//  */
// add_filter( 'lostpassword_url',  __NAMESPACE__ . '\\peak_lostpassword_url', 10, 0 );
//     function peak_lostpassword_url() {
//         return 'https://netforumpro.com/eweb/DynamicPage.aspx?WebCode=ForgotPassword&Site=GMN';
// }

function peak_logout_redirect( $logouturl, $redir )
{
    return $logouturl . '&amp;redirect_to=' . get_permalink();
}
add_filter( 'logout_url', __NAMESPACE__ . '\\peak_logout_redirect', 10, 2 );


add_filter( 'wp_nav_menu_objects', __NAMESPACE__ . '\\peak_add_param' );

/**
 * Add a parameter to item URLs.
 *
 * @wp-hook wp_nav_menu_objects
 * @param   array $items
 * @return  array
 */
function peak_add_param( $items )
{
    $out = array ();

    if(isset($_COOKIE['ssoToken'])){
        foreach ( $items as $item )
        {
            //Check for "sso" in css classes to add parameter
            if (in_array("sso", $item->classes)){
                $item->url = add_query_arg( 'ssoToken', $_COOKIE['ssoToken'], $item->url );
            }
            
            $out[] = $item;
        }

        return $out;
    }else{
        return $items;
    }

    
}


/**
 * Block wp-admin access for non-admins
 */
function peak_block_wp_admin() {
    if ( is_admin() && ! current_user_can( 'administrator' ) && ! current_user_can( 'editor' ) && ! current_user_can( 'author' ) && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
            wp_safe_redirect( home_url() );
        exit;
    }
}
add_action( 'admin_init', __NAMESPACE__ . '\\peak_block_wp_admin' );

// Jobs - set expiration date from gform submission
add_action( 'gform_pre_submission_16', function( $form ) {
    // Get value from date dropdown
    $length = rgpost( 'input_6' );

    date_default_timezone_set('America/New_York');

    // Get today's date
    $today = date('Y-m-d');

    // determine expiration date
    $expiration = date('Y-m-d',strtotime(date('Y-m-d', time()) . ' + ' . $length));

    // Set post expiration gfield value
    $_POST['input_11'] = $expiration;
});

// Loop through job posts

// Check if post expiration date field has a value

// If it does, and the date matches today, unpublish the post

// Format location (list) value to City, State for taxonomy mapping
add_action( 'gform_pre_submission_16', __NAMESPACE__ . '\\gfield_location_format' );
function gfield_location_format( $form ) {
    $location = '';

    // location field - returns as array
    $location = $_POST['input_15'];

    // replace commas with character code counterpart for correct taxonomy
    if(!empty($location)) {
        $location = str_replace(',', '&#44;', $location);

        // formatted location, set to hidden field
        $_POST['input_16'] = implode(', ', $location);
    }
}

// Add the custom View Count column to Downloads (wpdmpro) admin columns
add_filter( 'manage_wpdmpro_posts_columns', __NAMESPACE__ . '\\set_custom_edit_wpdmpro_columns' );
function set_custom_edit_wpdmpro_columns($columns) {
    $columns['view_count'] = __( 'View Count', 'visceral' );

    return $columns;
}

// Add the data to the custom columns for the book post type:
add_action( 'manage_wpdmpro_posts_custom_column' , __NAMESPACE__ . '\\custom_wpdmpro_column', 10, 2 );
function custom_wpdmpro_column( $column, $post_id ) {
    switch ( $column ) {
        case 'view_count' :
            $count = get_post_meta($post_id, '__wpdm_view_count', true);
            if ( $count )
                echo $count;
            break;
    }
}

// Modify Gravity Forms entries export to include Username and Email instead of User ID
// This filter adds 2 new column headers
add_filter( 'gform_export_fields', __NAMESPACE__ . '\\add_fields', 10, 1 );
function add_fields( $form ) {
    array_push( $form['fields'], array( 'id' => 'custom_field_username', 'label' => __( 'User Name', 'gravityforms' ) ) );
    array_push( $form['fields'], array( 'id' => 'custom_field_useremail', 'label' => __( 'User Email', 'gravityforms' ) ) );

    return $form;
}
// This filter gets the appropriate name and email
add_filter( 'gform_export_field_value', __NAMESPACE__ . '\\set_export_values', 10, 4 );
function set_export_values( $value, $form_id, $field_id, $entry ) {
    switch( $field_id ) {
    case 'custom_field_username' :
        $user  = get_user_by( 'id', $entry['created_by'] );
        $value = is_object( $user ) ? $user->display_name : '';
        break;
    case 'custom_field_useremail' :
      $user  = get_user_by( 'id', $entry['created_by'] );
      $value = is_object( $user ) ? $user->user_email : '';
        break;
    }
    return $value;
}

add_filter( 'wpseo_json_ld_output', __NAMESPACE__ . '\\yoast_seo_json_remove_partial' );
function yoast_seo_json_remove_partial() {
  if ( is_singular('post') ) {
    return false;
    }
  /* Use a second if statement here when needed */
}

/**
 * Function for `wpseo_meta_author` filter-hook.
 * 
 * @param string                 $author_name  The article author's display name. Return empty to disable the tag.
 * @param Indexable_Presentation $presentation The presentation of an indexable.
 *
 * @return string
 */
add_filter( 'wpseo_meta_author', __NAMESPACE__ . '\\override_yoast_meta', 30, 1 );
function override_yoast_meta( $author ) {
    if(is_singular('post')) {
        // Default to WP author
        $author = get_the_author(); 

        if(function_exists('get_field')) {
            if( have_rows('custom_authors') ) {
            	$count = count(get_field('custom_authors'));
            	$authors = array();

                while ( have_rows('custom_authors') ) : the_row();
            		$authors[] = get_sub_field('custom_author');
            		$author = implode(', ', $authors);
                endwhile;
            }elseif( get_field('custom_author') || get_field('custom_author_description') ) { // custom author
                $author = get_field('custom_author');
            }
        }
    }

    return $author;
}


/**
 *  Update enhanced Slack data on Posts
 */
add_filter('wpseo_enhanced_slack_data', function($data) {
    global $post;
    if ($post->post_type == 'post')
    {
        $ID = $post->ID;
        // Default to WP author
        $author_id = get_post_field( 'post_author', $ID );
        $author = get_the_author_meta( 'display_name', $author_id );

        if(function_exists('get_field')) {
            if( have_rows('custom_authors', $ID) ) {
            	$count = count(get_field('custom_authors', $ID));
            	$authors = array();

                while ( have_rows('custom_authors') ) : the_row();
            		$authors[] = get_sub_field('custom_author');
            		$author = implode(', ', $authors);
                endwhile;
            }elseif( get_field('custom_author', $ID)) { // custom author
                $author = get_field('custom_author', $ID);
            }
        }

        $data['Written by'] = $author;
        return $data;
    }
    else
    {
        return $data;
    }
});


/**
 * Alter the OpenGraph image for author pages
 */
add_filter('wpseo_opengraph_image', function($image) {
    if (is_author() && function_exists('get_field')) {
        $author_id = get_queried_object_id();
        $author_image = get_field('image', 'user_' . $author_id);
        if ($author_image) {
            $image = wp_get_attachment_url($author_id);
        }
    }
    return $image;
});

// Check if query param exists, if so, logout user and redirect to homepage
add_action('init', __NAMESPACE__ . '\\peak_logout_user');
function peak_logout_user() {
    if (isset($_GET['amslogout'])) {
        wp_logout(home_url());
    }
}

// Hide admin bar for non-admins
add_action('after_setup_theme', __NAMESPACE__ . '\\peak_hide_admin_bar');
function peak_hide_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

// For migrated Events: Set the correct start/end dates based on the Salesforce UTC date
function sync_event_dates_from_salesforce($post_id, $post, $update) {
    // Only set for post_type = tribe_events
	if ( 'tribe_events' !== $post->post_type ) {
		return;
	}

    // Retrieve the UTC start date
    $event_start_date_utc = get_post_meta($post_id, '_EventStartDateUTC', true);
    
    // Update the local start date
    if (!empty($event_start_date_utc)) {
        // Convert the UTC date to the local timezone
        $event_start_date = get_date_from_gmt($event_start_date_utc, 'Y-m-d H:i:s');

        update_post_meta($post_id, '_EventStartDate', $event_start_date);
    }

    $event_end_date_utc = get_post_meta($post_id, '_EventEndDateUTC', true);

    // Update the local end date
    if (!empty($event_end_date_utc)) {
        // Convert the UTC date to the local timezone
        $event_end_date = get_date_from_gmt($event_end_date_utc, 'Y-m-d H:i:s');

        update_post_meta($post_id, '_EventEndDate', $event_end_date);
    }
}

add_action('save_post', __NAMESPACE__ . '\\sync_event_dates_from_salesforce', 10, 3);

/*  DISABLE GUTENBERG STYLE IN HEADER| WordPress 5.9 and up */
function wps_deregister_styles() {
    wp_dequeue_style( 'global-styles' );
}
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\wps_deregister_styles', 100 );

// Disable Akismet for specific Gravity Form (Submit a Job)
add_filter( 'gform_akismet_enabled_16', '__return_false' );