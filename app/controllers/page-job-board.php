<?php

namespace App;

use Sober\Controller\Controller;

class JobBoard extends Controller
{
    public function perPage()
    {
        return array( 
            '10'  => 10,
            '25'  => 25,
            '50'  => 50,
            'All' => -1,
        );
    }

    public function getJobs()
    {
        $tax_query     = '';
        $paged         = ( get_query_var('paged') ) ? get_query_var('paged'): 1;
        $filters       = App::formFilters();
        $per_page      = (is_object($filters) && $filters->per_page) ? $filters->per_page : 10;
        $job_location  = !empty($filters->job_location) ? sanitize_text_field($filters->job_location): '';
        $salary_filter = !empty($filters->salary_filter) ? true: false;

        // If salary filter is set, we need two different queries
        // The first will include all jobs with salary ranges
        // The second will include all jobs without salary ranges
        // Then we'll combine the two results
        if ($salary_filter) {
            $salary_args = array(
                'post_type'              => 'job',
                'fields'                  => 'ids',
                'posts_per_page'         => -1,
                'paged'                  => $paged,
                'update_post_term_cache' => false, // Improves Query performance
                'update_post_meta_cache' => false, // Improves Query performance
                'meta_query' => array( // WordPress has all the results, now, return only the events after today's date
                    'relation' => 'OR',
                    array(
                        'relation' => 'AND',
                        array(
                            'key'      => 'salary_range',
                            'compare'  => 'EXISTS',
                        ),
                        array(
                            'key'     => 'job_expiration_date', // Check the start date field
                            'value'   => date("Y-m-d"), // Set today's date (note the similar format)
                            'compare' => '>=', // Return the ones greater than or equal to today's date
                            'type'    => 'DATE' // Let WordPress know we're working with date
                        ),
                    ),
                    array(
                        'relation' => 'AND',
                        array(
                            'key'      => 'salary_range',
                            'value'   => '',
                            'compare' => '!=', // include empty values
                        ),
                        array(
                            'key'     => 'job_expiration_date', // Check the start date field
                            'value'   => date("Y-m-d"), // Set today's date (note the similar format)
                            'compare' => '>=', // Return the ones greater than or equal to today's date
                            'type'    => 'DATE' // Let WordPress know we're working with date
                        ),
                    ),
                ),
            );

            // If there is a location filter, apply it
            if( $job_location ){
                $salary_args['tax_query'] = array(
                    'relation' => 'AND',
                    array(
                        'taxonomy' => 'job_location',
                        'field'     => 'slug',
                        'terms'    => $job_location,
                    )
                );
            }

            $no_salary_args = array(
                'post_type'              => 'job',
                'fields'                  => 'ids',
                'posts_per_page'         => -1,
                'paged'                  => $paged,
                'update_post_term_cache' => false, // Improves Query performance
                'update_post_meta_cache' => false, // Improves Query performance
                'meta_query' => array( // WordPress has all the results, now, return only the events after today's date
                    'relation' => 'OR',
                    array(
                        'relation' => 'AND',
                        array(
                            'key'      => 'salary_range',
                            'compare'  => 'NOT EXISTS',
                        ),
                        array(
                            'key'     => 'job_expiration_date', // Check the start date field
                            'value'   => date("Y-m-d"), // Set today's date (note the similar format)
                            'compare' => '>=', // Return the ones greater than or equal to today's date
                            'type'    => 'DATE' // Let WordPress know we're working with date
                        ),
                    ),
                    array(
                        'relation' => 'AND',
                        array(
                            'key'      => 'salary_range',
                            'value'   => '',
                            'compare' => '=', // include empty values
                        ),
                        array(
                            'key'     => 'job_expiration_date', // Check the start date field
                            'value'   => date("Y-m-d"), // Set today's date (note the similar format)
                            'compare' => '>=', // Return the ones greater than or equal to today's date
                            'type'    => 'DATE' // Let WordPress know we're working with date
                        ),
                    ),
                ),
            );

            // If there is a location filter, apply it
            if( $job_location ){
                $no_salary_args['tax_query'] = array(
                    'relation' => 'AND',
                    array(
                        'taxonomy' => 'job_location',
                        'field'     => 'slug',
                        'terms'    => $job_location,
                    )
                );
            }

            $salary_query    = new \WP_Query( $salary_args );
            $no_salary_query = new \WP_Query( $no_salary_args );
            $combined_posts  = array_merge( $salary_query->posts, $no_salary_query->posts );
            $combined_query  = new \WP_Query( array(
                'post_type'      => 'job',
                'post__in'       => $combined_posts,
                'orderby'        => 'post__in',
                'posts_per_page' => $per_page,
                'paged'          => $paged,
                ),
            );

            return $combined_query;
        } else {
            // Default query
            // All jobs that are not expired
            $args = array(
                'post_type'              => 'job',
                'posts_per_page'         => $per_page,
                'paged'                  => $paged,
                'update_post_term_cache' => false, // Improves Query performance
                'update_post_meta_cache' => false, // Improves Query performance
                'meta_query' => array( // WordPress has all the results, now, return only the events after today's date
                    array(
                        'key'     => 'job_expiration_date', // Check the start date field
                        'value'   => date("Y-m-d"), // Set today's date (note the similar format)
                        'compare' => '>=', // Return the ones greater than or equal to today's date
                        'type'    => 'DATE' // Let WordPress know we're working with date
                    ),
                ),
            );

            // If there is a location filter, apply it
            if( $job_location ){
                $args['tax_query'] = array(
                    'relation' => 'AND',
                    array(
                        'taxonomy' => 'job_location',
                        'field'     => 'slug',
                        'terms'    => $job_location,
                    )
                );
            }

            $query = new \WP_Query($args);
            
            return $query;
        }
    }

    public function getJobLocations()
    {
        // Instead of all event categories, we only want categories of upcoming events
        // 1. get query of upcoming events
        $args = array(
            'post_type'              => 'job',
            'posts_per_page'         => -1,
            'update_post_term_cache' => false, // Improves Query performance
            'update_post_meta_cache' => false, // Improves Query performance
            'meta_query' => array( // WordPress has all the results, now, return only the events after today's date
                array(
                    'key'     => 'job_expiration_date', // Check the start date field
                    'value'   => date("Y-m-d"), // Set today's date (note the similar format)
                    'compare' => '>=', // Return the ones greater than or equal to today's date
                    'type'    => 'DATE' // Let WordPress know we're working with date
                ),
            ),
        );
        $upcomingJobs = new \WP_Query($args);

        // 2. loop through these for their post terms
        $locations = array();
        if($upcomingJobs) {
            foreach( $upcomingJobs->posts as $job ) {
                $jobTerms = wp_get_post_terms($job->ID, 'job_location', array('fields' =>'ids'));
                $locations[] = $jobTerms;
            }

            // 3. this is going to return a multidimensional array. flatten that & grab only the unique values
            $uniqueCats = array_flatten($locations);
            $uniqueCats = array_unique($uniqueCats);
            // remove 742 (remote) from array - adding this to the top of the filters
            $key = array_search(742, $uniqueCats);
            if($key !== false) {
                unset($uniqueCats[$key]);
            }

            // 4. now we'll grab only the needed terms
            $locations = get_terms(array( 'taxonomy' => 'job_location', 'include' => $uniqueCats ));
            return $locations; // exclude Remote. Hard-coded at top of filters
        } else {
            return false;
        }
    }

    public function total()
    {
        $jobs = JobBoard::getJobs();
        return $jobs->found_posts;
    }

    public function begin()
    {
        $jobs          = JobBoard::getJobs();
        $paged         = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
        $post_per_page = $jobs->query_vars['posts_per_page'];
        $offset        = ( $paged - 1 ) * $post_per_page;
        $begin         = $offset + 1;

        return $begin;
    }

    public function end()
    {
        $jobs          = JobBoard::getJobs();
        $total         = $jobs->found_posts;
        $paged         = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
        $post_per_page = $jobs->query_vars['posts_per_page'];
        $offset        = ( $paged - 1 ) * $post_per_page;
        $end           = ( $paged*$post_per_page < $total ) ? $paged * $post_per_page: $total;

        return $end;
    }
    
    public function pagination()
    {
        $jobs         = JobBoard::getJobs();
        $found_posts  = $jobs->found_posts;
        $filters      = App::formFilters();
        $per_page     = (is_object($filters) && $filters->per_page) ? $filters->per_page : 10;
        $total        = ceil($found_posts / $per_page);
        $big          = 999999999; // need an unlikely integer
        $current_page = max( 1, get_query_var('paged') );
        $args         = array(
            'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format'  => '?paged=%#%',
            'current' => $current_page,
            'total'   => $total,
            'prev_text' => '<span class="icon icon-arrow-left"></span>' . __(' Previous', 'visceral'),
            'next_text' => __('Next ', 'visceral') . '<span class="icon icon-arrow-right"></span>'
        );
    
        return paginate_links( $args );
    }

    public function formFilters() 
    {
        return App::formFilters();
    }
}
