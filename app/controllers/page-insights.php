<?php

namespace App;

use Sober\Controller\Controller;

class Insights extends Controller
{

    public function getInsights()
    {
        $paged          = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
        $news_type      = !empty($_GET['news_type']) ? sanitize_text_field($_GET['news_type']) : '';
        $news_topic     = !empty($_GET['news_topic']) ? sanitize_text_field($_GET['news_topic']) : '';
        $news_principle = !empty($_GET['news_principle']) ? sanitize_text_field($_GET['news_principle']) : '';

        $args = array(
            'post_type'              => 'post',
            'posts_per_page'         => 9,
            'paged'                  => $paged,
            'update_post_term_cache' => false, // Improves Query performance
            'update_post_meta_cache' => false, // Improves Query performance
        );

        $args['tax_query'] = array(
            array(
                'taxonomy' => 'news_type',
                'field'    => 'slug',
                'terms'    => array('weekly-reads','news','announcement','journal-insights','journal-insights-other'),
                'operator' => 'NOT IN'
            ));

        if( !empty($news_type) || !empty($news_topic) ){
            $args['tax_query'] = array( 'relation' => 'AND' );
        }

        // If there is a type filter, apply it
        if ( !empty($news_type) ) {
            array_push( $args['tax_query'], array(
                'taxonomy' => 'news_type',
                'field'    => 'slug',
                'terms'    => $news_type,
            ) );
        }

        // If there is a topic filter, apply it
        if ( !empty($news_topic) ) {
            array_push( $args['tax_query'], array(
                'taxonomy' => 'news_topic',
                'field'    => 'slug',
                'terms'    => $news_topic,
            ) );
        }

        // If there is a principle filter, apply it
        if ( !empty($news_principle) ) {
            $args['meta_query'] = array(
                array(
                    'key'     => 'grantmaking_principle',
                    'value'   => $news_principle,
                    'compare' => 'LIKE',
                ),
            );
        }

        $query = new \WP_Query($args);
        return $query;
    }

    public function getWeeklyRead() {
        $args = array(
            'post_type'      => 'post',
            'posts_per_page' => 1,
            'tax_query'      => array(
                array(
                    'taxonomy' => 'news_type',
                    'field'    => 'slug',
                    'terms'    => 'weekly-reads',
                ),
            ),
        );

        $query = new \WP_Query($args);
        return $query;
    }

    public function getLatestJournal()
    {
         $query = new \WP_Query(array(
            'post_type'         => 'journals',
            'posts_per_page'    => 1,
            'post_status'       => 'publish',
        ));

         return $query;
    }

    public function getMoreJournals() {
         $query = new \WP_Query(array(
            'post_type'         => 'journals',
            'posts_per_page'    => '4',
            'offset'            => 1,
            'post_status'       => 'publish',
            'orderby'           => 'date',
        ));

         return $query;
    }

    public function featuredPost()
    {
        $related_content = visceral_related_posts(1, array('post'), 'main_article');
        return $related_content;
    }

     public function featuredNewArticle()
    {
        $related_content = visceral_related_posts(1, array('post'), 'top_news_article');
        return $related_content;
    }

    public function topics()
    {
        return get_terms('news_topic');
    }
    
    public function principles()
    {
        $principles_titles = array('Tie Practices to Values', 'Narrow the Power Gap', 'Drive Equity', 'Steward Responsively', 'Learn, Share, Evolve');
        $principles = array();

        foreach ($principles_titles as $title) {
            $principle = get_posts(array(
                'post_type'      => 'page',
                'title'          => $title,
                'posts_per_page' => 1,
            ));

            if(!empty($principle)) {
                $principle = $principle[0];
                array_push($principles, $principle);
            }
        }

        return $principles;
    }

    public function insight_pagination()
    {
        $news         = Insights::getInsights();
        $total        = $news->max_num_pages;
        $big          = 999999999; // need an unlikely integer
        $current_page = max( 1, get_query_var('paged') );

        $args         = array(
            'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format'  => 'page/%#%',
            'current' => $current_page,
            'total'   => $total,
            'prev_text' => '<span class="icon icon-arrow-left"></span>' . __(' Previous', 'visceral'),
            'next_text' => __('Next ', 'visceral') . '<span class="icon icon-arrow-right"></span>'
        );
    
        return paginate_links( $args );
    }    
}
