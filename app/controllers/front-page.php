<?php

namespace App;

use Sober\Controller\Controller;

class FrontPage extends Controller
{
    public function imgBgStyle()
    {
        $img = '';
        if ( has_post_thumbnail() ) {
            $home = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
            $img  = 'style=background-image:url(' . $home[0] . ');';
        }
        
        return $img;
    }

    public function homepageVideo()
    {
        $video_url = get_post_meta(get_the_id(), 'background_video', true);
        $video = '';

        if (!empty($video_url)) {
            $video = '<div class="home-intro-slide home-intro-slide-video-bg">';
            $video .= '    <video autoplay loop muted playsinline class="masthead__video" src="'. $video_url . '">';                
            $video .= '</div>';
        }

        return $video;
    }
}