<?php
namespace App;

use Sober\Controller\Controller;

class Resource extends Controller
{

    public function thumbnailImage()
    {
        $thumbnail_image = function_exists('get_field') && get_field('resource_image') ? get_field('resource_image') : '';   
        return $thumbnail_image;
    }

    public function topics()
    {
        $resource_topics = wp_get_post_terms(get_the_id(), 'resource_topic');
        $resource_topics_output = array();

        if (!empty($resource_topics)) {
            foreach ($resource_topics as $resource_topic) {
                $resource_topics_output[] = '<a href="/resources/?resource_type=&resource_topic=' . $resource_topic->slug . '">' . $resource_topic->name . '</a>';
            }
        }

        return implode(', ', $resource_topics_output);
    }

    public function types()
    {
        $commas    = '';
        $post_id    = get_the_ID();
        $types      = get_the_terms( $post_id, 'resource_type' );
        $type_names = array();

        if( $types ) {
            foreach( $types as $type ) {
                $type_names[] = $type->name;
            }

            $commas = implode(", ", $type_names);
        }

        return $commas;
    }

    public function principles()
    {
        $commas          = '';
        $post_id         = get_the_ID();
        $principles      = get_the_terms( $post_id, 'principle' );
        $principle_names = array();

        if($principles) {
            foreach( $principles as $principle ) {
                $slug     = $principle->slug;
                $pageSlug = '/principles-for-peak-grantmaking/' . $slug;
                $page     = get_page_by_path($pageSlug);
                
                if($page) {
                    $principle_names[] = '<a href="' . get_the_permalink($page->ID) . '">' . $principle->name . '</a>';
                }
            }
            $commas = implode(", ", $principle_names);
        }

        return $commas;
    }

    public function relatedContent()
    {
        $related_content = visceral_related_posts(3, array('post', 'tribe_events', 'resource'), 'related_posts');
        return $related_content;
    }

    public function members()
    {
        $members = '';

        if( function_exists('get_field') && ($member_access = get_field('member_options')) ) {
            if( $member_access['value'] == 'organizations') {
                $members = '<p class="post__access"><i class="fas fa-lock"></i> Organization Members Only</p>';
            } elseif( $member_access['value'] == 'members') {
                $members = '<p class="post__access"><i class="fas fa-lock"></i> Members Only</p>';
            }
        }

        return $members;
    }
}
