<?php

namespace App;

use Sober\Controller\Controller;

class Journals extends Controller
{
    public function getJournals() 
    {
        // get only first 3 results
        $ids = get_field('journal_posts');

        $query = new \WP_Query(array(
            'post_type'         => 'post',
            'posts_per_page'    => -1,
            'post__in'          => $ids,
            'post_status'       => 'any',
            'orderby'           => 'post__in',
        ));

        return $query;
    }

    public function getInThisIssue() 
    {
        // get only first 3 results
        $query = get_field('in_this_issue_content');
        $ids = array();
        
        if( have_rows('in_this_issue_content') ):
        	$c = 0;  
            while ( have_rows('in_this_issue_content') ) : the_row();
        		$ids[$c] = get_sub_field('post');
            	$c++;
            endwhile;
        endif;

        return $query;
    }
    public function getMoreJournals() {

    	 $currentID = get_the_ID();

    	 $query = new \WP_Query(array(
            'post_type'      => 'journals',
            'posts_per_page' => '4',
            'post_status'    => 'publish',
            'post__not_in'   => array($currentID),
            'orderby'        => 'date',
        ));

    	 return $query;
    }

    public function getAllJournals() {

         $currentID = get_the_ID();

         $query = new \WP_Query(array(
            'post_type'         => 'journals',
            'posts_per_page'    => -1,
            'post_status'       => 'publish',
            'post__not_in' => array($currentID),
            'orderby'           => 'date',
        ));

         return $query;
    }

    public function about_this_issue_image() {
    	$image = get_field('about_this_issue_image');
    	$size = 'about-this-issue'; // (thumbnail, medium, large, full or custom size)
        
    	if( $image ) {
    	    return wp_get_attachment_image( $image, $size );
    	}
    }
    
}
