<?php

namespace App;

use Sober\Controller\Controller;

class Resources extends Controller
{
    public function getResources()
    {
        $tax_query          = '';
        $paged              = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
        $resource_principle = !empty($_GET['resource_principle']) ? sanitize_text_field($_GET['resource_principle']): '';
        $resource_type      = !empty($_GET['resource_type']) ? sanitize_text_field($_GET['resource_type'])          : '';
        $resource_topic     = !empty($_GET['resource_topic']) ? sanitize_text_field($_GET['resource_topic']) : '';

        $args = array(
            'post_type'              => 'resource',
            'posts_per_page'         => 12,
            'paged'                  => $paged,
            'tax_query'              => $tax_query,
            'update_post_term_cache' => false, // Improves Query performance
            'update_post_meta_cache' => false, // Improves Query performance
        );

        if( !empty($resource_type) || !empty($resource_principle) || !empty($resource_topic) ){
            $args['tax_query'] = array(
                'relation' => 'AND',
            );
        }

        // If there is a type filter, apply it
        if ( !empty($resource_type) ) {
            array_push( $args['tax_query'], array(
                'taxonomy' => 'resource_type',
                'field'     => 'slug',
                'terms'    => $resource_type,
            ) );
        }

        // If there is a principle filter, apply it
        if ( !empty($resource_principle) ) {
            array_push( $args['tax_query'], array(
                'taxonomy' => 'principle',
                'field'     => 'slug',
                'terms'    => $resource_principle,
            ) );
        }

        // If there is a topic filter, apply it
        if ( !empty($resource_topic) ) {
            array_push( $args['tax_query'], array(
                'taxonomy' => 'resource_topic',
                'field'     => 'slug',
                'terms'    => $resource_topic,
            ) );
        }

        $query = new \WP_Query($args); 
        return $query;
    }

    public function featuredResources()
    {
        if (function_exists('get_field')) {
            $featured_resources = get_field('featured_resource');

            if (!empty($featured_resources)) {
                return $featured_resources;
            }
        } else {
            return false;
        }
    }

    public function getResourceTypes()
    {
        return get_terms('resource_type');
    }

    public function getResourcePrinciples()
    {
        return get_terms('principle');
    }

    public function getResourceTopics()
    {
        return get_terms('resource_topic');
    }

    public function total()
    {
        $resources = Resources::getResources();
        return $resources->found_posts;
    }

    public function begin()
    {
        $resources     = Resources::getResources();
        $paged         = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
        $post_per_page = $resources->query_vars['posts_per_page'];
        $offset        = ( $paged - 1 ) * $post_per_page;
        $begin         = $offset + 1;

        return $begin;
    }

    public function end()
    {
        $resources     = Resources::getResources();
        $total         = $resources->found_posts;
        $paged         = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
        $post_per_page = $resources->query_vars['posts_per_page'];
        $offset        = ( $paged - 1 ) * $post_per_page;
        $end           = ( $paged*$post_per_page < $total ) ? $paged * $post_per_page: $total;

        return $end;
    }
    
    public function pagination()
    {
        $resources    = Resources::getResources();
        $total        = $resources->max_num_pages;
        $big          = 999999999; // need an unlikely integer
        $current_page = max( 1, get_query_var('paged') );
        $args         = array(
            'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format'  => '?paged=%#%',
            'current' => $current_page,
            'total'   => $total,
            'prev_text' => '<span class="icon icon-arrow-left"></span>' . __(' Previous', 'visceral'),
            'next_text' => __('Next ', 'visceral') . '<span class="icon icon-arrow-right"></span>'
        );
    
        return paginate_links( $args );
    }
}
