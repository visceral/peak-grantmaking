<?php

namespace App;

use Sober\Controller\Controller;

class Model extends Controller
{

    // Phases content
    public function phases()
    {
        // Return content from repeater field
        return array_map(function($phase) {
            return [
                'label'   => $phase['label'] ?? NULL,
                'image'   => wp_get_attachment_image_src( $phase['icon'] ) ?? NULL,
                'heading' => $phase['heading'] ?? NULL,
                'main'    => $phase['main_text'] ?? NULL,
                'sidebar' => $phase['sidebar'] ?? NULL,
            ];
        }, get_field('model_phases') ?? []);
    }

    // Phase labels
    // Store in separate array to output anchor links
    public function phaseLabels()
    {
        // Return content from repeater field
        return array_map(function($phase) {
            return [
                'label' => $phase['label'] ?? NULL,
            ];
        }, get_field('model_phases') ?? []);
    }

    // Cross-Cutting Competencies content
    public function competencies()
    {
        // Return content from repeater field
        return array_map(function($phase) {
            return [
                'label'   => $phase['label'] ?? NULL,
                'image'   => wp_get_attachment_image_src( $phase['icon'] ) ?? NULL,
                'heading' => $phase['heading'] ?? NULL,
                'main'    => $phase['main_text'] ?? NULL,
                'sidebar' => $phase['sidebar'] ?? NULL,
            ];
        }, get_field('model_competencies') ?? []);
    }

    // Cross-Cutting Competencies labels
    // Store in separate array to output anchor links
    public function competencyLabels()
    {
        // Return content from repeater field
        return array_map(function($phase) {
            return [
                'label' => $phase['label'] ?? NULL,
            ];
        }, get_field('model_competencies') ?? []);
    }

    public function relatedContent()
    {
        $related_content = visceral_related_posts(3, array('post', 'tribe_events', 'resource'), 'related_posts');
        return $related_content;
    }
}
