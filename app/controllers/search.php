<?php

namespace App;

use Sober\Controller\Controller;

class Search extends Controller
{

    public function getPosts()
    {
        $paged      = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
        $news_type  = !empty($_GET['news_type']) ? sanitize_text_field($_GET['news_type']) : '';
        $news_topic = !empty($_GET['news_topic']) ? sanitize_text_field($_GET['news_topic']) : '';

        $args = array(
            'post_type'              => 'post',
            'posts_per_page'         => 8,
            'paged'                  => $paged,
            'update_post_term_cache' => false, // Improves Query performance
            'update_post_meta_cache' => false, // Improves Query performance
        );

        if( !empty($news_type) || !empty($news_topic) ){
            $args['tax_query'] = array(
                'relation' => 'AND',
            );
        }

        // If there is a type filter, apply it
        if ( !empty($news_type) ) {
            array_push( $args['tax_query'], array(
                'taxonomy' => 'news_type',
                'field'    => 'slug',
                'terms'    => $news_type,
            ) );
        }

        // If there is a topic filter, apply it
        if ( !empty($news_topic) ) {
            array_push( $args['tax_query'], array(
                'taxonomy' => 'news_topic',
                'field'    => 'slug',
                'terms'    => $news_topic,
            ) );
        }

        $featured_posts = News::featuredPosts();

        // Only exclude featured posts if filtering is not selected
        if ($featured_posts && empty($news_type) && empty($news_topic) ) {
            $featured = array();

            foreach( $featured_posts as $feature ) {
                array_push($featured, $feature->ID);
            }

            $args['post__not_in'] = $featured;
        }

        $query = new \WP_Query($args);
        return $query;
    }

    

    public function getNewsTopics()
    {
        return get_terms('news_topic');
    }

    public function getFilters()
    {
        return App::formFilters();
    }
    
    public function pagination()
    {
        global $wp_query;
        $total        = $wp_query->max_num_pages;
        $big          = 999999999; // need an unlikely integer
        $current_page = max( 1, get_query_var('paged') );

        $args         = array(
            'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format'  => 'page/%#%',
            'current' => $current_page,
            'total'   => $total,
            'prev_text' => '<span class="icon icon-arrow-left"></span>' . __(' Previous', 'visceral'),
            'next_text' => __('Next ', 'visceral') . '<span class="icon icon-arrow-right"></span>'
        );
    
        return paginate_links( $args );
    }
}
