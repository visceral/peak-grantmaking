<?php

namespace App;

use Sober\Controller\Controller;

class Post extends Controller
{
    public function thumbnailImage()
    {
        $thumbnail_image = '';
        if (function_exists('get_field')) {
            $thumbnail_image = get_field('resource_image');
        }
        
        return $thumbnail_image;
    }

    public function featured_image()
    {
        return get_aspect_ratio_image(16, 9, 'large');
    }

    public function showImage() {
        
        $show = '';
        if(function_exists('get_field')) {
            $show = get_field('show_featured_image', get_the_id());
        }

        return $show;
    }

    public function topics()
    {   
        $news_topics = get_primary_taxonomy_term(get_the_id(), 'news_topic');
        $news_topics_output = '';
        if($news_topics){
            $news_topics_output = '<a href="'.get_bloginfo('url' ).'/insights/?news_topic=' . $news_topics['slug'] . '">' . $news_topics['title'] . '</a>';

        }
        return $news_topics_output;
    }
    
    public function principles()
    {
        $m = array();
        $principles = get_field('grantmaking_principle');

        if($principles) {
            foreach($principles as $principle) {
                $m[] = '<a href="' . get_permalink($principle) . '">' . $principle->post_title . '</a>';
            }
        }

        return implode(', ', $m);
    }

    public function principlesLinks()
    {
        $m          = array();
        $principles = get_field('grantmaking_principle');

        if($principles) {
            foreach($principles as $principle) {
                $m[] = '<a class="" href="' . get_permalink( get_page_by_path('insights') ) . '?news_principle=' . $principle->ID . '"><strong>' . $principle->post_title . '</strong></a>';
            }
        }

        return implode(', ', $m);
    }

    public function topics_sidebar()
    {
        $news_topics = wp_get_post_terms(get_the_id(), 'news_topic');
        $news_topics_output = '';

        if (!empty($news_topics)) {
            foreach ($news_topics as $news_topic) {
                $news_topics_output .= '<a class="link--callout" href="'.get_bloginfo('url' ).'/insights/?news_topic=' . $news_topic->slug . '">' . $news_topic->name . '</a><br>';
            }
        }
        return $news_topics_output;
    }

    public function relatedContent()
    {
        $related_content = visceral_related_posts(3, array('post'), 'related_posts');
        return $related_content;
    }

    public function type()
    {
        return get_the_terms( get_the_ID(), 'news_type' );
    }

    public function hasAccess()
    {
        return peakMemberHasAccess();
    }

    public function journal()
    {
        $args = array(
            'post_type'  => 'journals',
            'meta_query' => array(
                array(
                    'key'     => 'journal_posts', // name of custom field
                    'value'   => '"' . get_the_ID() . '"',
                    'compare' => 'LIKE'
                ),
            ),
        );

        return get_posts($args);
    }

    public function inIssue()
    {
        $id = get_the_ID();
        $in_issue = get_posts(array(
        	'post_type'  => 'journals',
        	'meta_query' => array(
        	   'relation' => 'OR', /* <-- here */
        		array(
        			'key'     => 'in_this_issue_content_0_post',
        			'value'   => $id,
        			'compare' => '=',
        		),
        		array(
        			'key'     => 'in_this_issue_content_1_post',
        			'value'   => $id,
        			'compare' => '=',
        		),
        		array(
        			'key'     => 'in_this_issue_content_2_post',
        			'value'   => $id,
        			'compare' => '=',
        		),
        		array(
        			'key'     => 'in_this_issue_content_3_post',
        			'value'   => $id,
        			'compare' => '=',
        		)
        	)
        ));

        return $in_issue;
    }

    public function date()
    {
        return get_the_date();
    }

    public function restrictedcontent()
    {
        $r = '';

        if(function_exists('get_field') && get_field('restrict_content')){
            if(is_user_logged_in()) {
                $r = 0;
            } else {
                $r = 1;
            }
        } else {
            $r = 0;
        }

        return $r;
    }

    public function postMeta()
    {
        $meta_output     = '';
        $journal_eyebrow = '';
        $journal_number  = '';
        $journal_link    = '';
        $journal  = $this->journal();
        $in_issue = $this->inIssue();
        $principles_links = $this->principlesLinks();
        $topics = $this->topics();

        if($journal) {
        	$journal_id      = $journal[0]->ID;
        	$journal_number  = get_field('journal_issue_number',$journal_id);
        	$journal_link    = get_permalink($journal_id);
        	$journal_eyebrow = '<a href = "'.$journal_link.'"><span>Journal | Issue </span>'. $journal_number .'</a>';

        	if($this->topics()){
        		$journal_eyebrow .= '<br>';
        	}
        }elseif($in_issue){
        	$journal_id      = $in_issue[0]->ID;
        	$journal_number  = get_field('journal_issue_number',$journal_id);
        	$journal_link    = get_permalink($journal_id);
        	$journal_eyebrow = '<a href = "'.$journal_link.'"><span>Journal | Issue </span>'. $journal_number .'</a>';

        	if($this->topics()){
        		$journal_eyebrow .= '<br>';
        	}
        }

        if($topics) {
            $meta_output = '<p class="post__label">'. $journal_eyebrow . $topics .'</p>';
        }

        if($principles_links) {
            $meta_output .= '<p class="post__label">'. $principles_links .'</p>';
        }

        return $meta_output;
    }

    public function author()
    {
        // Authorship
        $author = array();
        $image  = '';
        $bio    = '';
        $author_image = '';
        $author['count'] = 1;

        if( have_rows('custom_authors') ) {
        	$count = count(get_field('custom_authors'));
        	$c     = 1;
            $name = array();

            while ( have_rows('custom_authors') ) : the_row();
                $name[] = get_sub_field('custom_author');
            endwhile;

            $author['author'] = implode(', ', $name);
        } elseif( get_field('custom_author') || get_field('custom_author_description') ) { // custom author
            $author['author'] = get_field('custom_author') ? get_field('custom_author') : '';
            $bio          = get_field('custom_author_description');
            $author_image = get_field('custom_author_image');

            if( $author_image ) {
                $author['image'] = '<img src="' . $author_image['url'] . '" width="' . $author_image['width'] . '" height="' . $author_image['height'] . '" alt="' . $author_image['alt'] . '">';
            }
        } else { // default to WP author
            $id     = get_the_author_meta( 'ID' );
            $author_image = '';

            if( get_field('image', 'user_' . $id) ) {
                $author_image = get_field('image', 'user_' . $id); // ACF field should be set to return image array
                $author_image = wp_get_attachment_image_src( $author_image, 'thumbnail' );
                $author['image'] = '<img src="' . $author_image[0] . '" width="' . $author_image[1] . '" height="' . $author_image[2] . '" alt="' . $author_image[3] . '">';
            }

            $author['author'] = get_the_author();

            // Bio
            if( get_field('short_bio', 'user_' . $id ) ) {
                $author['bio'] = get_field('short_bio', 'user_' . $id );
            } elseif( get_field('full_bio', 'user_' . $id ) ) {
                $author['bio'] = get_field('full_bio', 'user_' . $id );
            }
        }

        return $author;
    }

    public function customAuthors()
    {
        $m = '';

        if( have_rows('custom_authors') ):
            while ( have_rows('custom_authors') ) : the_row();
            	$author       = get_sub_field('custom_author');
            	$bio          = get_sub_field('custom_author_description');
            	$author_image = get_sub_field('custom_author_image');

            	if( $author_image ) {
            	    $image = '<img src="' . $author_image['url'] . '" width="' . $author_image['width'] . '" height="' . $author_image['height'] . '" alt="' . $author_image['alt'] . '">';
            	} else {
            		$image = '<img src="'.get_stylesheet_directory_uri().'/assets/images/no-author-image.png" width="150" height="150" alt="' . $author_image['alt'] . '">';
            	}

            	$m .= '<div class="row">';
        	    $m .= '	<div class="column md-25">';
        	    $m .= '	    <div class="img-circle">' . $image . '</div>';
        	    $m .= '	</div>';
                $m .= '	<div class="column md-75 ">';
        	    $m .= '	    <p><strong>' . $author . '</strong><br>' . wp_trim_words( $bio, 55 ) . '</p>';
        	    $m .= '	</div>';
                $m .= '</div>';
                
            endwhile;
        endif;

        return $m;
    }
}
