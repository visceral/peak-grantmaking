<?php

namespace App;

use Sober\Controller\Controller;

class App extends Controller
{   
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function mastheadImage()
    {
        if( is_search() ) return false;

        $image_id = '';
        
        // Start off with a check for custom header image (ACF). Return value should be set to Image ID
        if ( function_exists('get_field') && get_field( 'masthead_image' ) != '' ) {
            $image_id = get_field('masthead_image');
        }

        // Finally, if nothing else, we check for a featured image and get it's ID
        elseif ( has_post_thumbnail() ) {
            $image_id = get_post_thumbnail_id();
        }

        return $image_id;

    }

    public static function title()
    {
        
        if(isset($_GET['news_type']) && !empty($_GET['news_type'])) {
            $term_name = get_term_by('slug',$_GET['news_type'],'news_type');
            return $term_name->name;
        }
        if((isset($_GET['news_topic']) && !empty($_GET['news_topic'])) && (isset($_GET['news_principle']) && !empty($_GET['news_principle']))) {
            return get_the_title();
        }
        if(isset($_GET['news_topic']) && !empty($_GET['news_topic'])) {
            $term_name = get_term_by('slug',$_GET['news_topic'],'news_topic');
            return $term_name->name;
        }
        if(isset($_GET['news_principle']) && !empty($_GET['news_principle'])) {
            $page = get_post($_GET['news_principle']);
            return $page->post_title;
        }
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if(is_author()) {
            return get_the_author_meta('display_name');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public static function subtitle()
    {
        $ID             = get_the_ID();
        $s              = '';
        $subheadline    = '';
        $news_type      = isset($_GET['news_type']) ? sanitize_text_field($_GET['news_type']) : false;
        $news_topic     = isset($_GET['news_topic']) ? sanitize_text_field($_GET['news_topic']) : false;
        $news_principle = isset($_GET['news_principle']) ? sanitize_text_field($_GET['news_principle']) : '';

        if($news_type) {
            $term        = get_term_by('slug',$news_type,'news_type');
            $subheadline = $term->description;
        } elseif($news_topic) {
            $term        = get_term_by('slug',$news_topic,'news_topic');
            $subheadline = $term->description;
        } elseif ( function_exists('get_field') && get_field( 'subheadline', $ID ) != '' ) {
            $subheadline = get_field( 'subheadline', $ID );
        }

        if(!empty($subheadline)) {
            $s = '<h2 class="masthead__subheadline">' . $subheadline . '</h2>';
        }

        // Insights page
        if(is_page('insights') && !empty($news_principle)) {
            $ID = $news_principle;
            if ( function_exists('get_field') && get_field( 'insights_subheadline', $ID ) != '' ) {
                $s = get_field( 'insights_subheadline', $ID );
            }
        }

        // Front page
        if( is_front_page() && get_field('button_link') ) {
            $text = get_field('button_text') ? get_field('button_text') : 'Learn More';
            $s = '<a href="' . get_field('button_text') . '" class="btn">' . $text . '</a>';
        }

        return $s;
    }

    public static function breadcrumbs()
    {
        if( is_front_page() || is_search() || is_singular('wpdmpro') ) return;

        $b = '';

        // Single posts
        if( is_singular('post') ) {
            $b .= '<p class="breadcrumbs reveal"><span class="icon icon-arrow-left"></span> <a class="breadcrumbs__link" href="' . get_the_permalink( get_page_by_path('insights') ) . '">Insights</a></p>';
        }

        elseif( is_singular('resource') ) {
            $b .= '<p class="breadcrumbs reveal"><span class="icon icon-arrow-left"></span> <a class="breadcrumbs__link" href="' . get_the_permalink( get_page_by_path('resources') ) . '">Resources</a></p>';
        }
        //Journal issues
        elseif( is_singular('journals') ) {
            $b .= '<p class="breadcrumbs reveal"><a class="breadcrumbs__link" href="' . get_the_permalink( get_page_by_path('journals') ) . '">Journal</a> | Issue '.get_field('journal_issue_number').'</p>';
        }

        // Author/staff
        elseif( is_author() ) {
            $b .= '<p class="breadcrumbs reveal"><span class="icon icon-arrow-left"></span> <a class="breadcrumbs__link" href="' . get_the_permalink( get_page_by_path('about-us/staff') ) . '">Staff</a></p>';
        }

        // If we're on a top level page and there is a headline set
        elseif( is_page() && !get_post_ancestors( get_the_ID() ) ) {
            return $b;
        }

        elseif( function_exists('yoast_breadcrumb') ) {
            $b .= yoast_breadcrumb( '<p class="breadcrumbs reveal">','</p>' );
        }

        return $b;
    }

    public function getAllTopics()
    {

        return get_terms('news_topic');
    }

    public static function formFilters()
    {
        $form_filters = '';

        if($_REQUEST) {
            $form_filters = new \stdClass();
            foreach ($_REQUEST as $key => $value) {
                $form_filters->$key = $value;
            }
        }

        return $form_filters;
    }

    public function getInsightPermalink()
    {
        
        return get_permalink( get_page_by_title( 'Insights' ) );    
    }

    public function getLatestJournalPermalink() 
    {    
        $args = array( 
            'post_type'         => 'journals',
            'posts_per_page'    => 1,
            'post_status'       => 'publish',
        );
        $recent_posts = wp_get_recent_posts( $args );

        foreach( $recent_posts as $recent ){
            return get_permalink($recent["ID"]);
        }
    }
}
