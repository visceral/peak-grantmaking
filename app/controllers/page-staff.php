<?php

namespace App;

use Sober\Controller\Controller;

class Staff extends Controller
{

    public function getStaff()
    {
        // Get departments, which are children of Staff
        $staff = get_terms( array(
                'taxonomy' => 'user-type',
                'orderby'  => 'menu_order',
                'child_of' => 271
            )
        );
        
        return $staff;
    }

    public static function getStaffUsers($department)
    {
        $args = array(
            'taxonomy' => 'user-type',
            'term'     => $department,
        );

        $user_args = array(
            'meta_key' => 'order',
            'orderby'  => 'meta_value_num',
            'order'    => 'ASC',
            'meta_query' => array(
                array(
                    'key' => 'order',
                    'type' => 'NUMERIC'
                )
            )
        );

        return wp_get_users_of_group( $args, $user_args ); // WP User Groups query
    }

    public function featuredPosts()
    {
        $related_content = visceral_related_posts(2, array('post'), 'featured_news');
        return $related_content;
    }

    public function getNewsTypes()
    {
        return get_terms('news_type');
    }

    public function getNewsTopics()
    {
        return get_terms('news_topic');
    }

    public function getFilters()
    {
        return App::formFilters();
    }
}
