<?php

namespace App;

use Sober\Controller\Controller;

class Author extends Controller
{

    public function id() {
        return get_queried_object_id();
    }

    public function getPosts()
    {
        $args = array(
            'post_type'              => 'post',
            'posts_per_page'         => 8,
            'author'                 => Author::id(),
            'update_post_term_cache' => false, // Improves Query performance
            'update_post_meta_cache' => false, // Improves Query performance
        );

        $query = new \WP_Query($args);
        return $query;
    }

    public function user() {
        return get_userdata( Author::id() );
    }

    public function headshot()
    {
        $image = '';
        if (function_exists('get_field')) {
            // ACF image field set to return image ID
            $image = get_field('image', 'user_' . Author::id());
            $image = wp_get_attachment_image_src($image, 'medium');
        }

        $class = ( ($image[2] / $image[1]) > 1 ) ? 'portrait' : '';

        $image[] = $class;
        
        return $image;
    }

    public function email() {
        $author = Author::user();

        return $author->user_email;
    }

    public function fname() {
        $author = Author::user();

        return $author->first_name;
    }

    public function title()
    {
        return get_field('job_title', 'user_' . Author::id());
    }

    public function org()
    {
        return get_field('organization', 'user_' . Author::id());
    }

    public function twitter()
    {
        return get_field('twitter', 'user_' . Author::id());
    }

    public function phone()
    {
        return get_field('phone_number', 'user_' . Author::id());
    }

    public function department() {
        return get_field('user_department', 'user_' . Author::id());
    }

    public function shortBio()
    {
        return get_field('short_bio', 'user_' . Author::id());
    }

    public function fullBio()
    {
        return get_field('full_bio', 'user_' . Author::id());
    }
}